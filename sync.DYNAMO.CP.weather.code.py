#!/usr/bin/env python
#=========================================================================================
# 	This script transfers VAPOR data from the weather server to a local directory	
#
#    Aug, 2014	Walter Hannah 		University of Miami
#=========================================================================================
import datetime
import sys
import os
import numpy as np
#=========================================================================================
#=========================================================================================

server = "weather.rsmas.miami.edu"

DST_DIR = "~/Research/DYNAMO/CP/"

SRC_DIR = "~/Research/DYNAMO/CP/"

cmd = "rsync -uavt "

#---------------------------------------
#---------------------------------------
file_type = ".ncl"

os.system(cmd+" "+server+":"+SRC_DIR+"/*"+file_type+"  "+DST_DIR+"/")

#dir = "figs_TPW"
#os.system(cmd+" "+server+":"+SRC_DIR+dir+"/*"+file_type+"  "+DST_DIR+dir+"/")


#=========================================================================================
#=========================================================================================
