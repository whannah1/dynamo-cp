; convert the gridded TOGA radar to match the 
; time coordinate of ECMWF analysis data
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/custom_functions.ncl"
begin

    ifile = "~/Data/DYNAMO/RADAR/TOGA/TOGA_radar_gridded.v1.OND_2011_DYNAMO.nc"

    ofile = "~/Data/DYNAMO/RADAR/TOGA/TOGA_radar_gridded.v1.alt.OND_2011_DYNAMO.nc"


    print(""+ifile)
    print(""+ofile)
    printline()
    
;====================================================================================================
; Load Radar Data
;====================================================================================================
    if isfilepresent(ofile) then system("rm "+ofile) end if
    outfile = addfile(ofile,"c")

    infile = addfile(ifile,"r")
    itime = infile->time
    itime = (/ itime/3600. /)
    itime = (/ itime - itime(0) /)
    itime@units = "hours since 2011-10-01 00:00:00"

    lat = infile->lat
    lon = infile->lon

    nlat = dimsizes(lat)
    nlon = dimsizes(lon)

    nt = (31+30+31)*4
    otime = tofloat(ispan(0,nt-1,1))*6.
    otime@units = "hours since 2011-10-01 00:00:00"

    var = getfilevarnames(infile)
    num_v = dimsizes(var)

    odim = (/nt,nlat,nlon/)

    ;printline()
    ;print("file vars:")
    ;print("  "+var)
    ;printline()
    ;printMAM(infile->frac_shal)
    ;printMAM(infile->frac_cong)
    ;printMAM(infile->rain_shal)
    ;printMAM(infile->rain_cong)
    ;exit
;====================================================================================================
;====================================================================================================
    printline()
    do v = 0,num_v-1
    if dimsizes(dimsizes(infile->$var(v)$)).eq.3 then 

        print("  "+var(v))

        iV = infile->$var(v)$
    
        oV = new(odim,float)
    
        ;iV = where(iV.ne.0.,iV,iV@_FillValue)

        do t = 0,nt-1
            condition := (itime.gt.(otime(t)-3)).and.(itime.le.(otime(t)+3))
            condition := conform(iV,condition,0)
            if num(condition).gt.0 then 
                oV(t,:,:) = dim_avg_n( where(condition,iV,iV@_FillValue) ,0)
            end if
        end do
        
        if isStrSubset(var(v),"rain") then oV = (/ oV *24. /) end if
    
        ;oV = (/ oV /stddev(oV) /) 
    
        ;oV@units = iV@units
        if .not.isatt(iV,"long_name") then iV@long_name = var(v) end if
        oV@long_name = iV@long_name
        oV!0 = "time"
        oV!1 = "lat"
        oV!2 = "lon"
        oV&time = otime
        oV&lat  = lat
        oV&lon  = lon

        ;-----------------------------------
        ; print # of missing values as %
        ;-----------------------------------
        iV@long_name = oV@long_name
        chkMissing(iV)
        chkMissing(oV)
        printline()
        ;-----------------------------------
        ;-----------------------------------

        outfile->$var(v)$ = oV

        delete([/iV,oV/])

    end if
    end do
    printline()
;====================================================================================================
;====================================================================================================
end
