load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/custom_functions_DYNAMO_LSF.ncl"
load "$NCARG_ROOT/custom_functions_DYNAMO_ECMWF.ncl"
load "$NCARG_ROOT/custom_functions.ncl"
begin

    debug       = False
    reduceRes   = False


    if reduceRes then
        tfile = "~/Data/DYNAMO/MIMIC/6hour.DYNAMO.MIMIC.CP.SAres.nc"
    else
        tfile = "~/Data/DYNAMO/MIMIC/6hour.DYNAMO.MIMIC.CP.nc"
    end if

    lat1 = -20.
    lat2 =  20.
    lon1 =  40.
    lon2 = 120.
    
    mlev = (/800,800/)

    setfileoption("nc","Format","NetCDF4")
;====================================================================================================
; Load Sounding Array Data
;====================================================================================================
    infile = addfile("~/Data/DYNAMO/LSA/v2b/dynamo_basic_v2b_2011all.nc","r")
    SAlat = infile->lat({lat1:lat2})
    SAlon = infile->lon({lon1:lon2})
;====================================================================================================
; Load ECMWF U and V wind
;====================================================================================================
    idir = "~/Data/DYNAMO/ECMWF/data/"
    ufile = addfile(idir+"6hour.DYNAMO.ECMWF.U.nc","r")
    vfile = addfile(idir+"6hour.DYNAMO.ECMWF.V.nc","r")
    u = ufile->U(:,{mlev},{lat1:lat2},{lon1:lon2})
    v = vfile->V(:,{mlev},{lat1:lat2},{lon1:lon2})
;====================================================================================================
; Load Ps and Calculate dP
;====================================================================================================
    lev = ufile->lev
    infile = addfile(idir+"6hour.DYNAMO.ECMWF.Ps.nc","r")
    ps = infile->Ps(:,{lat1:lat2},{lon1:lon2})  
    p   = lev*100.
    p@units = "Pa"
    p!0 = "lev"
    p&lev = p
    dp = calc_dP(p,ps)
    dp!1 = "lev"
    dp&lev = lev
    coldp = dim_sum_n_Wrap(dp/g,1)
;====================================================================================================
; Calculate steering flow
;====================================================================================================
    ub = dim_sum_n(u*dp(:,{mlev},:,:)/g,1) / dim_sum_n(dp(:,{mlev},:,:)/g,1) 
    vb = dim_sum_n(v*dp(:,{mlev},:,:)/g,1) / dim_sum_n(dp(:,{mlev},:,:)/g,1) 
;====================================================================================================
; Load (or Calculate) LT for MIMIC 
;====================================================================================================
    time = ispan(0,92*24-6,6)
    time@units = "hours after 2011-10-01 00:00:00"
    
    ifile  = "~/Data/DYNAMO/MIMIC/cimss_morphed_tpw_DYNAMO_2011.nc"
    infile = addfile(ifile,"r")
    MItime = infile->time
    MIlat  = infile->latitude ({lat1:lat2})
    MIlon  = infile->longitude({lon1:lon2})
    tmp   = infile->tpw(:,{lat1:lat2},{lon1:lon2}) 
    MITPW = new(dimsizes(tmp),float)
    MITPW = tofloat( tmp )
    delete(tmp)
    MITPW = tofloat( where(isnan_ieee(MITPW),MITPW@_FillValue,MITPW) ) 
    
    bsz = 6
    MITPW := block_avg( MITPW ,bsz)

    tMITPW = MITPW / coldp
    
    tMITPW!0 = "time"
    tMITPW!1 = "lat"
    tMITPW!2 = "lon"
    tMITPW&time = time
    tMITPW&lat  = MIlat
    tMITPW&lon  = MIlon
    
    ddxMITPW = calc_ddx(tMITPW)
    ddyMITPW = calc_ddy(tMITPW)
    MIDDT = calc_ddt(MITPW,6.*3600.)* 86400.
    MIADV =  ( - ub * ddxMITPW  - vb * ddyMITPW ) * coldp * 86400.
    
    MICPR = MIDDT - MIADV

    copy_VarCoords(MITPW,MICPR)
;====================================================================================================
; Reduce Resolution to match LSA data if needed
;====================================================================================================   
    if reduceRes then
        print("ERROR: the reduceRes option is not fully implemented for mk.CP.MIMIC.ncl")
        exit
        iMITPW = MITPW
        delete([/MITPW/])
        ECTPW = area_hi2lores_Wrap(ilon,ilat,iMITPW ,False,1.,SAlon,SAlat,False)
        delete([/iMITPW/])
    end if
;====================================================================================================
; Set coordinates 
; many of these changes may seem weird, but they appear to be 
; neccessary to make the ouput datafile be opened by IDV
;====================================================================================================
    lat := MIlat
    lon := MIlon
    lat@long_name = "Latitude"
    lon@long_name = "Longitude"
    lat@units = "degrees north"
    lon@units = "degrees east"
    
    MITPW!0 = "time"
    MITPW!1 = "lat"
    MITPW!2 = "lon"
    MITPW&time = time
    MITPW&lat  = lat
    MITPW&lon  = lon

    copy_VarCoords(MITPW,MIDDT)
    copy_VarCoords(MITPW,MIADV)
    copy_VarCoords(MITPW,MICPR)
    
    MITPW@long_name = "Total Column Water Vapor"
    MIDDT@long_anem = "MIMIC Eulerian Tendency"
    MIADV@long_name = "MIMIC Advective Tendency"
    MICPR@long_name = "MIMIC Lagrangian Tendency"

    MITPW@units = "mm"
    MIDDT@units = "mm/day"
    MIADV@units = "mm/day"
    MICPR@units = "mm/day"
;====================================================================================================
;====================================================================================================
    printline()
    printMAM(MITPW)
    printMAM(MIDDT)
    printMAM(MIADV)
    printMAM(MICPR)
    printline()
;====================================================================================================
; Write to file
;====================================================================================================    
    if isfilepresent(tfile) then system("rm "+tfile) end if

    outfile = addfile(tfile,"c")
    outfile->MITPW = MITPW
    outfile->MIDDT = MIDDT
    outfile->MIADV = MIADV
    outfile->MICPR = MICPR
;====================================================================================================
;====================================================================================================

    printline()
    print("")
    print("  "+tfile)
    print("")
;====================================================================================================
;====================================================================================================
end

