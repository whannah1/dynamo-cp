load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/custom_functions_DYNAMO_LSF.ncl"
load "$NCARG_ROOT/custom_functions_DYNAMO_ECMWF.ncl"
load "$NCARG_ROOT/custom_functions.ncl"
begin

    fig_type = "png"
    fig_file = "~/Research/DYNAMO/CP/joint.bin.Q.v2"
    
    reduceRes   = False
    dailyAvg    = False
    subLHF      = True
    debug       = False
;====================================================================================================
;====================================================================================================
    sa = "lsan"
    
    if sa.eq. "lsan" then
        lat1 =   0.
        lat2 =   7.
        lon1 =  72.
        lon2 =  79.
    end if
    if sa.eq. "lsas" then
        lat1 =  -7.
        lat2 =   0.
        lon1 =  72.
        lon2 =  79.
    end if
    
    lat1 = -10.
    lat2 =  10.
    lon1 =  50.
    lon2 = 100.
    
        res = True
        res@gsnDraw                         = False
        res@gsnFrame                        = False
        res@vpHeightF                       = 0.5
        res@tmXTOn                          = False
        res@tmXBMinorOn                     = False
        res@tmYLMinorOn                     = False
        res@tmYRMinorOn                     = False
        res@gsnLeftStringFontHeightF        = 0.02
        res@gsnCenterStringFontHeightF      = 0.02
        res@gsnRightStringFontHeightF       = 0.02
        res@tmXBLabelFontHeightF            = 0.015
        res@tmYLLabelFontHeightF            = 0.015
        res@tiXAxisFontHeightF              = 0.02
        res@tiYAxisFontHeightF              = 0.02
        ;res@lbLabelFontheightF             = 0.01
        res@tmXBMajorOutwardLengthF         = 0.0
        res@tmXBMinorOutwardLengthF         = 0.0
        res@tmYLMajorOutwardLengthF         = 0.0
        res@tmYLMinorOutwardLengthF         = 0.0
        res@gsnLeftString                   = ""
        res@gsnCenterString                 = ""
        res@gsnRightString                  = ""
        ;res@tmXBLabelAngleF                    = -50.
        
        lres = res
        lres@xyDashPattern              = 0
        lres@xyLineThicknessF           = 1.
        lres@xyLineColor                = "black"
;====================================================================================================
; Load ECMWF Data
;====================================================================================================   
    tfile = "~/Data/DYNAMO/ECMWF/data/6hour.DYNAMO.ECMWF.CP.nc"
    infile = addfile(tfile,"r")
    iECTPW = infile->ECTPW;(:,{lat1:lat2},{lon1:lon2})
    ;iECDDT = infile->ECDDT;(:,{lat1:lat2},{lon1:lon2})
    iECCPR = infile->ECCPR;(:,{lat1:lat2},{lon1:lon2})
    ;copy_VarCoords(iECTPW,iECDDT)
    copy_VarCoords(iECTPW,iECCPR)
    ECTPW = iECTPW(:,{lat1:lat2},{lon1:lon2})
    ;ECDDT = iECDDT(:,{lat1:lat2},{lon1:lon2})
    ECCPR = iECCPR(:,{lat1:lat2},{lon1:lon2})

;====================================================================================================
; Load ECLHF
;====================================================================================================
if subLHF then
    printline()
    print("Loading ECLHF...")
    infile = addfile("~/Data/DYNAMO/ECMWF/data/12hour.00-12.DYNAMO.ECMWF.LHF.nc","r")
    EClat = infile->lat({lat1:lat2})
    EClon = infile->lon({lon1:lon2})
    t1 = 365 - (31+30+31)
    t2 = 365 - 1

    ECLHF = block_avg( infile->LHF(:,{lat1:lat2},{lon1:lon2}) ,2)
    ECLHF = (/ECLHF/Lv*86400*-1./)
    ECLHF@long_name     = "Evaporation"
    ECLHF@units         = "mm/day"

    ECLHF = dim_rmvmean_n_Wrap(ECLHF,0)


    ECCPR := block_avg(ECCPR,4)
    ECTPW := block_avg(ECTPW,4)

    ECCPR = (/ ECCPR - ECLHF /)
end if
;====================================================================================================
;====================================================================================================
if True then
    infile = addfile("~/Data/DYNAMO/ECMWF/data/EC.q1q2.nc","r")
    EClev = infile->lev;({lev})

    ECQ1 = infile->q1(:,:,{lat1:lat2},{lon1:lon2})
    ECQ2 = infile->q2(:,:,{lat1:lat2},{lon1:lon2})

    ;infile = addfile("~/Data/DYNAMO/ECMWF/data/6hour.DYNAMO.ECMWF.OMEGA.nc","r")
    ;ECW = infile->OMEGA(:t2,:,{lat1:lat2},{lon1:lon2})

    if reduceRes then
        tECQ1 = area_hi2lores_Wrap(ECQ1&lon,ECQ1&lat,ECQ1,False,1.,SAlon,SAlat,False)
        tECQ2 = area_hi2lores_Wrap(ECQ1&lon,ECQ1&lat,ECQ2,False,1.,SAlon,SAlat,False)
        tECW  = area_hi2lores_Wrap( ECW&lon, ECW&lat, ECW,False,1.,SAlon,SAlat,False)
        delete([/ECQ1,ECQ2,ECW/])
        ECQ1 = tECQ1
        ECQ2 = tECQ2
        ECW = tECW
        delete([/tECQ1,tECQ2,tECW/])
    end if
    
    infile = addfile("~/Data/DYNAMO/ECMWF/data/6hour.DYNAMO.ECMWF.Ps.nc","r")
    ps = infile->Ps(:,{lat1:lat2},{lon1:lon2})  
    p = ECQ1&lev*100.
    p@units = "Pa"
    p!0 = "lev"
    p&lev = p
    dp = calc_dP(p,ps)
    Qtop = dim_sum_n(ECQ1(:,{:500},:,:),1)
    Qbot = dim_sum_n(ECQ1(:,{600:},:,:),1)
    
    modeIndex2 = Qbot - Qtop
    modeIndex1 = Qbot + Qtop

    modeIndex1 = (/ modeIndex1/stddev(modeIndex1) /)
    modeIndex2 = (/ modeIndex2/stddev(modeIndex2) /)

    printline()
    printVarSummary(ECQ1)
    printline()
end if
;====================================================================================================
; Bin data
;====================================================================================================
    nvar = 2
    
    binsy = True
    binsy@verbose = False
    binsy@bin_min = -21
    binsy@bin_max =  27
    binsy@bin_spc =   3

    if dailyAvg.or.subLHF then
        binsy@bin_min = -10
        binsy@bin_max =  16
    end if

    binsx = True
    binsx@verbose = False
    binsx@bin_min = 30
    binsx@bin_max = 63
    binsx@bin_spc =  3
    
        ;binsy = True
        ;binsy@verbose = False
        ;binsy@bin_min = -21
        ;binsy@bin_max =  27
        ;binsy@bin_spc =   3

        ;binsx = True
        ;binsx@verbose = False
        ;binsx@bin_min = 30;36
        ;binsx@bin_max = 63;66
        ;binsx@bin_spc =  3
    
    ybin    = ispan(binsy@bin_min,binsy@bin_max,binsy@bin_spc)
    xbin    = ispan(binsx@bin_min,binsx@bin_max,binsx@bin_spc)
    nbiny   = dimsizes(ybin)
    nbinx   = dimsizes(xbin)
    bdim    = (/nvar,nbiny,nbinx/)
    binval = new(bdim,float)
    bincnt = new(bdim,float)
    binavg = new(nvar,float)

    do v = 0,nvar-1
        print("    v = "+v+"    v%nvar = "+v%nvar)
        if v.eq.0 then Vz = modeIndex1 end if
        if v.eq.1 then Vz = modeIndex2 end if
        Vy = ECCPR
        Vx = ECTPW      
        
        if dailyAvg then
            Vx := block_avg(Vx,4)
            Vy := block_avg(Vy,4)
            Vz := block_avg(Vz,4)
        end if
        if subLHF then
            Vz := block_avg(Vz,4)
        end if
        
        tmp = bin_ZbyYX(Vz,Vy,Vx,binsy,binsx,0)
        
        binval(v,:,:) = tmp
        bincnt(v,:,:) = tmp@pct
        binavg(v) = avg(Vx)

        delete([/Vz,Vy,Vx,tmp/])
    end do

    printline()
    bincnt!0 = "var"
    bincnt!1 = "ybin"
    bincnt!2 = "xbin"
    bincnt&ybin = ybin
    bincnt&xbin = xbin
;====================================================================================================
;====================================================================================================
    wks = gsn_open_wks(fig_type,fig_file)
    ;plot = new(nvar*2,graphic)
    plot = new(nvar,graphic)
        cres = res
        cres@cnFillOn           = True
        cres@cnLinesOn          = False
        ;cres@lbOrientation     = "vertical"
        ;cres@cnFillPalette     = ""
        cres@tiYAxisString      = "LCT - LHF [mm day~S~-1~N~]"
        cres@tiXAxisString      = "CWV [mm]"
        cres@gsnRightString     = ""
        cres@pmLabelBarOrthogonalPosF   = 0.02
        cres@tiXAxisOffsetYF            = .115
        
        cres@cnLevelSelectionMode   = "ExplicitLevels"
        ;cres@cnLevels               = ispan(-80,80,5);/1e2
        cres@cnLevels               = ispan(-60,60,5)/1e2

        cres@gsnLeftString      = "ECMWF"
        cres@lbLabelFontHeightF = 0.015
        
    do v = 0,nvar-1
        p1 = v
        p2 = v+nvar
        ;if v.eq.0 then cres@gsnLeftString = "ECMWF" end if
        ;plot(p1) = gsn_csm_contour(wks,binval(v,:,:),cres)
        ;plot(p2) = gsn_csm_contour(wks,bincnt(v,:,:),cres)
        ;overlay(plot(p1) , gsn_csm_xy(wks,(/-1e3,1e3/),(/0.,0./),lres))
        ;overlay(plot(p2) , gsn_csm_xy(wks,(/-1e3,1e3/),(/0.,0./),lres))

        cres@gsnRightString     = "M~B~"+(v+1)+"~N~"
        
        plot(v) = gsn_csm_contour(wks,binval(v,:,:),cres)
            tres = res
            tres@cnFillOn           = False
            tres@cnLinesOn          = True
            tres@cnLineLabelsOn     = False
            tres@cnInfoLabelOn      = False
            tres@cnLineThicknessF   = 3.
            tres@gsnContourZeroLineThicknessF   = 6.
            tres@gsnContourNegLineDashPattern   = 1
        overlay(plot(v) , gsn_csm_contour(wks,bincnt(v,:,:),tres))
        delete(tres)
        
        overlay(plot(v) , gsn_csm_xy(wks,(/-1e3,1e3/),(/0.,0./),lres))

        overlay(plot(v) , gsn_csm_xy(wks,(/1.,1./)*avg(ECTPW),(/-1e3,1e3/),lres)) 
        
    end do
    
    
    
;====================================================================================================
; Finalize Figure
;====================================================================================================
        pres = True
        pres@gsnFrame                           = True
        pres@gsnPanelYWhiteSpacePercent         = 5
        pres@amJust                             = "TopLeft"
        pres@gsnPanelFigureStringsFontHeightF   = 0.01
        pres@gsnPanelFigureStrings              = (/"a","b","c","d","e","f"/)

    gsn_panel(wks,plot,(/1,dimsizes(plot)/),pres)

  
    print("")
    print(" "+fig_file+"."+fig_type)
    print("")
    if fig_type.eq."png" then system("convert -trim "+fig_file+"."+fig_type+"   "+fig_file+"."+fig_type) end if

;====================================================================================================
;====================================================================================================
end
