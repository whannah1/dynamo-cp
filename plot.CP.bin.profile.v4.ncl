; Bin a height resolved variable against the Lagrangian tendnecy
; v4 - similar to v2, but only bins on TPW and 
; conditionally smaples based on the Lagrangian tendency
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/custom_functions_DYNAMO_LSF.ncl"
load "$NCARG_ROOT/custom_functions_DYNAMO_ECMWF.ncl"
load "$NCARG_ROOT/custom_functions.ncl"
begin

    fig_type = "png"
    fig_file = "~/Research/DYNAMO/CP/CP.bin.profile.v4"
    
    reduceRes   = False
    recalc      = True
    debug       = False
    
    rmv_lhf     = False
;====================================================================================================
;====================================================================================================       

    sa = "lsan"
    
    if sa.eq. "lsan" then
        lat1 =   0.
        lat2 =   7.
        lon1 =  72.
        lon2 =  79.
    end if
    if sa.eq. "lsas" then
        lat1 =  -7.
        lat2 =   0.
        lon1 =  72.
        lon2 =  79.
    end if
    
    lat1 = -10.
    lat2 =  10.
    lon1 =  50.
    lon2 = 100.
    
    lev = (/100,150,200,250,300,400,500,600,700,800,850,900,925,950,1000/)
    
    fig_type@wkWidth  = 2048
    fig_type@wkHeight = 2048
    
        res = True
        res@gsnDraw                         = False
        res@gsnFrame                        = False
        res@vpHeightF                       = 0.4
        res@tmXTOn                          = False
        res@tmXBMinorOn                     = False
        res@tmYLMinorOn                     = False
        res@tmYRMinorOn                     = False
        res@gsnLeftStringFontHeightF        = 0.02
        res@gsnCenterStringFontHeightF      = 0.02
        res@gsnRightStringFontHeightF       = 0.02
        res@tmXBLabelFontHeightF            = 0.015
        res@tmYLLabelFontHeightF            = 0.015
        res@tiXAxisFontHeightF              = 0.015
        res@tiYAxisFontHeightF              = 0.015
        res@tmXBMajorOutwardLengthF         = 0.0
        res@tmXBMinorOutwardLengthF         = 0.0
        res@tmYLMajorOutwardLengthF         = 0.0
        res@tmYLMinorOutwardLengthF         = 0.0
        res@gsnLeftString                   = ""
        res@gsnCenterString                 = ""
        res@gsnRightString                  = ""
        ;res@tmXBLabelAngleF                = -50.
        res@gsnYAxisIrregular2Linear        = True
        
        lres = res
        lres@xyDashPattern              = 0
        lres@xyLineThicknessF           = 3.
        lres@xyLineColor                = "black"
;====================================================================================================
; Load ECMWF Data
;====================================================================================================   
    if reduceRes then
        ifile = "~/Data/DYNAMO/ECMWF/data/6hour.DYNAMO.ECMWF.CP.SAres.nc"
    else
        ifile = "~/Data/DYNAMO/ECMWF/data/6hour.DYNAMO.ECMWF.CP.nc"
    end if
    infile = addfile(ifile,"r")

    nblk = 4
    ECTPW = infile->ECTPW(:,{lat1:lat2},{lon1:lon2})
    ECDDT = infile->ECDDT(:,{lat1:lat2},{lon1:lon2})
    ECADV = infile->ECADV(:,{lat1:lat2},{lon1:lon2})
    ECCPR = infile->ECCPR(:,{lat1:lat2},{lon1:lon2})
;====================================================================================================
;====================================================================================================   
t2 = (31+30+31)*4-1
if True then
    infile = addfile("~/Data/DYNAMO/ECMWF/data/EC.q1q2.nc","r")
    EClev = infile->lev({lev})
    
    ECQ1 = infile->q1(:,{lev},{lat1:lat2},{lon1:lon2})
    ECQ2 = infile->q2(:,{lev},{lat1:lat2},{lon1:lon2})
    
    infile = addfile("~/Data/DYNAMO/ECMWF/data/6hour.DYNAMO.ECMWF.OMEGA.nc","r")
    ECW = infile->OMEGA(:t2,:,{lat1:lat2},{lon1:lon2})
    
    if reduceRes then
        tECQ1 = area_hi2lores_Wrap(ECQ1&lon,ECQ1&lat,ECQ1,False,1.,SAlon,SAlat,False)
        tECQ2 = area_hi2lores_Wrap(ECQ1&lon,ECQ1&lat,ECQ2,False,1.,SAlon,SAlat,False)
        tECW  = area_hi2lores_Wrap( ECW&lon, ECW&lat, ECW,False,1.,SAlon,SAlat,False)
        delete([/ECQ1,ECQ2,ECW/])
        ECQ1 = tECQ1
        ECQ2 = tECQ2
        ECW = tECW
        delete([/tECQ1,tECQ2,tECW/])
    end if

    printline()
    printVarSummary(ECQ1)
    printline()
end if
;====================================================================================================
; Bin the data
;====================================================================================================

    ngrp = 2
    nvar = 1

    bins1 = True
    bins1@verbose = False
    bins1@bin_min = 24
    bins1@bin_max = 64
    bins1@bin_spc =  2

    bins2 = True
    bins2@verbose = False   
    bins2@bin_min = -20
    bins2@bin_max =  20
    bins2@bin_spc =   2
    
    num_bin = dimsizes(ispan(bins1@bin_min,bins1@bin_max,bins1@bin_spc))
    xbin    = new((/ngrp,num_bin/),float)
    xbin(0,:) = ispan(bins1@bin_min,bins1@bin_max,bins1@bin_spc) 
    ;xbin(1,:) = ispan(bins2@bin_min,bins2@bin_max,bins2@bin_spc) 
    xbin(1,:) = xbin(0,:)
    num_lev = dimsizes(lev)
    bdim    = (/ngrp,nvar,num_lev,num_bin/)
    binval  = new(bdim,float)
    binpct  = new(bdim,float)
    
    tstr = new(ngrp,string)
    xstr = new(ngrp,string)
    
    do p = 0,ngrp-1
    do v = 0,nvar-1
    
        print("    v = "+v+"    v%nvar = "+v%nvar)
        
        bins = bins1
        Vx = ECTPW 
        
        do k = 0,num_lev-1  
            
            Vy = ECQ1(:,k,:,:)

            if p.eq.0 then Vy = where(ECCPR.gt.0,Vy,Vy@_FillValue)  end if
            if p.eq.1 then Vy = where(ECCPR.lt.0,Vy,Vy@_FillValue)  end if
            
            tmp = bin_YbyX(Vy,Vx,bins)
            binval(p,v,k,:) = tmp
            binpct(p,v,k,:) = tmp@pct
            delete([/Vy,tmp/])
        end do
        delete([/Vx/])
        
        xstr(p) = "CWV [mm]" 
        
        tstr(v) = "ECMWF Q1" 

    end do
    end do
    
    binval!0 = "group"
    binval!1 = "var"
    binval!2 = "lev"
    binval!3 = "bin"
    binval&lev = lev
    
    printline()
;====================================================================================================
; Create plot
;====================================================================================================
    wks = gsn_open_wks(fig_type,fig_file)
    plot = new(ngrp*nvar,graphic)

        cres = res
        cres@cnFillOn               = True
        cres@cnLinesOn              = False
        cres@trYReverse             = True
        cres@cnLevelSelectionMode   = "ExplicitLevels"
        cres@tmYROn                 = False
        ;cres@lbOrientation          = "vertical"
        ;cres@lbLabelBarOn          = False
        cres@pmLabelBarOrthogonalPosF   = 0.02
        cres@tiXAxisOffsetYF            = .085
        
        dres = lres
        dres@tiYAxisString          = "[% of Observations]"
        dres@xyDashPattern          = 1
    
    
    do p = 0,ngrp-1
    do v = 0,nvar-1
        
        tval     = binval(p,v,:,:)
        tval!1   = "bin"
        tval&bin = xbin(p,:)
        
        ;cres@gsnCenterString   = tstr(v)
        if p.eq.0 then cres@gsnCenterString   = "LT > 0" end if
        if p.eq.1 then cres@gsnCenterString   = "LT < 0" end if
        cres@tiXAxisString      = xstr(p)
        cres@tiYAxisString      = "Pressure [hPa]"
    
        cres@gsnLeftString      = "ECMWF"
        cres@gsnRightString     = "Q~B~1~N~ [K day~S~-1~N~]"
        cres@lbLabelFontHeightF = 0.015
        cres@trYMaxF            = EClev(13)

        ;cres@cnLevels = tofloat( ispan(-30,30,1) )
        
        cres@cnLevels = tofloat( ispan(-30,30,1))

        i = p+v*ngrp
        plot(i) = gsn_csm_contour(wks,tval,cres)    
        
        overlay(plot(i) , gsn_csm_xy2(wks,xbin(p,:),xbin(p,:)*0.,binpct(p,v,0,:),lres,dres))
        
        overlay(plot(i) , gsn_csm_xy(wks,(/0.,0./),(/-1e3,1e3/),lres))
        
        delete(tval)
        delete(cres@cnLevels)
    end do
    end do
;====================================================================================================
; Finalize Figure
;====================================================================================================
        pres = True
        pres@gsnPanelYWhiteSpacePercent         = 5
        pres@amJust                             = "TopLeft"
        pres@gsnPanelFigureStringsFontHeightF   = 0.01
        pres@gsnPanelFigureStrings              = (/"a","b","c","d","e","f"/)

    gsn_panel(wks,plot,(/nvar,ngrp/),pres)
    ;gsn_panel(wks,plot,(/ngrp,nvar/),pres)
  
    print("")
    print(" "+fig_file+"."+fig_type)
    print("")
    if fig_type.eq."png" then system("convert -trim "+fig_file+"."+fig_type+"   "+fig_file+"."+fig_type) end if
;====================================================================================================
;====================================================================================================
end
