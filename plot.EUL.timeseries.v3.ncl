; Timeseries of 6t-hourly data at Gan
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/contrib/time_axis_labels.ncl"
load "$NCARG_ROOT/custom_functions_DYNAMO_LSF.ncl"
load "$NCARG_ROOT/custom_functions_DYNAMO_ECMWF.ncl"
load "$NCARG_ROOT/custom_functions.ncl"
begin

	fig_type = "png"
	fig_file = "~/Research/DYNAMO/CP/EUL.timeseries.v3"

	daily = False

;====================================================================================================
;====================================================================================================		
	sa = "lsan"
	
	if sa.eq. "lsan" then
		lat1 =   0.
		lat2 =   7.
		lon1 =  72.
		lon2 =  79.
	end if
	if sa.eq. "lsas" then
		lat1 =  -7.
		lat2 =   0.
		lon1 =  72.
		lon2 =  79.
	end if
	
	wks = gsn_open_wks(fig_type,fig_file)
	plot = new(1,graphic)
		res = True
		res@gsnDraw                         = False
		res@gsnFrame                        = False
		res@vpHeightF						= 0.15
		res@tmXTOn                          = False
		res@gsnLeftStringFontHeightF        = 0.01
		res@gsnCenterStringFontHeightF    	= 0.01
		res@gsnRightStringFontHeightF       = 0.01
		res@tmXBLabelFontHeightF            = 0.005
		res@tmYLLabelFontHeightF            = 0.005
		res@tiXAxisFontHeightF				= 0.01
		res@tiYAxisFontHeightF				= 0.01
		res@tmXBMajorOutwardLengthF         = 0.0
		res@tmXBMinorOutwardLengthF         = 0.0
		res@tmYLMajorOutwardLengthF         = 0.0
		res@tmYLMinorOutwardLengthF         = 0.0
		res@gsnLeftString                   = ""
		res@gsnCenterString              	= ""
		res@gsnRightString                  = ""
		res@tmXBLabelAngleF					= -50.
		
		lres = res
		lres@xyDashPattern              = 0
		lres@xyLineThicknessF           = 1.
		lres@xyLineColor                = "black"
;====================================================================================================
; Load Sounding Array Data
;====================================================================================================
	bsz = 2
	ver = "2b"
	if ver.eq."2a" then ifile = "~/Data/DYNAMO/LSA/data/v2a/dynamo_basic_v2a_2011all.nc" end if
	if ver.eq."2b" then ifile = "~/Data/DYNAMO/LSA/data/v2b/dynamo_basic_v2b_2011all.nc" end if
	infile = addfile(ifile,"r")
	ps  = infile->ps(:,{lat1:lat2},{lon1:lon2})
	p   = infile->level
	p!0 = "lev"
	p&lev = p
	dp  = calc_dP(p,ps) *100.
	wmr = infile->q(:,:,{lat1:lat2},{lon1:lon2})
	wmr = (/wmr/1000./)
	q 	= wmr/(1.+wmr)
	copy_VarCoords(wmr,q)
	
	bq  = block_avg( q,bsz)
	bdp = block_avg(dp,bsz)

	
	
	SATPWa1 = block_avg( dim_avg_n( dim_sum_n( q*dp/g,1) ,(/1,2/)) ,bsz)

	SATPWa2 = dim_avg_n( dim_sum_n( bq*bdp/g,1) ,(/1,2/))

	SAnt = dimsizes(SATPWa1)
	SAtime = tofloat(ispan(0,SAnt-1,1))/4. + 273.
	printline()
	;printVarSummary(SATPWa1)
	printMAM(SATPWa2)
	delete(p)
;====================================================================================================
;====================================================================================================
	ver = "2b"
	if ver.eq."2a" then ifile = "~/Data/DYNAMO/LSA/data/v2a/dynamo_basic_v2a_2011all.nc" end if
	if ver.eq."2b" then ifile = "~/Data/DYNAMO/LSA/data/v2b/dynamo_basic_v2b_2011all.nc" end if
	infile = addfile(ifile,"r")
	ps  = infile->ps(:,{lat1:lat2},{lon1:lon2})
	p   = infile->level
	p!0 = "lev"
	p&lev = p
	dp  = calc_dP(p,ps) *100.
	
	wmr = infile->q(:,:,{lat1:lat2},{lon1:lon2})
	wmr = (/wmr/1000./)
	q 	= wmr/(1.+wmr)
	copy_VarCoords(wmr,q)
	
	bq  = block_avg( q,bsz)
	bdp = block_avg(dp,bsz)

	SATPWb1 = block_avg( dim_avg_n( dim_sum_n( q*dp/g,1) ,(/1,2/)) ,bsz) 
	
	SATPWb2 = dim_avg_n( dim_sum_n( bq*bdp/g,1) ,(/1,2/))
	
	printMAM(SATPWb2)
;====================================================================================================
; Load MIMIC Data
;====================================================================================================
	ifile  = "~/Data/DYNAMO/MIMIC/cimss_morphed_tpw_DYNAMO_2011.nc"
	infile = addfile(ifile,"r")
	tmp = infile->tpw(:,{lat1:lat2},{lon1:lon2}) 
	iMITPW = new(dimsizes(tmp),float)
	iMITPW = tofloat( tmp )
	delete(tmp)
	iMITPW = tofloat( where(isnan_ieee(iMITPW),iMITPW@_FillValue,iMITPW) ) 
	bsz = 6
	MITPW = block_avg( dim_avg_n_Wrap(iMITPW,(/1,2/)) ,bsz)
	printline()
	;printVarSummary(MITPW)
	printMAM(MITPW)
;====================================================================================================
; Load ECMWF CWV Data
;====================================================================================================
	ifile = "~/Data/DYNAMO/ECMWF/data/6hour.DYNAMO.ECMWF.CWV.nc"
	infile = addfile(ifile,"r")
	ECTPW = new(SAnt,float)
	ECTPW = dim_avg_n( infile->CWV(:,{lat1:lat2},{lon1:lon2})  ,(/1,2/))
	printline()
	;printVarSummary(ECTPW)
	printMAM(ECTPW)
;====================================================================================================
; Create Plot 
;====================================================================================================
	bsz = 4

	time = SAtime(::bsz)
	time@units = "days since 2011-01-01"
	cdtime = cd_calendar(time,2)
	tmres = True
	tmres@ttmFormat 		= "%D %c, 2011"
	tmres@ttmAxis   		= "XB"
	tmres@ttmMinorStride	= 4/bsz
	tmres@ttmMajorStride	= 5*4/bsz
	time_axis_labels(time,res,tmres)
	
		clr = (/"blue","cyan","red","orange","green","palegreen"/)
		tres = res
		tres@xyLineThicknessF 	= 4.
		tres@gsnCenterString   	= "Eulerian CWV Tendency (smoothed daily mean)"
		;tres@trYMinF         	= 30.
		;tres@trYMaxF        	= 65.
		tres@trXMinF			= min(time)
		tres@trXMaxF			= max(time)
		;tres@xyLineColor    	= "black"
		tres@xyDashPattern    	= 0
		tres@xyLineColors		= clr
		;tres@xyDashPatterns  	= (/0,0,0,0,0,0,0/)
		tres@tiXAxisString   	= ""
		tres@tiYAxisString   	= "[mm day~S~-1~N~]"
		
;delete(SATPWb2)		
;SATPWb2 = MITPW
	
	if False then
		dt 	= 6*3600. *bsz
		MIDDT   = calc_ddt( block_avg(MITPW  ,bsz) ,dt)*86400.
		ECDDT   = calc_ddt( block_avg(ECTPW  ,bsz) ,dt)*86400.
		SADDTa1 = calc_ddt( block_avg(SATPWa1,bsz) ,dt)*86400.
		SADDTa2 = calc_ddt( block_avg(SATPWa2,bsz) ,dt)*86400.
		SADDTb1 = calc_ddt( block_avg(SATPWb1,bsz) ,dt)*86400.
		SADDTb2 = calc_ddt( block_avg(SATPWb2,bsz) ,dt)*86400.
	else
		dt 	= 6*3600.
		MIDDT   = block_avg( calc_ddt( MITPW   ,dt)*86400. ,bsz)
		ECDDT   = block_avg( calc_ddt( ECTPW   ,dt)*86400. ,bsz)
		SADDTa1 = block_avg( calc_ddt( SATPWa1 ,dt)*86400. ,bsz)
		SADDTa2 = block_avg( calc_ddt( SATPWa2 ,dt)*86400. ,bsz)
		SADDTb1 = block_avg( calc_ddt( SATPWb1 ,dt)*86400. ,bsz)
		SADDTb2 = block_avg( calc_ddt( SATPWb2 ,dt)*86400. ,bsz)
	end if
	
	;nsmooth = 0
	;do n = 0,nsmooth-1 ECDDT   = wgt_runave_Wrap(ECDDT  , (/0.25,0.5,0.25/), 0) end do
	;do n = 0,nsmooth-1 MIDDT   = wgt_runave_Wrap(MIDDT  , (/0.25,0.5,0.25/), 0) end do
	;do n = 0,nsmooth-1 SADDTa1 = wgt_runave_Wrap(SADDTa1, (/0.25,0.5,0.25/), 0) end do
	;do n = 0,nsmooth-1 SADDTa2 = wgt_runave_Wrap(SADDTa2, (/0.25,0.5,0.25/), 0) end do
	;do n = 0,nsmooth-1 SADDTb1 = wgt_runave_Wrap(SADDTb1, (/0.25,0.5,0.25/), 0) end do
	;do n = 0,nsmooth-1 SADDTb2 = wgt_runave_Wrap(SADDTb2, (/0.25,0.5,0.25/), 0) end do
		
	;plot(0) = gsn_csm_xy(wks,time,(/ECDDT,MIDDT,SADDTa1,SADDTa2,SADDTb1,SADDTb2/),tres)
	plot(0) = gsn_csm_xy(wks,time,(/SADDTa1,SADDTa2,SADDTb1,SADDTb2/),tres)	
	;plot(0) = gsn_csm_xy(wks,time,(/ECDDT,MIDDT,SADDTb2/),tres)
	
	;plot(0) = gsn_csm_xy(wks,SAtime,(/ECTPW,MITPW,SATPWb2/),tres)

	overlay(plot(0) , gsn_csm_xy(wks,(/-1e3,1e3/),(/0.,0./),lres))

;====================================================================================================
; Finalize Figure
;====================================================================================================
		pres = True
		pres@gsnFrame                           = False
		pres@txString                           = ""
		;pres@gsnPanelBottom                    = 0.1 
		;pres@amJust                            = "TopLeft"
		;pres@gsnPanelFigureStringsFontHeightF  = 0.015
		;pres@gsnPanelFigureStrings             = (/"a","b","c","d"/) 

	gsn_panel(wks,plot,(/dimsizes(plot),1/),pres)

	legend = create "Legend" legendClass wks
		"lgAutoManage"              : False
		"vpXF"                      : 0.5
		"vpYF"                      : 0.5
		"vpWidthF"                  : 0.1
		"vpHeightF"                 : 0.1
		"lgPerimOn"                 : True
		"lgLabelsOn"                : True
		"lgLineLabelsOn"            : False
		"lgItemCount"               : 6
		"lgLabelStrings"            : (/"ECMWF","MIMIC","LSA a1","LSA a2","LSA b1","LSA b2"/)
		"lgLabelFontHeightF"        : 0.01
		"lgDashIndex"             	: 0
		"lgLineThicknessF"			: 4.
		"lgLineColors"              : clr
		"lgMonoDashIndex"			: True
	end create

	draw(legend)
	frame(wks)
  
	print("")
	print(" "+fig_file+"."+fig_type)
	print("")
	if fig_type.eq."png" then system("convert -trim "+fig_file+"."+fig_type+"   "+fig_file+"."+fig_type) end if

;====================================================================================================
;====================================================================================================
end
