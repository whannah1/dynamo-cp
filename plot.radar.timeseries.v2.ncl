; plots a timeseries of radar data
; v1 - only radar data
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/contrib/time_axis_labels.ncl"
load "$NCARG_ROOT/custom_functions.ncl"
begin

	src = "SPOL"

	fig_type = "png"
	fig_file = "~/Research/DYNAMO/CP/radar.timeseries.v2"
	
	reduceRes	= False
	debug 		= False
	daily       = False
	
	dh = 3.
	
	nblk = 1
	if daily then nblk = nblk * 4 end if
;====================================================================================================
;====================================================================================================		    
	nh = toint( 24/dh )
	
	sa = "lsan"
	if sa.eq. "lsan" then
		lat1 =   0.
		lat2 =   7.
		lon1 =  72.
		lon2 =  79.
	end if
	if sa.eq. "lsas" then
		lat1 =  -7.
		lat2 =   0.
		lon1 =  72.
		lon2 =  79.
	end if
	
	lat1 = 0.5
	lon1 = 73.5
	
	ypts = (/lat1,lat1,lat1,lat1/)
	xpts = (/lon1,lon1,lon1,lon1/)
	
	wks = gsn_open_wks(fig_type,fig_file)
    plot = new(3,graphic)
    num_p = dimsizes(plot)
		res = True;setres_default()
        res@gsnDraw  = False
        res@gsnFrame = False
        res@tmXBMajorOutwardLengthF         = 0.0
        res@tmXBMinorOutwardLengthF         = 0.0
        res@tmYLMajorOutwardLengthF         = 0.0
        res@tmYLMinorOutwardLengthF         = 0.0
		res@vpHeightF						= 0.2
		res@tmXTOn                          = False
		res@tmYLMinorOn						= False
		res@tmYRMinorOn						= False

		res@tmXBLabelAngleF  				= -90.
		
		lres = res
		lres@xyDashPattern              = 0
		lres@xyLineThicknessF           = 6.
		lres@xyLineColor                = "black"
		
		cres = res
		cres@cnFillOn 				= True
		cres@cnLinesOn 				= False
		cres@cnSpanFillPalette		= True
		cres@trYReverse				= True
;====================================================================================================
; Load Radar Data
;====================================================================================================
	radsrc = "SPOL"
    if radsrc.eq."SPOL" then ifile = "~/Data/DYNAMO/RADAR/SPOL/spol_radar_gridded.v2.alt.OND_2011_DYNAMO.nc" end if
    infile = addfile(ifile,"r")

    rtime = infile->time

    ctime = cd_calendar(rtime(0),0)
    hr = ctime(0,3)
    mt = ctime(0,4)
    sc = ctime(0,5)

    rtime = (/ rtime - rtime(0) /)
    ;rtime@units = "hours since 2011-10-01 00:00:00"
    rtime@units = "hours since 2011-10-01 "+hr+":"+mt+":"+sc


    rlat = infile->lat
    rlon = infile->lon
    nlat = dimsizes(rlat)
    nlon = dimsizes(rlon)
    lat1 = min(rlat)
    lat2 = max(rlat)
    lon1 = min(rlon)
    lon2 = max(rlon)
    
    ;rain_total = infile->rain_total
    ;rain_other = infile->rain_other
    ;rain_strat = infile->rain_strat
    ;rain_deep  = infile->rain_deep
    rain_cong  = infile->rain_cong ;+ infile->rain_shal
    rain_shal  = infile->rain_shal

    frac_shal  = infile->frac_shal
    frac_cong  = infile->frac_cong ;+ frac_shal
    frac_deep  = infile->frac_deep
    frac_strat = infile->frac_strat
    frac_other = infile->frac_other
    
    eth10      = infile->top_10_dBZ
    eth20      = infile->top_20_dBZ

    if True then 
        ;rtime := rtime(::24)
        if daily then rtime := rtime(::4) end if
        ;rtime := block_avg( rtime ,nblk)

        rain_shal  := block_avg( dim_avg_n_Wrap( rain_shal  ,(/1,2/)) ,nblk)
        rain_cong  := block_avg( dim_avg_n_Wrap( rain_cong  ,(/1,2/)) ,nblk)

        frac_shal  := block_avg( dim_avg_n_Wrap( frac_shal  ,(/1,2/)) ,nblk)
        frac_cong  := block_avg( dim_avg_n_Wrap( frac_cong  ,(/1,2/)) ,nblk)
        frac_deep  := block_avg( dim_avg_n_Wrap( frac_deep  ,(/1,2/)) ,nblk)
        frac_strat := block_avg( dim_avg_n_Wrap( frac_strat ,(/1,2/)) ,nblk)
        frac_other := block_avg( dim_avg_n_Wrap( frac_other ,(/1,2/)) ,nblk)
    end if

    if False then 
        rain_shal = (/ rain_shal / stddev(rain_shal) /)
        rain_cong = (/ rain_cong / stddev(rain_cong) /)

        frac_shal = (/ frac_shal / stddev(frac_shal) /)
        frac_cong = (/ frac_cong / stddev(frac_cong) /)
    end if

    frac_shal = (/ frac_shal * 100. /)
    frac_cong = (/ frac_cong * 100. /)
;====================================================================================================
; Load ECMWF CWV Data
;====================================================================================================
	ifile = "~/Data/DYNAMO/ECMWF/data/6hour.DYNAMO.ECMWF.CP.nc"
    infile = addfile(ifile,"r")
    ECCPR = dim_avg_n_Wrap( infile->ECCPR(:,{ypts},{xpts}) ,(/1,2/))

    ifile = "~/Data/DYNAMO/ECMWF/data/6hour.DYNAMO.ECMWF.CWV.nc"
	infile = addfile(ifile,"r")
	ECTPW = dim_avg_n_Wrap( infile->CWV(:,{ypts},{xpts}) ,(/1,2/))
	
	nt = (31+30+31)*4
    ECtime = tofloat(ispan(0,nt-1,1))*6.
    ECtime@units = "hours since 2011-10-01 00:00:00"

    nblk = 1
    if daily then nblk = 4 end if
    ECTPW  := block_avg( ECTPW ,nblk)
    ECCPR  := block_avg( ECCPR ,nblk)
    ECtime := block_avg( ECtime ,nblk)
;====================================================================================================
; Plot radar data
;====================================================================================================
    cdtime := cd_calendar(rtime,2)
    tmres = True
    tmres@ttmFormat         = "%D %c"
    tmres@ttmAxis           = "XB"
    tmres@ttmMinorStride    = 1
    tmres@ttmMajorStride    = 4
    if daily then tmres@ttmMajorStride = 2 end if
    time_axis_labels(rtime,lres,tmres)

	clr = (/"red","blue"/);,"green","purple","orange"/)

		lres@trXMinF			= min(rtime)
		lres@trXMaxF			= max(rtime)
		lres@tiXAxisString  	= ""
		;lres@xyLineThicknessF	= 4.
		lres@gsnRightString		= ""
		lres@gsnLeftString   	= ""
		lres@xyLineColors 		= clr
		lres@trXMinF            = min(ECtime) + 31*24
        ;lres@trXMaxF            = max(ECtime) 
        lres@trXMaxF            = min(ECtime) + (31+25)*24
        ;lres@trYMinF			= 0.
		;lres@trYMaxF			= 100.		
	

    ;plot(0) = gsn_csm_xy(wks,rtime,(/frac_shal,frac_cong,frac_deep,frac_strat,frac_other/),lres)
	;plot(0) = gsn_csm_xy(wks,rtime,(/frac_shal,frac_cong/),lres)

        lres@gsnLeftString      = "Rainfall"
        lres@tiYAxisString      = "[mm day~S~-1~N~]"
    plot(0) = gsn_csm_xy(wks,rtime,(/rain_shal,rain_cong/),lres)

        lres@gsnLeftString      = "Area Fraction"
        lres@tiYAxisString      = "[%]"
    plot(1) = gsn_csm_xy(wks,rtime,(/frac_shal,frac_cong/),lres)
	
;====================================================================================================
; Plot ECMWF data
;====================================================================================================
    ;cdtime := cd_calendar(ECtime,2)
    ;tmres = True
    ;tmres@ttmFormat         = "%D %c, 2011"
    ;tmres@ttmAxis           = "XB"
    ;tmres@ttmMinorStride    = 1
    ;tmres@ttmMajorStride    = 5    
    ;time_axis_labels(rtime,lres,tmres)

		lres2 = lres
		lres2@xyMonoLineColor	= True
		lres2@xyLineThicknesses	= (/8.,1./)
        lres2@gsnLeftString      = "TPW"
		lres2@tiYAxisString  	= "[mm]"
        lres3 = lres2
        lres3@gsnLeftString      = ""
        lres2@xyLineColor        = "black"
        lres3@xyLineColor        = "green"
        
        lres2@trYMinF			= 50-15
		lres2@trYMaxF			= 50+15
		lres3@trYMinF			= -35
		lres3@trYMaxF			=  35
		
		if daily then 
            lres2@trYMinF			= 50-15
		    lres2@trYMaxF			= 50+15
		    lres3@trYMinF			= -20
		    lres3@trYMaxF			=  20	
		end if

	;plot(0) = gsn_csm_xy2(wks,ECtime,(/frac_shal,frac_cong,ECCPR/),ECTPW,lres,lres2)
    ;plot(0) = gsn_csm_xy3(wks,ECtime,(/frac_shal,frac_cong/),ECTPW,ECCPR,lres,lres2,lres3)

    plot(2) = gsn_csm_xy2(wks,ECtime,(/ECTPW,ECTPW*0+50/),(/ECCPR/),lres2,lres3)

       
    if .not.daily then 
            ;lres2@xyDashPattern   = 1
            ;lres3@xyDashPattern   = 1
            lres2@xyMonoLineThickness = True
            lres3@xyMonoLineThickness = True
            lres2@xyLineThicknessF = 4.
            lres3@xyLineThicknessF = 4.
        tblk = 4
        overlay( plot(2) , gsn_csm_xy2(wks,block_avg(ECtime,tblk),block_avg(ECTPW,tblk),block_avg(ECCPR,tblk),lres2,lres3)  )
    end if
    
;====================================================================================================
; Adding shading when Revelle was off-site
;====================================================================================================
        gres = True
        gres@gsFillColor        = "lightgray"
        gres@gsFillOpacityF     = 0.2

    yy = (/-1e3,1e3/)

    xx := ( (/31+7,31+12/) - 1 )*24.
    xshade := (/xx(0),xx(0),xx(1),xx(1),xx(0)/)
    yshade := (/yy(0),yy(1),yy(1),yy(0),yy(0)/)
    dum1 = new(num_p,graphic)
    do p = 0,num_p-1
        dum1(p) = gsn_add_polygon(wks,plot(p),xshade,yshade,gres)
    end do

    xx := ( (/31+15,31+19/) - 1 )*24.
    xshade := (/xx(0),xx(0),xx(1),xx(1),xx(0)/)
    yshade := (/yy(0),yy(1),yy(1),yy(0),yy(0)/)
    dum2 = new(num_p,graphic)
    do p = 0,num_p-1
        dum2(p) = gsn_add_polygon(wks,plot(p),xshade,yshade,gres)
    end do

;====================================================================================================
; Finalize Figure
;====================================================================================================
		pres = True
		pres@gsnPanelYWhiteSpacePercent			= 5
		pres@gsnFrame                      		= False
		pres@amJust                       		= "TopLeft"
		pres@gsnPanelFigureStringsFontHeightF	= 0.01
		pres@gsnPanelFigureStrings          	= (/"a","b","c","d","e","f"/)

	layout = (/dimsizes(plot),1/)
	
	gsn_panel(wks,plot,layout,pres)

	legend1 = create "Legend" legendClass wks
		"lgAutoManage"              : False
		"vpXF"                      : 0.72;0.65
		"vpYF"                      : 0.62;0.68
		"vpWidthF"                  : 0.08
		"vpHeightF"                 : 0.04
		"lgPerimOn"                 : True
		"lgLabelsOn"                : True
		"lgLineLabelsOn"            : False
		"lgItemCount"               : 2
		"lgLabelStrings"            : (/"shallow","congestus"/)
		"lgLabelFontHeightF"        : 0.01
		"lgDashIndex"             	: 0
		"lgLineThicknessF"			: 6.
		"lgLineColors"              : clr
		"lgMonoDashIndex"			: True
	end create
	draw(legend1)

    legend2 = create "Legend" legendClass wks
        "lgAutoManage"              : False
        "vpXF"                      : 0.65
        "vpYF"                      : 0.35
        "vpWidthF"                  : 0.08
        "vpHeightF"                 : 0.03
        "lgPerimOn"                 : True
        "lgLabelsOn"                : True
        "lgLineLabelsOn"            : False
        "lgItemCount"               : 2
        "lgLabelStrings"            : (/"TPW","LT"/)
        "lgLabelFontHeightF"        : 0.01
        "lgDashIndex"               : 0
        "lgLineThicknessF"          : 6.
        "lgLineColors"              : (/"black","green"/)
        "lgMonoDashIndex"           : True
    end create
    draw(legend2)

    frame(wks)
  
	print("")
	print(" "+fig_file+"."+fig_type)
	print("")
	if fig_type.eq."png" then system("convert -trim "+fig_file+"."+fig_type+"   "+fig_file+"."+fig_type) end if

;====================================================================================================
;====================================================================================================
end
