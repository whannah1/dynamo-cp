; Timeseries of 6t-hourly data at Gan
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/contrib/time_axis_labels.ncl"
load "$NCARG_ROOT/custom_functions_DYNAMO_LSF.ncl"
load "$NCARG_ROOT/custom_functions_DYNAMO_ECMWF.ncl"
load "$NCARG_ROOT/custom_functions.ncl"
begin

	fig_type = "png"
	fig_file = "~/Research/DYNAMO/CP/map.EC_vs_LSA.v1"

	daily = True
	
	nsmooth = 0
	
	nday = 10

;====================================================================================================
;====================================================================================================		
	sa = "lsan"
	
	if sa.eq. "lsan" then
		lat1 =   0.
		lat2 =   7.
		lon1 =  72.
		lon2 =  79.
	end if
	if sa.eq. "lsas" then
		lat1 =  -7.
		lat2 =   0.
		lon1 =  72.
		lon2 =  79.
	end if
	
	lat1 = -15.
	lat2 =  15.
	lon1 =  60.
	lon2 =  90.
		
    line = "----------------------------------------------"	
	
	wks = gsn_open_wks(fig_type,fig_file)
		res = True
		res@gsnDraw                         = False
		res@gsnFrame                        = False
		res@tmXTOn                          = False
		res@tmXBMinorOn						= False
		res@tmYLMinorOn						= False
		res@gsnLeftStringFontHeightF        = 0.015
		res@gsnCenterStringFontHeightF    	= 0.015
		res@gsnRightStringFontHeightF       = 0.015
		res@tmXBLabelFontHeightF            = 0.01
		res@tmYLLabelFontHeightF            = 0.01
		res@tiXAxisFontHeightF				= 0.015
		res@tiYAxisFontHeightF				= 0.015
		res@tmXBMajorOutwardLengthF         = 0.0
		res@tmXBMinorOutwardLengthF         = 0.0
		res@tmYLMajorOutwardLengthF         = 0.0
		res@tmYLMinorOutwardLengthF         = 0.0
		res@gsnLeftString                   = ""
		res@gsnCenterString              	= ""
		res@gsnRightString                  = ""
		;res@tmXBLabelAngleF					= -50.
		
		;lres = res
		;lres@xyDashPattern              = 0
		;lres@xyLineThicknessF           = 1.
		;lres@xyLineColor                = "black"
		
		;mres = True
		;mres@gsMarkerIndex = 1
		;mres@gsMarkerSizeF = 0.02
		;mres@gsMarkerColor = "black"
		
;====================================================================================================
; Load Sounding Array Data
;====================================================================================================
	ver = "2b"
	t = nday*8
	if ver.eq."2a" then ifile = "~/Data/DYNAMO/LSA/data/v2a/dynamo_basic_v2a_2011all.nc" end if
	if ver.eq."2b" then ifile = "~/Data/DYNAMO/LSA/data/v2b/dynamo_basic_v2b_2011all.nc" end if
	infile = addfile(ifile,"r")
	;printVarSummary(infile->time)
	print("SA time: "+ (infile->time(t)/24-273) )
	SAlat = infile->lat({lat1:lat2})
	SAlon = infile->lon({lon1:lon2})
	ps  = infile->ps(:,{lat1:lat2},{lon1:lon2})
	p   = infile->level
	p!0 = "lev"
	p&lev = p
	dp  = calc_dP(p,ps) *100.
	;u   = infile->u(:,:,{lat1:lat2},{lon1:lon2})
	;v   = infile->v(:,:,{lat1:lat2},{lon1:lon2})
	wmr = infile->q(:,:,{lat1:lat2},{lon1:lon2}) 
	wmr = (/wmr/1000./)
	q 	= wmr/(1.+wmr)
	copy_VarCoords(wmr,q)

	
	SATPW = dim_sum_n(        q(t,:,:,:)*dp(t,:,:,:)/g,0)
	printline()
	printVarSummary(SATPW)
	SATPW!0 = "lat"
	SATPW!1 = "lon"
	SATPW&lat = SAlat
	SATPW&lon = SAlon
	delete([/p,ps,q/])
;====================================================================================================
; Load MIMIC Data
;====================================================================================================
if False then
	ifile  = "~/Data/DYNAMO/MIMIC/cimss_morphed_tpw_DYNAMO_2011.nc"
	infile = addfile(ifile,"r")
	;MInt   = dimsizes(infile->time)/6
	tmp = infile->tpw(:,{lat1:lat2},{lon1:lon2}) 
	iMITPW = new(dimsizes(tmp),float)
	iMITPW = tofloat( tmp )
	delete(tmp)
	iMITPW = tofloat( where(isnan_ieee(iMITPW),iMITPW@_FillValue,iMITPW) ) 
	bsz = 6
	MITPW = block_avg( dim_avg_n_Wrap(iMITPW,(/0/)) ,bsz)
	printline()
	printVarSummary(MITPW)
end if
;====================================================================================================
; Load ECMWF CWV Data
;====================================================================================================
	ifile = "~/Data/DYNAMO/ECMWF/data/6hour.DYNAMO.ECMWF.CWV.nc"
	infile = addfile(ifile,"r")
	printline()
	;printVarSummary(infile->time)
	print("EC time: "+ (infile->time(t)/24) ) 
	;ECTPW = new(SAnt,float)
	t = nday*4
	ECTPW = infile->CWV(t,{lat1:lat2},{lon1:lon2}) 
	printline()
	printVarSummary(ECTPW)
;====================================================================================================
; Create Plot 
;====================================================================================================	
	clr = (/"blue","green","red"/)
	
	tres = res
	tres@cnFillOn				= True
	tres@cnLinesOn				= False
	;tres@cnFillPalette			= ""
	tres@cnSpanFillPalette		= True
	tres@cnLevelSelectionMode	= "ExplicitLevels"
	tres@cnLevels				= ispan(26,64,4)
	tres@gsnAddCyclic			= False
	tres@mpLimitMode			= "LatLon"
	tres@mpMinLatF				= lat1
	tres@mpMaxLatF				= lat2
	tres@mpMinLonF				= lon1
	tres@mpMaxLonF				= lon2
	
	plot = new(2,graphic)
	
	tres@gsnLeftString = "LSA version "+ver
	plot(0) = gsn_csm_contour_map(wks,SATPW,tres)
	tres@gsnLeftString = "ECMWF"
	plot(1) = gsn_csm_contour_map(wks,ECTPW,tres)

	;overlay(plot(0) , gsn_csm_xy(wks,(/-1e3,1e3/),(/0.,0./),lres))
	
	num_p = dimsizes(plot)
	;--------------------------------------------------------------------------------------------
	; Add Array lines
	;--------------------------------------------------------------------------------------------
	site_lat  = (/-0.7,		0.,		-7.3,	-7.3 ,   4.1,  6.9,  0./)
	site_lon  = (/73.2,		80.,		80.,		72.5 ,  73.5, 79.8, 80./)
		lres = True
		lres@gsLineColor = "black"
	; NSA+SSA
	array_lat = new(8,float)
	array_lon = new(8,float)
	array_lat(:3) = site_lat(:3)
	array_lon(:3) = site_lon(:3)
	array_lat(4) = site_lat(0)
	array_lon(4) = site_lon(0)
	array_lat(5:) = site_lat(4:)
	array_lon(5:) = site_lon(4:)
	ldum = new(num_p,graphic)
	do p = 0,num_p-1 ldum(p) = gsn_add_polyline(wks,plot(p),array_lon,array_lat,lres) end do
	;--------------------------------------------------------------------------------------------
	; Add Line at Equator
	;--------------------------------------------------------------------------------------------
	eq_line_lon = (/0.,360./)
	eq_line_lat = (/0.,0./)
	lres@xyDashPattern = 1
	edum = new(num_p,graphic)
	do p = 0,num_p-1 edum(p) = gsn_add_polyline(wks,plot(p),eq_line_lon,eq_line_lat,lres) end do

;====================================================================================================
; Finalize Figure
;====================================================================================================
		pres = True
		pres@gsnFrame                           = False
		pres@txString                           = ""
		;pres@gsnPanelBottom                    = 0.1 
		;pres@amJust                            = "TopLeft"
		;pres@gsnPanelFigureStringsFontHeightF  = 0.015
		;pres@gsnPanelFigureStrings             = (/"a","b","c","d"/) 

	gsn_panel(wks,plot,(/num_p,1/),pres)

	legend = create "Legend" legendClass wks
		"lgAutoManage"              : False
		"vpXF"                      : 0.5
		"vpYF"                      : 0.5
		"vpWidthF"                  : 0.1
		"vpHeightF"                 : 0.06
		"lgPerimOn"                 : True
		"lgLabelsOn"                : True
		"lgLineLabelsOn"            : False
		"lgItemCount"               : 3
		"lgLabelStrings"            : (/"ECMWF","MIMIC","Soundings"/)
		"lgLabelFontHeightF"        : 0.01
		"lgDashIndex"             	: 0
		"lgLineThicknessF"			: 4.
		"lgLineColors"              : clr
		"lgMonoDashIndex"			: True
	end create

	;draw(legend)
	frame(wks)
  
	print("")
	print(" "+fig_file+"."+fig_type)
	print("")
	if fig_type.eq."png" then system("convert -trim "+fig_file+"."+fig_type+"   "+fig_file+"."+fig_type) end if

;====================================================================================================
;====================================================================================================
end
