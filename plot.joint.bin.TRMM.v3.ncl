; plots TRMM data within the joint distribution of CWV and
; the Lagrangian tendency defined by ECMWF data
; v1 & v2 were very slow despite my best efforts to speed them up
; v3 uses a "binmap" variable created by mk.joint.bin.TRMM.v1.ncl
; in order to reduce the redundancy of previous methods
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/custom_functions.ncl"
begin

    ver = 2

    fig_type = "png"
    fig_file = "~/Research/DYNAMO/CP/joint.bin.TRMM.v3."+ver
    
    reduceRes   = False
    dailyAvg    = False
    avgFlag     = False
    debug       = False

    nvar = 2

;====================================================================================================
;====================================================================================================
        res = setres_default()
        res@vpHeightF                       = 0.5
        res@gsnLeftStringFontHeightF        = 0.025
        res@gsnCenterStringFontHeightF      = 0.015
        res@gsnRightStringFontHeightF       = 0.015
        res@gsnLeftString                   = ""
        res@gsnCenterString                 = ""
        res@gsnRightString                  = ""
        
        lres = res
        lres@xyDashPattern              = 0
        lres@xyLineThicknessF           = 1.
        lres@xyLineColor                = "black"
;====================================================================================================
;====================================================================================================
    ifile = "~/Data/DYNAMO/CP/joint.bin.TRMM.binmap.nc"
    infile = addfile(ifile,"r")

    binmap = infile->binmap
    binpct = infile->bincnt

    printline()
    printMAM(binpct)

    ;exit
    
    Yavg = infile->Yavg
    Xavg = infile->Xavg
    
    binsx = infile->binsx
    binsy = infile->binsy
    
    lat1 = infile->lat1
    lat2 = infile->lat2
    lon1 = infile->lon1
    lon2 = infile->lon2
;====================================================================================================
; Load ECMWF Data
;====================================================================================================   
    ifile = "~/Data/DYNAMO/ECMWF/data/6hour.DYNAMO.ECMWF.CP.nc"
    infile = addfile(ifile,"r")
    EClat = infile->lat({lat1:lat2})
    EClon = infile->lon({lon1:lon2})
    ECTPW = infile->ECTPW(:,{lat1:lat2},{lon1:lon2})
    ECCPR = infile->ECCPR(:,{lat1:lat2},{lon1:lon2})

    ECtime = ispan(0,92*24-6,6)
    ECtime@units = "hours after 2011-10-01 00:00:00"

    num_t   = dimsizes(ECtime)
    num_lat = dimsizes(EClat)
    num_lon = dimsizes(EClon)

    ECCPR&time = ECtime
    ECTPW&time = ECtime
;====================================================================================================
; Load TRMM orbital data
;====================================================================================================
    ifile = "~/Data/DYNAMO/TRMM/orbital/TRMM.DYNAMO.PRdata.2011.50E_100E_10S_10N.v2.nc"
    infile = addfile(ifile,"r")

    t1 = 0
    if debug then 
        t2 = toint( 1000 )
    else
        t2 = dimsizes(infile->time)-1
    end if

    TRlat  = infile->lat        (t1:t2)
    TRlon  = infile->lon        (t1:t2)
    TRtime = infile->time       (t1:t2)
    TRrain = infile->rainSfc    (t1:t2)

    ;TReth1_3 = infile->numETH_1_3  (t1:t2)
    ;TReth3_5 = infile->numETH_3_5  (t1:t2)
    ;TReth5_7 = infile->numETH_5_7  (t1:t2)
    ;TReth7_9 = infile->numETH_7_9  (t1:t2)
    
    ;numRain  = infile->numRain    (t1:t2)
    ;numConv  = infile->numConv    (t1:t2)
    numStrat = infile->numStrat   (t1:t2)

    cluster1 = new(dimsizes(TRtime),float)
    cluster4 = new(dimsizes(TRtime),float)

    ;cluster  = infile->$ivar$    (t1:t2)
    cluster1  = infile->cluster1    (t1:t2)
    cluster4  = infile->cluster4    (t1:t2)

    dt = ( ECtime(1) - ECtime(0) )/2.
    do t = 0,dimsizes(ECtime)-1
        condition = TRtime.gt.(ECtime(t)-dt) .and. TRtime.le.(ECtime(t)+dt)
        TRtime = where(condition,ECtime(t),TRtime)
        delete(condition)
    end do

    TRrain@lat  = TRlat
    TRrain@lon  = TRlon
    TRrain@time = TRtime

    ;copy_VarAtts(TRrain,TReth1_3)
    ;copy_VarAtts(TRrain,TReth3_5)
    ;copy_VarAtts(TRrain,TReth5_7)
    ;copy_VarAtts(TRrain,TReth7_9)
    ;copy_VarAtts(TRrain,numRain )
    ;copy_VarAtts(TRrain,numConv )
    ;copy_VarAtts(TRrain,numStrat)
    copy_VarAtts(TRrain,cluster1)
    copy_VarAtts(TRrain,cluster4)

    ;numRain@long_name  = "# raining pixels"
    ;numConv@long_name  = "# convective pixels"
    ;numStrat@long_name = "# stratiform pixels"
    ;cluster@long_name = "cluster class"
    ;cluster@long_name = "cluster class"
   
;====================================================================================================
; Bin data
;====================================================================================================
    nbiny  = toint( ( binsy@bin_max - binsy@bin_min + binsy@bin_spc )/binsy@bin_spc )
    nbinx  = toint( ( binsx@bin_max - binsx@bin_min + binsx@bin_spc )/binsx@bin_spc )
    biny   = fspan(binsy@bin_min,binsy@bin_max,nbiny)
    binx   = fspan(binsx@bin_min,binsx@bin_max,nbinx)
    bdim   = (/nvar,nbiny,nbinx/)
    binval = new(bdim,float)
    bincnt = new(bdim,float)
    
    name = new(nvar,string)

    do v = 0,nvar-1
    
        print("    v = "+v+"    v%nvar = "+v%nvar)

        Vz = new(dimsizes(TRtime),float)        ; This is to supress "_FillValue" warnings

        ; 4-panel of main cloud clusters
        if ver.eq.1 then
            if v.eq.0 then Vz = where(cluster1.eq.1,1.,0.)  end if
            if v.eq.1 then Vz = where(cluster1.eq.2,1.,0.)  end if
            if v.eq.0 then name(v) = "cluster class: shallow and congestus" end if
            if v.eq.1 then name(v) = "cluster class: congestus plus some deep" end if
        end if

        if ver.eq.2 then
            if v.eq.0 then Vz = where(cluster1.eq.0,1.,0.)  end if
            if v.eq.1 then Vz = where(cluster1.eq.4,1.,0.)  end if
            if v.eq.0 then name(v) = "cluster class: non-raining" end if
            if v.eq.1 then name(v) = "cluster class: stratiform" end if
        end if

        if ver.eq.3 then
            if v.eq.0 then Vz = where(cluster1.eq.3,1.,0.)  end if
            if v.eq.1 then Vz = where(cluster1.eq.2,1.,0.)  end if
            if v.eq.0 then name(v) = "cluster class: congestus plus some deep" end if
            if v.eq.1 then name(v) = "cluster class: deep conv + stratiform" end if
        end if

        ; comparison of 0.25 and 1 degree cluster domains
        if False then
            if v.eq.0 then Vz = cluster1 end if
            if v.eq.1 then Vz = cluster4 end if
            Vz = where(Vz.eq.0,1.,0.)
            if v.eq.0 then name(v) =  "1.0-deg cluster class: non-raining" end if
            if v.eq.1 then name(v) = "0.25-deg cluster class: deep" end if
        end if

        ; stratiform comparison
        ;if v.eq.0 then Vz = numStrat end if
        ;if v.eq.1 then Vz = where(cluster1.eq.4,1.,0.) end if
        ;if v.eq.0 then name(v) = "# of stratiform pixels per box" end if
        ;if v.eq.1 then name(v) = "1.0-deg cluster class: stratiform" end if

        ;copy_VarCoords(cluster4,Vz)
        ;copy_VarAtts  (cluster4,Vz)
        ;name(v) = Vz@long_name

        ;Vz = (/ Vz/stddev(Vz) /)
        ;Vz = where(Vz.ne.0.,Vz,Vz@_FillValue) 

        do by = 0,nbiny-1
        do bx = 0,nbinx-1
            binid   = toint( by*1e3 + bx )
            condition = binmap.eq.binid
            bincnt(v,by,bx) = num(condition)
            binval(v,by,bx) = avg( where(condition,Vz,Vz@_FillValue) ) 
        end do
        end do
        delete([/Vz/])
    end do
    
    bincnt := bincnt(0,:,:)
    printMAM(bincnt)
    bincnt = 100.* tofloat(bincnt) / tofloat(product(dimsizes(cluster1)))
    printMAM(bincnt)

    printline()

    if any(ver.eq.(/1,2,3/)) then
        binval = binval*100.
    end if
    
    binval!0 = "var"
    binval!1 = "ybin"
    binval!2 = "xbin"
    binval&ybin = biny
    binval&xbin = binx
    copy_VarCoords(binval(0,:,:),bincnt)
;====================================================================================================
;====================================================================================================
    wks = gsn_open_wks(fig_type,fig_file)
    ;plot = new(nvar*2,graphic)
    plot = new(nvar,graphic)
        cres = res
        cres@cnFillOn           = True
        cres@cnLinesOn          = False
        cres@cnInfoLabelOn      = False
        cres@cnFillPalette      = "CBR_wet"
        cres@cnFillPalette      = "GMT_drywet"
        cres@cnFillPalette      = "MPL_cool"
        ;cres@cnFillPalette     = ""
        cres@lbOrientation      = "vertical"
        ;cres@cnFillMode         = "RasterFill"
        cres@cnFillMode         = "CellFill"
        cres@tiYAxisString      = "Lagrangian Tendency [mm day~S~-1~N~]"
        cres@tiXAxisString      = "CWV [mm]"

        tres = cres
        tres@cnFillOn           = False
        tres@cnLinesOn          = True
        tres@cnLineThicknessF   = 3.
        tres@gsnContourZeroLineThicknessF   = 6.
        tres@gsnContourNegLineDashPattern   = 1
        
        ;cres@cnLevelSelectionMode  = "ExplicitLevels"
        ;cres@cnLevels               = ispan(5,95,5)/1e2
        ;cres@lbLabelStride          = 2

        if any(ver.eq.(/1,2,3/)) then
            cres@lbTitleString = "%"
        end if

        
    do v = 0,nvar-1
        p1 = v
        p2 = v+nvar
        
        cres@gsnLeftString = name(v)

        plot(v) = gsn_csm_contour(wks,binval(v,:,:),cres)
        
        overlay(plot(v) , gsn_csm_contour(wks,bincnt,tres))
        
        overlay(plot(v) , gsn_csm_xy(wks,(/-1e3,1e3/),(/0.,0./),lres))
        overlay(plot(v) , gsn_csm_xy(wks,(/1.,1./)*Xavg,(/-1e3,1e3/),lres)) 
        
        ;if v.eq.0 then 
        ;   overlay(plot(v) , gsn_csm_xy(wks,(/1.,1./)*avg(ECTPW),(/-1e3,1e3/),lres)) 
        ;end if
    end do

    
;====================================================================================================
; Finalize Figure
;====================================================================================================
        pres = setres_panel_labeled()
        pres@gsnPanelYWhiteSpacePercent = 5

    if nvar.eq.4 then
        gsn_panel(wks,plot,(/2,2/),pres)
    end if
    
    if any(nvar.eq.(/5,6/)) then
        gsn_panel(wks,plot,(/3,2/),pres)
    end if
    
    if all(nvar.ne.(/4,5,6/)) then
       gsn_panel(wks,plot,(/1,dimsizes(plot)/),pres)
       ;gsn_panel(wks,plot,(/dimsizes(plot),1/),pres)
    end if

    trimPNG(fig_file)
;====================================================================================================
;====================================================================================================
end
