; Bins radar data 
; v5 - bin conv/strat fraction against LT (incr. and decr.)
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/custom_functions_DYNAMO_LSF.ncl"
load "$NCARG_ROOT/custom_functions_DYNAMO_ECMWF.ncl"
load "$NCARG_ROOT/custom_functions.ncl"
begin

	src = "SPOL"

	fig_type = "png"
	fig_file = "~/Research/DYNAMO/CP/radar.bin.v5"
	
	reduceRes	= True
	debug 		= False
;====================================================================================================
;====================================================================================================		
	nvar = 2
	ngrp = 2
	bins = True
	bins@verbose = False
    bins@bin_min = -20
    bins@bin_max =  20
    bins@bin_spc =   4

	xbin 	= ispan(bins@bin_min,bins@bin_max,bins@bin_spc)
    num_bin = dimsizes(xbin)
    bdim 	= (/ngrp,nvar,num_bin/)
    binval = new(bdim,float)
    bincnt = new(bdim,float)
    bincnt = 0
    
	wks = gsn_open_wks(fig_type,fig_file)
	plot = new(1,graphic)
		res = True
		res@gsnDraw                         = False
		res@gsnFrame                        = False
		res@vpHeightF						= 0.3
		res@tmXTOn                          = False
		res@tmXBMinorOn						= False
		res@tmYLMinorOn						= False
		res@tmYRMinorOn						= False
		res@gsnLeftStringFontHeightF        = 0.015
		res@gsnCenterStringFontHeightF    	= 0.015
		res@gsnRightStringFontHeightF       = 0.015
		res@tmXBLabelFontHeightF            = 0.01
		res@tmYLLabelFontHeightF            = 0.01
		res@tiXAxisFontHeightF				= 0.015
		res@tiYAxisFontHeightF				= 0.015
		res@tmXBMajorOutwardLengthF         = 0.0
		res@tmXBMinorOutwardLengthF         = 0.0
		res@tmYLMajorOutwardLengthF         = 0.0
		res@tmYLMinorOutwardLengthF         = 0.0
		res@gsnLeftString                   = ""
		res@gsnCenterString              	= ""
		res@gsnRightString                  = ""
		;res@tmXBLabelAngleF					= -50.
		
		lres = res
		lres@xyDashPattern              = 0
		lres@xyLineThicknessF           = 3.
		lres@xyLineColor                = "black"
		
		cres = res
		cres@cnFillOn 				= True
		cres@cnLinesOn 				= False
		cres@cnSpanFillPalette		= True
		cres@trYReverse				= True
;====================================================================================================
; Load Radar Data
;====================================================================================================
	;if src.eq."SPOL" then ifile = "~/Data/DYNAMO/RADAR/SPOL/gridded_survey_scans_2d/spol_radar.OND_2011_DYNAMO.nc" end if
	if src.eq."SPOL" then ifile = "~/Data/DYNAMO/RADAR/SPOL/spol_radar_gridded.1D.OND_2011_DYNAMO.nc" end if
	infile = addfile(ifile,"r")
	;print(infile)
	rtime = infile->time
	rtime = (/ rtime - rtime(0) /)
	rtime@units = "hours since 2011-10-01 00:00:00"
	
	rlat = infile->lat
	rlon = infile->lon
	lat1 = min(rlat) -1.
	lat2 = max(rlat) +1.
	lon1 = min(rlon) -1.
	lon2 = max(rlon) +1.
	
	rdim = (/dimsizes(rtime)/)
	iRADcldtop00 = new(rdim,float)
	iRADcldtop10 = new(rdim,float)
	iRADcldtop20 = new(rdim,float)
	iRADcldtop30 = new(rdim,float)
	iRADstrfrc	 = new(rdim,float)
	iRADcnvfrc	 = new(rdim,float)
	
	iRADprecip = infile->precip		*24.
	;iRADclrfrc = infile->RADclrfrc
	iRADstrfrc = infile->RADstrfrc
	iRADcnvfrc = infile->RADcnvfrc
	iRADcldtop00 = infile->top_0_dBZ_avg
	
	if True then
		iRADcldtop10 = infile->top_10_dBZ_avg
		iRADcldtop20 = infile->top_20_dBZ_avg
		iRADcldtop30 = infile->top_30_dBZ_avg
	else
		iRADcldtop10 = infile->top_10_dBZ_max
		iRADcldtop20 = infile->top_20_dBZ_max
		iRADcldtop30 = infile->top_30_dBZ_max
	end if

	nt = (31+30+31)*4
	time = tofloat(ispan(0,nt-1,1))*6.
	time@units = "hours since 2011-10-01 00:00:00"
	
	rdim = (/nt/)
	RADprecip = new(rdim,float)
	RADstrfrc = new(rdim,float)
	RADcnvfrc = new(rdim,float)
	RADcldtop00 = new(rdim,float)
	RADcldtop10 = new(rdim,float)
	RADcldtop20 = new(rdim,float)
	RADcldtop30 = new(rdim,float)
	
	do t = 0,nt-1
		condition = (rtime.gt.(time(t)-3)).and.(rtime.lt.(time(t)+3))
		if num(condition).gt.0 then 
			RADprecip(t) = avg( where(condition,iRADprecip,iRADprecip@_FillValue) )
			RADstrfrc(t) = avg( where(condition,iRADstrfrc,iRADstrfrc@_FillValue) )
			RADcnvfrc(t) = avg( where(condition,iRADcnvfrc,iRADcnvfrc@_FillValue) )
			RADcldtop00(t) = avg( where(condition,iRADcldtop00,RADcldtop00@_FillValue) )
			RADcldtop10(t) = avg( where(condition,iRADcldtop10,RADcldtop10@_FillValue) )
			RADcldtop20(t) = avg( where(condition,iRADcldtop20,RADcldtop20@_FillValue) )
			RADcldtop30(t) = avg( where(condition,iRADcldtop30,RADcldtop30@_FillValue) )
		end if
	end do
	
	
	RADprecip!0 = "time"
	RADprecip&time = time
	copy_VarCoords(RADprecip,RADstrfrc)
	copy_VarCoords(RADprecip,RADcnvfrc)
	copy_VarCoords(RADprecip,RADcldtop00)
	copy_VarCoords(RADprecip,RADcldtop10)
	copy_VarCoords(RADprecip,RADcldtop20)
	copy_VarCoords(RADprecip,RADcldtop30)
	
;====================================================================================================
; Load Sounding Array Data
;====================================================================================================
	ver = "2b"
	if ver.eq."2a" then ifile = "~/Data/DYNAMO/LSA/data/v2a/dynamo_basic_v2a_2011all.nc" end if
	if ver.eq."2b" then ifile = "~/Data/DYNAMO/LSA/data/v2b/dynamo_basic_v2b_2011all.nc" end if
	infile = addfile(ifile,"r")
	SAlat = infile->lat({lat1:lat2})
	SAlon = infile->lon({lon1:lon2})
	ps  = infile->ps(:,{lat1:lat2},{lon1:lon2})
	p   = infile->level
	p!0 = "lev"
	p&lev = p
	idp = calc_dP(p,ps) *100.
	iu  = infile->u(:,:,{lat1:lat2},{lon1:lon2})
	iv  = infile->v(:,:,{lat1:lat2},{lon1:lon2})
	wmr = infile->q(:,:,{lat1:lat2},{lon1:lon2})
	wmr = (/wmr/1000./)
	iq 	= wmr/(1.+wmr)
	copy_VarCoords(wmr,iq)
	dp = block_avg(idp,2)
	q  = block_avg(iq,2)
	u  = block_avg(iu,2)
	v  = block_avg(iv,2)
	
	dt = 6*3600.
	iSADDT = calc_ddt(q,dt)*86400.
	iSAADV = calc_adv(q,u,v) *-1.*86400.	
	SATPW = dim_sum_n(     q*dp/g,1) 
	SADDT = dim_sum_n(iSADDT*dp/g,1) 
	SAADV = dim_sum_n(iSAADV*dp/g,1) 
	
	SACPR = SADDT-SAADV
	delete([/wmr,q,u,v,p,ps,dp,iSADDT,iSAADV,idp,iq,iu,iv/])
;====================================================================================================
; Load ECMWF Data
;====================================================================================================	
	t2 = (31+30+31)*4-1
	if debug then t2 = 10*4 end if
	infile = addfile("~/Data/DYNAMO/ECMWF/data/6hour.DYNAMO.ECMWF.U.nc","r")
	u = infile->U(:t2,:,{lat1:lat2},{lon1:lon2})
	infile = addfile("~/Data/DYNAMO/ECMWF/data/6hour.DYNAMO.ECMWF.V.nc","r")
	v = infile->V(:t2,:,{lat1:lat2},{lon1:lon2})
	infile = addfile("~/Data/DYNAMO/ECMWF/data/6hour.DYNAMO.ECMWF.Q.nc","r")
	q = infile->Q(:t2,:,{lat1:lat2},{lon1:lon2})
	
	EClev = q&lev
	
	infile = addfile("~/Data/DYNAMO/ECMWF/data/6hour.DYNAMO.ECMWF.Ps.nc","r")
	ps = infile->Ps(:t2,{lat1:lat2},{lon1:lon2})	
	p = EClev*100.
	p@units = "Pa"
	p!0 = "lev"
	p&lev = p
	dp = calc_dP(p,ps)

	ifile = "~/Data/DYNAMO/ECMWF/data/6hour.DYNAMO.ECMWF.CWV.nc"
	infile = addfile(ifile,"r")
	ECTPW = infile->CWV(:t2,{lat1:lat2},{lon1:lon2}) 
	;ECTPW = dim_sum_n(q*dp/g,1)	
	
	if reduceRes then
		iECTPW = ECTPW
		ilat = q&lat
		ilon = q&lon
		iq = q
		iu = u
		iv = v
		idp = dp
		delete([/u,v,q,dp,ECTPW/])
		q  = area_hi2lores_Wrap(ilon,ilat,iq ,False,1.,SAlon,SAlat,False)
		u  = area_hi2lores_Wrap(ilon,ilat,iu ,False,1.,SAlon,SAlat,False)
		v  = area_hi2lores_Wrap(ilon,ilat,iv ,False,1.,SAlon,SAlat,False)
		dp = area_hi2lores_Wrap(ilon,ilat,idp,False,1.,SAlon,SAlat,False)
		ECTPW = area_hi2lores_Wrap(ilon,ilat,iECTPW ,False,1.,SAlon,SAlat,False)
		delete([/iu,iv,iq,idp,iECTPW/])
	end if
	
	iECDDT = calc_ddt(q,6*3600.)		 *86400.
	iECADV = calc_adv(q,u,v) *-1.*86400.
	iECCPR = iECDDT - iECADV
	
	;ECDDT = dim_sum_n(iECDDT*dp/g,1)
	;ECADV = dim_sum_n(iECADV*dp/g,1)
	;ECCPR = ECDDT - ECADV
	
	ECDDT = dim_sum_n(iECDDT*dp/g,1) ;,(/1,2/))
	ECADV = dim_sum_n(iECADV*dp/g,1) ;,(/1,2/))
	ECCPR = ECDDT - ECADV
	
	delete([/q,u,v,iECDDT,iECADV,p,ps,dp/])
;====================================================================================================
; Bin data
;====================================================================================================

    print("Binning data...")
    do p = 0,ngrp-1
    do v = 0,nvar-1
        print("    v = "+v+"    v%nvar = "+v%nvar)
        
        ;if p.eq.0 then pcond = ECDDT.gt.stddev(ECDDT)* 1. end if
        ;if p.eq.1 then pcond = ECDDT.lt.stddev(ECDDT)*-1. end if
        
        ;if p.eq.0 then pcond = ECCPR.gt.0. end if
        ;if p.eq.1 then pcond = ECCPR.lt.0. end if
        
        ;if p.eq.0 then pcond = ECCPR.gt.stddev(ECCPR)* 1. end if
        ;if p.eq.1 then pcond = ECCPR.lt.stddev(ECCPR)*-1. end if
        
        ;Vx = dim_avg_n_Wrap(where(pcond,ECTPW,ECTPW@_FillValue),(/1,2/))
        Vx = dim_avg_n_Wrap(ECCPR,(/1,2/))
        
        if v.eq.0 then Vy = RADcnvfrc  end if
        if v.eq.1 then Vy = RADstrfrc  end if
		
		tmp = bin_YbyX(Vy,Vx,bins)
        binval(p,v,:) = tmp
        bincnt(p,v,:) = tmp@pctcnt
                
        delete([/Vy,Vx/])
        
    end do
    end do
    
    ;unit = "["+(/"km","km","km"/)+"]"
    
    binval = (/binval*100./)
    
    name = new(nvar,string)
    name(0) = "convective fraction"
    name(1) = "straiform fraction"
    
    pname = new(nvar,string)
    pname(0) = "TPW increasing"
    pname(1) = "TPW decreasing"
;====================================================================================================
; Create Plot
;====================================================================================================
		tres = res
		tres@tmYROn				= False
		tres@cnLevelSelectionMode	= "ExplicitLevels"
		tres@cnLevels	= ispan(-8,8,1)*1e-3

		lres@trXMinF			= min(xbin)
		lres@trXMaxF			= max(xbin)
		lres1 = lres
		lres2 = lres
		lres1@tiXAxisString  	= "[mm]"
		lres1@tiYAxisString  	= "[%]"
		lres2@tiYAxisString  	= "[% of observations]"
		lres1@xyLineThicknessF	= 4.
		lres2@xyLineThicknessF	= 1.
		lres2@trYMinF			=  0.
		;lres2@trYMaxF			= 50.
		
		lres1@gsnLeftString   	= ""
		
		clr = (/"red","green","blue"/)
		lres1@xyLineColors = clr
	
	do p = 0,ngrp-1	
		lres1@xyDashPattern		= p
		lres2@xyDashPattern		= p
		if p.eq.0 then          plot = gsn_csm_xy2(wks,xbin,binval(p,:,:),bincnt(p,:,:),lres1,lres2)  end if
		if p.ne.0 then overlay( plot , gsn_csm_xy2(wks,xbin,binval(p,:,:),bincnt(p,:,:),lres1,lres2)) end if
	end do
;====================================================================================================
; Finalize Figure
;====================================================================================================
		pres = True
		pres@gsnPanelYWhiteSpacePercent			= 5
		pres@gsnFrame                      		= False
		pres@amJust                       		= "TopLeft"
		pres@gsnPanelFigureStringsFontHeightF	= 0.01
		pres@gsnPanelFigureStrings          	= (/"a","b","c","d","e","f"/)

	layout = (/dimsizes(plot),1/)
	
	gsn_panel(wks,plot,layout,pres)

	legend = create "Legend" legendClass wks
		"lgAutoManage"              : False
		"vpXF"                      : 0.2
		"vpYF"                      : 0.7
		"vpWidthF"                  : 0.1
		"vpHeightF"                 : 0.05
		"lgPerimOn"                 : True
		"lgLabelsOn"                : True
		"lgLineLabelsOn"            : False
		"lgItemCount"               : nvar
		"lgLabelStrings"            : name
		"lgLabelFontHeightF"        : 0.01
		"lgDashIndex"             	: 0
		"lgLineThicknessF"			: 4.
		"lgLineColors"              : clr
		"lgMonoDashIndex"			: True
	end create

	draw(legend)
	frame(wks)
  
	print("")
	print(" "+fig_file+"."+fig_type)
	print("")
	if fig_type.eq."png" then system("convert -trim "+fig_file+"."+fig_type+"   "+fig_file+"."+fig_type) end if

;====================================================================================================
;====================================================================================================
end
