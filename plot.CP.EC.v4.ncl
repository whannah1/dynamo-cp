; Similar to v1, plots a timeseries of TPW, but uses anomalies 
; instead of the total TPW since mean values are different between
; ECMWF and DYNAMO NSA. Also, the Lagrangian Tendency of the 
; ECMWF water budget is overlayed using the right Y-Axis. 
; A smoothed version of the Lagrangian Tendency is also overlayed.
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/contrib/time_axis_labels.ncl"
load "$NCARG_ROOT/custom_functions_DYNAMO_ECMWF.ncl"
load "$NCARG_ROOT/custom_functions.ncl"
begin
	
	fig_type = "png"
	fig_file = "~/Research/DYNAMO/CP/CP.EC.v4"

	sa = "lsan"
	
	daily = True
	
	nsmooth = 3

;====================================================================================================
;====================================================================================================
	if sa.eq."lsan" then
		lat1 =  -1.
		lat2 =   8.
		lon1 =  72.
		lon2 =  81.
	end if
	if sa.eq."lsas" then
		lat1 = -8.
		lat2 = -1.
		lon1 =  72.
		lon2 =  81.
	end if


	wks = gsn_open_wks(fig_type,fig_file)
	gsn_define_colormap(wks,"WhiteBlueGreenYellowRed")
	;gsn_define_colormap(wks,"ncl_default")
	plot = new(1,graphic)
		res = True
		res@gsnDraw                         = False
		res@gsnFrame                        = False
		res@vpHeightF						= 0.4
		res@tmXTOn                          = False
		res@gsnLeftStringFontHeightF        = 0.015
		res@gsnCenterStringFontHeightF    	= 0.015
		res@gsnRightStringFontHeightF       = 0.015
		res@tmXBLabelFontHeightF            = 0.01
		res@tmYLLabelFontHeightF            = 0.01
		res@tiXAxisFontHeightF				= 0.015
		res@tiYAxisFontHeightF				= 0.015
		res@tmXBMajorOutwardLengthF         = 0.0
		res@tmXBMinorOutwardLengthF         = 0.0
		res@tmYLMajorOutwardLengthF         = 0.0
		res@tmYLMinorOutwardLengthF         = 0.0
		res@gsnLeftString                   = ""
		res@gsnCenterString              	= ""
		res@gsnRightString                  = ""
		res@tmXBLabelAngleF					= -45.
		
		lres = res
		lres@xyDashPattern              = 0
		lres@xyLineThicknessF           = 1.
		lres@xyLineColor                = "black"
;====================================================================================================
; Load Sounding Array Data
;====================================================================================================
	infile = addfile("~/Data/DYNAMO/ECMWF/data/6hour.DYNAMO.ECMWF.Ps.nc","r")
	ips  = infile->Ps(:,{lat1:lat2},{lon1:lon2})
	infile = addfile("~/Data/DYNAMO/ECMWF/data/6hour.DYNAMO.ECMWF.U.nc","r")
	iu = infile->U(:,:,{lat1:lat2},{lon1:lon2})
	infile = addfile("~/Data/DYNAMO/ECMWF/data/6hour.DYNAMO.ECMWF.V.nc","r")
	iv = infile->V(:,:,{lat1:lat2},{lon1:lon2})
	infile = addfile("~/Data/DYNAMO/ECMWF/data/6hour.DYNAMO.ECMWF.Q.nc","r")
	iq = infile->Q(:,:,{lat1:lat2},{lon1:lon2})
	
	printVarSummary(iq)
	s = 10
	
	ps = ips(:,::s,::s)
	u = iu(:,:,::s,::s)
	v = iv(:,:,::s,::s)
	q = iq(:,:,::s,::s)
	
	printVarSummary(q)
	exit
	
	p   = q&lev*100.
	p!0 = "lev"
	p&lev = p
	dp  = calc_dP(p,ps) 
	
	dt 	= 6*3600.
	adv = calc_adv(q,u,v) *-1.
	ddt = calc_ddt(q,dt)

	TPW = block_avg( dim_avg_n( dim_sum_n(        q*dp/g,1) ,(/1,2/)) ,4) /1000.*1000.  ; divide by density and convert to mm
	DDT = block_avg( dim_avg_n( dim_sum_n(      ddt*dp/g,1) ,(/1,2/)) ,4)
	ADV = block_avg( dim_avg_n( dim_sum_n(      adv*dp/g,1) ,(/1,2/)) ,4)
	LT  = block_avg( dim_avg_n( dim_sum_n((ddt-adv)*dp/g,1) ,(/1,2/)) ,4)
	;LT2  = DDT - ADV
	
	TPW = dim_rmvmean_Wrap(TPW)
	ADV = (/ADV *86400./)
	DDT = (/DDT *86400./)
	LT  = (/LT  *86400./)
	
	do n = 0,nsmooth-1 LT  = wgt_runave_Wrap(LT , (/0.25,0.5,0.25/), 0) end do
	do n = 0,nsmooth-1 DDT = wgt_runave_Wrap(DDT, (/0.25,0.5,0.25/), 0) end do
	do n = 0,nsmooth-1 ADV = wgt_runave_Wrap(ADV, (/0.25,0.5,0.25/), 0) end do

;====================================================================================================
; Plot Map
;====================================================================================================
	time = ispan(0,dimsizes(TPW)-1,1)
	time@units = "days since 2011-10-01"
	cdtime = cd_calendar(time,2)
	tmres = True
	tmres@ttmFormat 		= "%D%c"
	tmres@ttmAxis   		= "XB"
	tmres@ttmMinorStride	= 1
	tmres@ttmMajorStride	= 5
	time_axis_labels(time,res,tmres)


		res@xyLineThicknessF 	= 4.
		res@xyDashPattern    	= 0
		res@trXMinF				= min(time)
		res@trXMaxF				= max(time)
		tres1 = res
		tres1@gsnCenterString   = "ECMWF Analysis"
		tres1@trYMinF         	= -14.
		tres1@trYMaxF        	=  14.
		tres1@xyLineColor   	= "black"
		tres1@tiYAxisString   	= "TPW Anomaly [mm]"
		tres2 = res
		tres2@trYMinF     		= -8.
		tres2@trYMaxF     		=  8.
		tres2@xyLineColors      = (/"red","green","blue"/)
		tres2@tiYAxisString  	= "TPW Tendency [mm/day]"
		
	plot(0) = gsn_csm_xy2(wks,time,TPW,(/LT,DDT,ADV/),tres1,tres2)

	overlay(plot(0) , gsn_csm_xy(wks,(/-1e3,1e3/),(/0.,0./),lres))

;====================================================================================================
; Finalize Figure
;====================================================================================================
		pres = True
		pres@gsnFrame                           = False
		pres@txString                           = ""
		;pres@gsnPanelBottom                    = 0.1 
		;pres@amJust                            = "TopLeft"
		;pres@gsnPanelFigureStringsFontHeightF  = 0.015
		;pres@gsnPanelFigureStrings             = (/"a","b","c","d"/) 

	gsn_panel(wks,plot,(/dimsizes(plot),1/),pres)

	legend = create "Legend" legendClass wks
		"lgAutoManage"              : False
		"vpXF"                      : 0.4
		"vpYF"                      : 0.3
		"vpWidthF"                  : 0.2
		"vpHeightF"                 : 0.12
		"lgPerimOn"                 : True
		"lgLabelsOn"                : True
		"lgLineLabelsOn"            : False
		"lgItemCount"               : 4
		;"lgLabelStrings"            : (/"ECMWF TPW","NSA TPW","ECMWF Lagrangian Tend.","LHFLX"/)
		"lgLabelStrings"            : (/"ECMWF TPW","NSA TPW","ECMWF Lagrangian Tend.","NSA Lagrangian Tend."/)
		"lgLabelFontHeightF"        : 0.014
		"lgDashIndexes"             : (/0,1,0,1/)
		"lgLineThicknesses"         : (/4.,4.,4.,4./)
		"lgLineColors"              : (/"black","black","red","red"/)
		"lgMonoLineThickness"       : False
	end create

	;draw(legend)
	frame(wks)
  
	print("")
	print(" "+fig_file+"."+fig_type)
	print("")
	if fig_type.eq."png" then system("convert -trim "+fig_file+"."+fig_type+"   "+fig_file+"."+fig_type) end if

;====================================================================================================
;====================================================================================================
end
