load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/contrib/time_axis_labels.ncl"
load "$NCARG_ROOT/custom_functions_DYNAMO_ECMWF.ncl"
load "$NCARG_ROOT/custom_functions.ncl"
begin
	
	fig_type = "png"
	fig_file = "~/Research/DYNAMO/CP/ADV.time-height.v1"

	sa = "lsan"
	
	daily = True
	
	nsmooth = 0

;====================================================================================================
;====================================================================================================
	if sa.eq."lsan" then
		lat1 =  -1.
		lat2 =   8.
		lon1 =  72.
		lon2 =  81.
	end if
	if sa.eq."lsas" then
		lat1 = -8.
		lat2 = -1.
		lon1 =  72.
		lon2 =  81.
	end if
;====================================================================================================
; Load Sounding Array Data
;====================================================================================================
	ver = "2b"
	if ver.eq."2a" then ifile = "~/Data/DYNAMO/LSA/v2a/dynamo_basic_v2a_2011all.nc" end if
	if ver.eq."2b" then ifile = "~/Data/DYNAMO/LSA/v2b/dynamo_basic_v2b_2011all.nc" end if
	infile = addfile(ifile,"r")
	SAlev1 = infile->level
	u   = infile->u(:,:,{lat1:lat2},{lon1:lon2})
	v   = infile->v(:,:,{lat1:lat2},{lon1:lon2})
	wmr = infile->q(:,:,{lat1:lat2},{lon1:lon2})
	wmr = (/wmr/1000./)
	q 	= wmr/(1.+wmr)
	copy_VarCoords(wmr,q)
	dt 	= 3*3600.
	
	adv = calc_adv(q,u,v) *-1.
	ddt = calc_ddt(q,dt)
	cpr = (ddt-adv)

	SADDT = block_avg( dim_avg_n( ddt ,(/2,3/)) ,8)
	SAADV = block_avg( dim_avg_n( adv ,(/2,3/)) ,8)
	SACPR = block_avg( dim_avg_n( cpr ,(/2,3/)) ,8)
	
	SAADV = (/SAADV *86400./)
	SADDT = (/SADDT *86400./)
	SACPR = (/SACPR *86400./)
	
	do n = 0,nsmooth-1 SACPR = wgt_runave_n_Wrap(SACPR, (/0.25,0.5,0.25/), 0,0) end do
	do n = 0,nsmooth-1 SADDT = wgt_runave_n_Wrap(SADDT, (/0.25,0.5,0.25/), 0,0) end do
	do n = 0,nsmooth-1 SAADV = wgt_runave_n_Wrap(SAADV, (/0.25,0.5,0.25/), 0,0) end do
	
	SAADV!0 = "time"
	SAADV!1 = "lev"
	SAADV&lev = SAlev1
	
	delete([/u,v,wmr,q,dt,adv,ddt,cpr/])
	
	SAADV1 = SAADV
	delete([/SAADV,SADDT,SACPR/])
;====================================================================================================
; Load Sounding Array Data
;====================================================================================================
	ver = "2b"
	if ver.eq."2a" then ifile = "~/Data/DYNAMO/LSA/v2a/dynamo_basic_v2a_2011all.nc" end if
	if ver.eq."2b" then ifile = "~/Data/DYNAMO/LSA/v2b/dynamo_basic_v2b_2011all.nc" end if
	infile = addfile(ifile,"r")
	SAlev2 = infile->level
	u   = infile->u(:,:,{lat1:lat2},{lon1:lon2})
	v   = infile->v(:,:,{lat1:lat2},{lon1:lon2})
	wmr = infile->q(:,:,{lat1:lat2},{lon1:lon2})
	wmr = (/wmr/1000./)
	q 	= wmr/(1.+wmr)
	copy_VarCoords(wmr,q)
	dt 	= 3*3600.
	
	adv = calc_adv(q,u,v) *-1.
	ddt = calc_ddt(q,dt)
	cpr = (ddt-adv)

	SADDT = block_avg( dim_avg_n( ddt ,(/2,3/)) ,8)
	SAADV = block_avg( dim_avg_n( adv ,(/2,3/)) ,8)
	SACPR = block_avg( dim_avg_n( cpr ,(/2,3/)) ,8)
	
	SAADV = (/SAADV *86400.*1e3/)
	SADDT = (/SADDT *86400.*1e3/)
	SACPR = (/SACPR *86400.*1e3/)
	
	do n = 0,nsmooth-1 SACPR = wgt_runave_n_Wrap(SACPR, (/0.25,0.5,0.25/), 0,0) end do
	do n = 0,nsmooth-1 SADDT = wgt_runave_n_Wrap(SADDT, (/0.25,0.5,0.25/), 0,0) end do
	do n = 0,nsmooth-1 SAADV = wgt_runave_n_Wrap(SAADV, (/0.25,0.5,0.25/), 0,0) end do
	
	SAADV!0 = "time"
	SAADV!1 = "lev"
	SAADV&lev = SAlev2
	
	delete([/u,v,wmr,q,dt,adv,ddt,cpr/])
	
	SAADV2 = SAADV
	delete([/SAADV,SADDT,SACPR/])
;====================================================================================================
; Load ECMWF
;====================================================================================================
	infile = addfile("~/Data/DYNAMO/ECMWF/data/6hour.DYNAMO.ECMWF.U.nc","r")
	u = infile->U(:,:,{lat1:lat2},{lon1:lon2})
	infile = addfile("~/Data/DYNAMO/ECMWF/data/6hour.DYNAMO.ECMWF.V.nc","r")
	v = infile->V(:,:,{lat1:lat2},{lon1:lon2})
	infile = addfile("~/Data/DYNAMO/ECMWF/data/6hour.DYNAMO.ECMWF.Q.nc","r")
	q = infile->Q(:,:,{lat1:lat2},{lon1:lon2})
	
	EClev = q&lev
	
	dt 	= 6*3600.
	adv = calc_adv(q,u,v) *-1.
	ddt = calc_ddt(q,dt)
	cpr = (ddt-adv)
	
	ECDDT = block_avg( dim_avg_n( ddt ,(/2,3/)) ,4)
	ECADV = block_avg( dim_avg_n( adv ,(/2,3/)) ,4)
	ECCPR = block_avg( dim_avg_n( cpr ,(/2,3/)) ,4)
	
	ECADV = (/ECADV *86400.*1e3/)
	ECDDT = (/ECDDT *86400.*1e3/)
	ECCPR = (/ECCPR *86400.*1e3/)
	
	do n = 0,nsmooth-1 ECCPR = wgt_runave_n_Wrap(ECCPR, (/0.25,0.5,0.25/), 0,0) end do
	do n = 0,nsmooth-1 ECDDT = wgt_runave_n_Wrap(ECDDT, (/0.25,0.5,0.25/), 0,0) end do
	do n = 0,nsmooth-1 ECADV = wgt_runave_n_Wrap(ECADV, (/0.25,0.5,0.25/), 0,0) end do
	
	ECADV!0 = "time"
	ECADV!1 = "lev"
	ECADV&lev = EClev
	
	delete([/u,v,q,dt,adv,ddt,cpr/])
;====================================================================================================
; Plot Map
;====================================================================================================\
	wks = gsn_open_wks(fig_type,fig_file)
	;gsn_define_colormap(wks,"WhiteBlueGreenYellowRed")
	gsn_define_colormap(wks,"ncl_default")
	plot = new(3,graphic)
		res = True
		res@gsnDraw                         = False
		res@gsnFrame                        = False
		res@vpHeightF						= 0.3
		res@tmXTOn                          = False
		res@gsnLeftStringFontHeightF        = 0.015
		res@gsnCenterStringFontHeightF    	= 0.015
		res@gsnRightStringFontHeightF       = 0.015
		res@tmXBLabelFontHeightF            = 0.01
		res@tmYLLabelFontHeightF            = 0.01
		res@tiXAxisFontHeightF				= 0.015
		res@tiYAxisFontHeightF				= 0.015
		res@tmXBMajorOutwardLengthF         = 0.0
		res@tmXBMinorOutwardLengthF         = 0.0
		res@tmYLMajorOutwardLengthF         = 0.0
		res@tmYLMinorOutwardLengthF         = 0.0
		res@gsnLeftString                   = ""
		res@gsnCenterString              	= ""
		res@gsnRightString                  = ""
		res@tmXBLabelAngleF					= -45.
		res@lbTitlePosition					= "bottom"
		res@lbTitleFontHeightF				= 0.015
		res@lbLabelFontHeightF				= 0.015
		res@lbTitleString					= "[g kg~S~-1~N~ day~S~-1~N~]"
        res@gsnYAxisIrregular2Linear        = True
		
		lres = res
		lres@xyDashPattern              = 0
		lres@xyLineThicknessF           = 1.
		lres@xyLineColor                = "black"
		
		time = ispan(0,dimsizes(ECADV(:,0))-1,1)
		time@units = "days since 2011-10-01"
		cdtime = cd_calendar(time,2)
		tmres = True
		tmres@ttmFormat 		= "%D%c"
		tmres@ttmAxis   		= "XB"
		tmres@ttmMinorStride	= 1
		tmres@ttmMajorStride	= 5
		time_axis_labels(time,res,tmres)


		tres = res
		tres@trYReverse				= True
		tres@cnFillOn 				= True
		tres@cnLinesOn				= False
		tres@gsnSpreadColors		= True
		tres@tiYAxisString   		= "[hPa]"
		tres@cnLevelSelectionMode	= "ExplicitLevels"
		tres@cnLevels				= ispan(-20,20,2)/1e1
		tres@gsnRightString			= "Advective Tendency"
		
		if True then 
			tmp = SAADV2
			delete(SAADV2)
			SAADV2 = linint1_n_Wrap(SAlev2,tmp,False,EClev(::-1),0,1)
			delete(tmp)
		end if
		
		
		tres@gsnLeftString 			= "ECMWF Analysis"
	plot(0) = gsn_csm_contour(wks,ECADV(lev|:,time|:),tres)
	
		tres@gsnLeftString 			= "Sounding Array (2a)"
	plot(2) = gsn_csm_contour(wks,SAADV1(lev|:,time|:),tres)
	
		tres@gsnLeftString 			= "Sounding Array (2b)"
	plot(1) = gsn_csm_contour(wks,SAADV2(lev|:,time|:),tres)

	;overlay(plot(0) , gsn_csm_xy(wks,(/-1e3,1e3/),(/0.,0./),lres))

;====================================================================================================
; Finalize Figure
;====================================================================================================
		pres = True
		;pres@gsnPanelBottom                    = 0.1 
		pres@amJust                            = "TopLeft"
		pres@gsnPanelFigureStringsFontHeightF  = 0.015
		pres@gsnPanelFigureStrings             = (/"a","b","c","d"/) 

	;gsn_panel(wks,plot,(/dimsizes(plot),1/),pres)
	
	gsn_panel(wks,plot((/0,1/)),(/1,2/),pres)

  
	print("")
	print(" "+fig_file+"."+fig_type)
	print("")
	if fig_type.eq."png" then system("convert -trim "+fig_file+"."+fig_type+"   "+fig_file+"."+fig_type) end if

;====================================================================================================
;====================================================================================================
end
