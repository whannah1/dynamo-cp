load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/custom_functions_DYNAMO_LSF.ncl"
load "$NCARG_ROOT/custom_functions_DYNAMO_ECMWF.ncl"
load "$NCARG_ROOT/custom_functions.ncl"
begin

	fig_type = "png"
	fig_file = "~/Research/DYNAMO/CP/CP.joint.bin.v2"
	
	reduceRes	= False
	dailyAvg	= False
	
	debug 		= False
;====================================================================================================
;====================================================================================================		
	nvar = 1
	binsy = True
	binsy@verbose = False
    binsy@bin_min = -26
    binsy@bin_max =  26
    binsy@bin_spc =   2
    
	binsx = True
	binsx@verbose = False
    binsx@bin_min = 24
    binsx@bin_max = 70
    binsx@bin_spc =  2

	ybin 	= ispan(binsy@bin_min,binsy@bin_max,binsy@bin_spc)
	xbin 	= ispan(binsx@bin_min,binsx@bin_max,binsx@bin_spc)
    nbiny 	= dimsizes(ybin)
    nbinx 	= dimsizes(xbin)
    bdim 	= (/nvar,nbiny,nbinx/)
    binval = new(bdim,float)
    bincnt = new(bdim,float)
    bincnt = 0

	sa = "lsan"
	
	if sa.eq. "lsan" then
		lat1 =   0.
		lat2 =   7.
		lon1 =  72.
		lon2 =  79.
	end if
	if sa.eq. "lsas" then
		lat1 =  -7.
		lat2 =   0.
		lon1 =  72.
		lon2 =  79.
	end if
	
	lat1 = -10.
	lat2 =  10.
	lon1 =  60.
	lon2 =  90.
    
		res = True
		res@gsnDraw                         = False
		res@gsnFrame                        = False
		res@vpHeightF						= 0.5
		res@tmXTOn                          = False
		res@tmXBMinorOn						= False
		res@tmYLMinorOn						= False
		res@tmYRMinorOn						= False
		res@gsnLeftStringFontHeightF        = 0.01
		res@gsnCenterStringFontHeightF    	= 0.01
		res@gsnRightStringFontHeightF       = 0.01
		res@tmXBLabelFontHeightF            = 0.01
		res@tmYLLabelFontHeightF            = 0.01
		res@tiXAxisFontHeightF				= 0.01
		res@tiYAxisFontHeightF				= 0.01
		res@tmXBMajorOutwardLengthF         = 0.0
		res@tmXBMinorOutwardLengthF         = 0.0
		res@tmYLMajorOutwardLengthF         = 0.0
		res@tmYLMinorOutwardLengthF         = 0.0
		res@gsnLeftString                   = ""
		res@gsnCenterString              	= ""
		res@gsnRightString                  = ""
		;res@tmXBLabelAngleF					= -50.
		
		lres = res
		lres@xyDashPattern              = 0
		lres@xyLineThicknessF           = 1.
		lres@xyLineColor                = "black"
;====================================================================================================
; Load Sounding Array Data
;====================================================================================================
if False then
	print("Loading Sounding data...")
	ver = "2b"
	if ver.eq."2a" then ifile = "~/Data/DYNAMO/LSA/data/v2a/dynamo_basic_v2a_2011all.nc" end if
	if ver.eq."2b" then ifile = "~/Data/DYNAMO/LSA/data/v2b/dynamo_basic_v2b_2011all.nc" end if
	infile = addfile(ifile,"r")
	SAlat = infile->lat({lat1:lat2})
	SAlon = infile->lon({lon1:lon2})
	ps  = infile->ps(:,{lat1:lat2},{lon1:lon2})
	p   = infile->level
	p!0 = "lev"
	p&lev = p
	idp = calc_dP(p,ps) *100.
	iu  = infile->u(:,:,{lat1:lat2},{lon1:lon2})
	iv  = infile->v(:,:,{lat1:lat2},{lon1:lon2})
	wmr = infile->q(:,:,{lat1:lat2},{lon1:lon2})
	wmr = (/wmr/1000./)
	iq 	= wmr/(1.+wmr)
	copy_VarCoords(wmr,iq)
	dp = block_avg(idp,2)
	q  = block_avg(iq,2)
	u  = block_avg(iu,2)
	v  = block_avg(iv,2)
	
	dt = 6*3600.
	iSADDT = calc_ddt(q,dt)*86400.
	iSAADV = calc_adv(q,u,v) *-1.*86400.	
	SATPW = dim_sum_n(     q*dp/g,1) 
	SADDT = dim_sum_n(iSADDT*dp/g,1) 
	SAADV = dim_sum_n(iSAADV*dp/g,1) 
	
	SACPR = SADDT-SAADV
	delete([/wmr,q,u,v,p,ps,dp,iSADDT,iSAADV,idp,iq,iu,iv/])
end if
;====================================================================================================
; Load ECMWF Data
;====================================================================================================	
if False then 
	print("Loading ECMWF data...")
	t2 = (31+30+31)*4-1
	if debug then t2 = 10*4 end if
	infile = addfile("~/Data/DYNAMO/ECMWF/data/6hour.DYNAMO.ECMWF.U.nc","r")
	u = infile->U(:t2,:,{lat1:lat2},{lon1:lon2})
	infile = addfile("~/Data/DYNAMO/ECMWF/data/6hour.DYNAMO.ECMWF.V.nc","r")
	v = infile->V(:t2,:,{lat1:lat2},{lon1:lon2})
	infile = addfile("~/Data/DYNAMO/ECMWF/data/6hour.DYNAMO.ECMWF.Q.nc","r")
	q = infile->Q(:t2,:,{lat1:lat2},{lon1:lon2})
	
	infile = addfile("~/Data/DYNAMO/ECMWF/data/6hour.DYNAMO.ECMWF.Ps.nc","r")
	ps = infile->Ps(:t2,{lat1:lat2},{lon1:lon2})	
	p = q&lev*100.
	p@units = "Pa"
	p!0 = "lev"
	p&lev = p
	dp = calc_dP(p,ps)

	ifile = "~/Data/DYNAMO/ECMWF/data/6hour.DYNAMO.ECMWF.CWV.nc"
	infile = addfile(ifile,"r")
	ECTPW = infile->CWV(:t2,{lat1:lat2},{lon1:lon2}) 
	;ECTPW = dim_sum_n(q*dp/g,1)	
	
	if reduceRes then
		iECTPW = ECTPW
		ilat = q&lat
		ilon = q&lon
		iq = q
		iu = u
		iv = v
		idp = dp
		delete([/u,v,q,dp,ECTPW/])
		q  = area_hi2lores_Wrap(ilon,ilat,iq ,False,1.,SAlon,SAlat,False)
		u  = area_hi2lores_Wrap(ilon,ilat,iu ,False,1.,SAlon,SAlat,False)
		v  = area_hi2lores_Wrap(ilon,ilat,iv ,False,1.,SAlon,SAlat,False)
		dp = area_hi2lores_Wrap(ilon,ilat,idp,False,1.,SAlon,SAlat,False)
		ECTPW = area_hi2lores_Wrap(ilon,ilat,iECTPW ,False,1.,SAlon,SAlat,False)
		delete([/iu,iv,iq,idp,iECTPW/])
	end if
	
	iECADV = calc_adv(q,u,v) *-1.*86400.
	dt 	= 6*3600.
	iECDDT = calc_ddt(q,dt)*86400.
	
	ECDDT = dim_sum_n(iECDDT*dp/g,1)
	ECADV = dim_sum_n(iECADV*dp/g,1)
	ECCPR = ECDDT - ECADV
	
	delete([/q,u,v,iECDDT,iECADV,p,ps,dp/])
else
	tfile = "~/Data/DYNAMO/ECMWF/data/6hour.DYNAMO.ECMWF.CP.nc"
	infile = addfile(tfile,"r")
	iECTPW = infile->ECTPW;(:,{lat1:lat2},{lon1:lon2})
	iECDDT = infile->ECDDT;(:,{lat1:lat2},{lon1:lon2})
	iECCPR = infile->ECCPR;(:,{lat1:lat2},{lon1:lon2})
	copy_VarCoords(iECTPW,iECDDT)
	copy_VarCoords(iECTPW,iECCPR)
	ECTPW = iECTPW(:,{lat1:lat2},{lon1:lon2})
	ECDDT = iECDDT(:,{lat1:lat2},{lon1:lon2})
	ECCPR = iECCPR(:,{lat1:lat2},{lon1:lon2})
end if

;====================================================================================================
; Load ECLHF
;====================================================================================================
if True then
	printline()
	print("Loading ECLHF...")
	infile = addfile("~/Data/DYNAMO/ECMWF/data/12hour.00-12.DYNAMO.ECMWF.LHF.nc","r")
	EClat = infile->lat({lat1:lat2})
	EClon = infile->lon({lon1:lon2})
	t1 = 365 - (31+30+31)
	t2 = 365 - 1

	ECLHF = block_avg( infile->LHF(:,{lat1:lat2},{lon1:lon2}) ,2)
	ECLHF = (/ECLHF/Lv*86400*-1./)
	ECLHF@long_name 	= "Evaporation"
	ECLHF@units			= "mm/day"

	ECLHF = dim_rmvmean_n_Wrap(ECLHF,0)
	
	ECCPR = (/ ECCPR - ECLHF /)
end if
;====================================================================================================
; Bin data
;====================================================================================================
    do v = 0,nvar-1
        print("    v = "+v+"    v%nvar = "+v%nvar)
	    if v.eq.0 then 
	    	Vz = ECDDT
	    	Vy = ECCPR 
	    	Vx = ECTPW 
	    end if

        binval(v,:,:) = bin_ZbyYX(Vz,Vy,Vx,binsy,binsx,0)
        bincnt(v,:,:) = bin_ZbyYX(Vz,Vy,Vx,binsy,binsx,1)
        
        delete([/Vz,Vy,Vx/])
    end do
    printline()
    bincnt!0 = "var"
    bincnt!1 = "ybin"
    bincnt!2 = "xbin"
    bincnt&ybin = ybin
    bincnt&xbin = xbin
;====================================================================================================
;====================================================================================================
	wks = gsn_open_wks(fig_type,fig_file)
	;plot = new(nvar*2,graphic)
	plot = new(nvar,graphic)
		cres = res
		cres@cnFillOn 			= True
		cres@cnLinesOn			= False
		cres@lbOrientation 		= "vertical"
		;cres@cnFillPalette		= ""
		cres@tiYAxisString   	= "Lagrangian Tendency [mm day~S~-1~N~]"
		cres@tiXAxisString   	= "TPW [mm]"
		cres@gsnRightString   	= "% of Obsrevations"
		
		;cres@cnLevelSelectionMode	= "ExplicitLevels"
		;cres@cnLevels				= ispan(5,95,10)/1e2
		
	do v = 0,nvar-1
		p1 = v
		p2 = v+nvar
		if v.eq.0 then cres@gsnLeftString = "ECMWF" end if
		;plot(p1) = gsn_csm_contour(wks,binval(v,:,:),cres)
		;plot(p2) = gsn_csm_contour(wks,bincnt(v,:,:),cres)
		;overlay(plot(p1) , gsn_csm_xy(wks,(/-1e3,1e3/),(/0.,0./),lres))
		;overlay(plot(p2) , gsn_csm_xy(wks,(/-1e3,1e3/),(/0.,0./),lres))
		
		plot(v) = gsn_csm_contour(wks,bincnt(v,:,:),cres)
			tres = cres
			tres@cnFillOn			= False
			tres@cnLinesOn			= True
			tres@cnLineThicknessF	= 3.
			tres@gsnContourZeroLineThicknessF	= 6.
			tres@gsnContourNegLineDashPattern	= 1
		overlay(plot(v) , gsn_csm_contour(wks,binval(v,:,:),tres))
		delete(tres)
		overlay(plot(v) , gsn_csm_xy(wks,(/-1e3,1e3/),(/0.,0./),lres))
		
		if v.eq.0 then 
			;overlay(plot(p1) , gsn_csm_xy(wks,(/1.,1./)*avg(ECTPW),(/-1e3,1e3/),lres)) 
			;overlay(plot(p2) , gsn_csm_xy(wks,(/1.,1./)*avg(ECTPW),(/-1e3,1e3/),lres)) 
			overlay(plot(v) , gsn_csm_xy(wks,(/1.,1./)*avg(ECTPW),(/-1e3,1e3/),lres)) 
		end if
	end do
	
	
	
;====================================================================================================
; Finalize Figure
;====================================================================================================
		pres = True
		pres@gsnFrame                           = True

	gsn_panel(wks,plot,(/dimsizes(plot),1/),pres)

  
	print("")
	print(" "+fig_file+"."+fig_type)
	print("")
	if fig_type.eq."png" then system("convert -trim "+fig_file+"."+fig_type+"   "+fig_file+"."+fig_type) end if

;====================================================================================================
;====================================================================================================
end
