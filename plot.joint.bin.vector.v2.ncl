\load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/custom_functions_DYNAMO_LSF.ncl"
load "$NCARG_ROOT/custom_functions_DYNAMO_ECMWF.ncl"
load "$NCARG_ROOT/custom_functions.ncl"
begin

	fig_type = "png"
	fig_file = "~/Research/DYNAMO/CP/joint.bin.vector.v2"
	
	reduceRes	= False
	debug 		= False
;====================================================================================================
;====================================================================================================
	lat1 = -10.
	lat2 =  10.
	lon1 =  60.
	lon2 =  90.
	
	fig_type@wkWidth  = 2048
	fig_type@wkHeight = 2048
	
		res = setres_default()
		res@vpHeightF						= 0.5
		res@tmXTOn                          = False
		res@tmXBMinorOn						= False
		res@tmYLMinorOn						= False
		res@tmYRMinorOn						= False
		res@gsnLeftStringFontHeightF        = 0.01
		res@gsnCenterStringFontHeightF    	= 0.01
		res@gsnRightStringFontHeightF       = 0.01
		res@tmXBMajorOutwardLengthF         = 0.0
		res@tmXBMinorOutwardLengthF         = 0.0
		res@tmYLMajorOutwardLengthF         = 0.0
		res@tmYLMinorOutwardLengthF         = 0.0
		res@gsnLeftString                   = ""
		res@gsnCenterString              	= ""
		res@gsnRightString                  = ""
		lres = res
		lres@xyDashPattern              = 0
		lres@xyLineThicknessF           = 1.
		lres@xyLineColor                = "black"
;====================================================================================================
; Load ECMWF Data
;====================================================================================================	
    if reduceRes then
        ifile = "~/Data/DYNAMO/ECMWF/data/6hour.DYNAMO.ECMWF.CP.SAres.nc"
    else
        ifile = "~/Data/DYNAMO/ECMWF/data/6hour.DYNAMO.ECMWF.CP.nc"
    end if
    infile = addfile(ifile,"r")

    nblk = 4
    ECTPW = infile->ECTPW(:,{lat1:lat2},{lon1:lon2})
    ;ECDDT = infile->ECDDT(:,{lat1:lat2},{lon1:lon2})
    ;ECADV = infile->ECADV(:,{lat1:lat2},{lon1:lon2})
    ECCPR = infile->ECCPR(:,{lat1:lat2},{lon1:lon2})
;====================================================================================================
; Bin data
;====================================================================================================
	nvar = 1
	;-------------------------------------------------------------
	; Define bins
	;-------------------------------------------------------------
	binsy = True
	binsy@verbose = False
    binsy@bin_min = -20
    binsy@bin_max =  20
    binsy@bin_spc =   2
	binsx = True
	binsx@verbose = False
    binsx@bin_min = 30
    binsx@bin_max = 64
    binsx@bin_spc =  2
	;-------------------------------------------------------------
	;-------------------------------------------------------------
	ybin 	= ispan(binsy@bin_min,binsy@bin_max,binsy@bin_spc)
	xbin 	= ispan(binsx@bin_min,binsx@bin_max,binsx@bin_spc)
    nbiny 	= dimsizes(ybin)
    nbinx 	= dimsizes(xbin)
    bdim 	= (/nvar,nbiny,nbinx/)
    binval = new(bdim,float)
    binu   = new(bdim,float)
    binv   = new(bdim,float)
    bincnt = new(bdim,float)
    binavg = new(nvar,float)
	;-------------------------------------------------------------
	;-------------------------------------------------------------
    do v = 0,nvar-1
        print("    v = "+v+"    v%nvar = "+v%nvar)
	   	;-------------------------------------------------------------
		;-------------------------------------------------------------
	   	Vy = ECCPR
		Vx = ECTPW
		;-------------------------------------------------------------
		;-------------------------------------------------------------
	    bsz = 1
	    if bsz.gt.1 then
			tVx = block_avg(Vx,bsz)
			tVy = block_avg(Vy,bsz)
			delete([/Vx,Vy/])
			Vx = tVx
			Vy = tVy
			delete([/tVx,tVy/])
		end if
	    ;-------------------------------------------------------------
		;-------------------------------------------------------------
	    ;if v.eq.1 then Vx = (/ Vx - ECLHF /) end if
	    ;-------------------------------------------------------------
		;-------------------------------------------------------------
	    nt = dimsizes(Vy(:,0,0))
	    Vu = new(dimsizes(Vy),float)
	    Vv = new(dimsizes(Vy),float)
	    Vu(0:nt-2,:,:) = ( Vx(1:nt-1,:,:) - Vx(0:nt-2,:,:) ) /6. /bsz *24. 
	    Vv(0:nt-2,:,:) = ( Vy(1:nt-1,:,:) - Vy(0:nt-2,:,:) ) /6. /bsz *24. /10.
	    ;-------------------------------------------------------------
		;-------------------------------------------------------------
        Vu@long_name = "U"
        Vv@long_name = "V"
        Vx@long_name = "X"
        Vy@long_name = "Y"
			printline()
			printMAMS(Vu)
			printMAMS(Vv)
			printMAMS(Vx)
			printMAMS(Vy)
			printline()
        binu  (v,:,:) = bin_ZbyYX(Vu,Vy,Vx,binsy,binsx,0)
        binv  (v,:,:) = bin_ZbyYX(Vv,Vy,Vx,binsy,binsx,0)
        bincnt(v,:,:) = bin_ZbyYX(Vy,Vy,Vx,binsy,binsx,1)
        binavg(v) = avg(Vx)
        ;-------------------------------------------------------------
		;-------------------------------------------------------------
        delete([/Vu,Vv/])
        delete([/Vy,Vx/])
    end do
    ;-------------------------------------------------------------
	;-------------------------------------------------------------
    printline()
    bincnt!0 = "var"
    bincnt!1 = "ybin"
    bincnt!2 = "xbin"
    bincnt&ybin = ybin
    bincnt&xbin = xbin
;====================================================================================================
;====================================================================================================
	wks = gsn_open_wks(fig_type,fig_file)
	plot = new(nvar,graphic)
		cres = res
		cres@cnFillOn 			= False
		cres@cnLinesOn			= True
		cres@cnLineThicknessF	= 6.
		cres@cnLineLabelsOn		= False
		;cres@cnInfoLabelOn		= False
		cres@lbOrientation 		= "vertical"
		;cres@cnFillMode 		= "RasterFill"
		cres@tiYAxisString   	= "Lagrangian Tendency [mm day~S~-1~N~]"
		cres@tiXAxisString   	= "CWV [mm]"
		;cres@gsnRightString   	= "% of Obsrevations"
		;cres@cnLevelSelectionMode	= "ExplicitLevels"
		;cres@cnLevels				= ispan(5,95,10)/1e2
		
	do v = 0,nvar-1
		p1 = v
		p2 = v+nvar
		;-------------------------------------------------------------
		;-------------------------------------------------------------
			cres@gsnLeftString = ""
		plot(v) = gsn_csm_contour(wks,bincnt(v,:,:),cres)
		
		overlay(plot(v) , gsn_csm_xy(wks,(/-1e3,1e3/),(/0.,0./),lres))
		overlay(plot(v) , gsn_csm_xy(wks,(/1.,1./)*binavg(v),(/-1e3,1e3/),lres)) 
		;-------------------------------------------------------------
		;-------------------------------------------------------------
			vres = True
			vres@gsnDraw                    = False
			vres@gsnFrame                   = False
			vres@vcRefAnnoOn                = False
			vres@vcRefMagnitudeF            = 10.               ; define vector ref mag
			vres@vcRefLengthF               = 0.045             ; define length of vec ref
			vres@vcRefAnnoOrthogonalPosF    = -.5               ; move ref vector
			vres@vcRefAnnoArrowLineColor    = "black"           ; change ref vector color
			vres@vcRefAnnoArrowUseVecColor  = False             ; don't use vec color for ref
			vres@vcGlyphStyle               = "CurlyVector"     ; turn on curley vectors
			vres@vcLineArrowColor           = "black"           ; change vector color
			vres@vcLineArrowThicknessF      = 3.               ; change vector thickness
			vres@vcVectorDrawOrder          = "PostDraw"        ; draw vectors last
			vres@vcMinMagnitudeF            = 1.
			vres@gsnLeftString              = ""
			vres@gsnRightString             = ""
			vres@gsnCenterString            = ""
			vres@vcMinDistanceF             = 0.017
		
			printline()
			printMAM(binu)
			printline()
			printMAM(binv)
			printline()

        binu = where(bincnt.ge.0.1,binu,binu@_FillValue)
        binv = where(bincnt.ge.0.1,binv,binv@_FillValue)

		;overlay( plot(v) , gsn_csm_vector(wks,binu(v,:,:),binv(v,:,:),vres) )
		;-------------------------------------------------------------
		;------------------------------------------------------------- 
	end do
;====================================================================================================
; Finalize Figure
;====================================================================================================
		pres = True
		pres@gsnFrame                           = True
	gsn_panel(wks,plot,(/dimsizes(plot),1/),pres)
  
	print("")
	print(" "+fig_file+"."+fig_type)
	print("")
	if fig_type.eq."png" then system("convert -trim "+fig_file+"."+fig_type+"   "+fig_file+"."+fig_type) end if
;====================================================================================================
;====================================================================================================
end
