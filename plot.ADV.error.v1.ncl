load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/contrib/time_axis_labels.ncl"
load "$NCARG_ROOT/custom_functions_DYNAMO_ECMWF.ncl"
load "$NCARG_ROOT/custom_functions.ncl"
begin
	
	fig_type = "png"
	fig_file = "~/Research/DYNAMO/CP/ADV.error.v1"
	
	verbose = False

	lat1 =  -40.
	lat2 =   40.
	lon1 =    0.
	lon2 =  360.
	
	recalc = False

;====================================================================================================
; Load ECMWF
;====================================================================================================
	tfile = "~/Data/DYNAMO/ECMWF/data/ADV.error.nc"
	
	ufile = addfile("~/Data/DYNAMO/ECMWF/data/6hour.DYNAMO.ECMWF.U.nc","r")
	vfile = addfile("~/Data/DYNAMO/ECMWF/data/6hour.DYNAMO.ECMWF.V.nc","r")
	qfile = addfile("~/Data/DYNAMO/ECMWF/data/6hour.DYNAMO.ECMWF.Q.nc","r")
	lat = ufile->lat({lat1:lat2})
	lon = ufile->lon({lon1:lon2})
	
	if recalc then
		print("Loading EC data...")
		
		t1 = 25*4
		t2 = t1+5*4
	
		if verbose then printMM(lat) end if
		if verbose then printMM(lon) end if

		u = ufile->U(t1:t2,:,{lat1:lat2},{lon1:lon2})
		v = vfile->V(t1:t2,:,{lat1:lat2},{lon1:lon2})
		q = qfile->Q(t1:t2,:,{lat1:lat2},{lon1:lon2})
	
		infile = addfile("~/Data/DYNAMO/ECMWF/data/6hour.DYNAMO.ECMWF.Ps.nc","r")
		ps = infile->Ps(t1:t2,{lat1:lat2},{lon1:lon2})	
		lev = q&lev 
		p   = lev*100.
		p!0 = "lev"
		p&lev = p
		dp = calc_dP(p,ps)
	
		print("Calculating advection...")
	
		adv = calc_adv(q,u,v) 
	
		delete([/u,v,q/])
	
		adv = (/adv *86400.*-1./)
		;adv = adv*1e3
	
		adv!0 = "time"
		adv!1 = "lev"
		adv!2 = "lat"
		adv!3 = "lon"
		adv&lev = lev
		adv&lat = lat
		adv&lon = lon
	
		print("Integrating advection...")
	
		ADV = dim_sum_n(adv*dp/g,1) /dim_sum_n(dp/g,1)
		
		copy_VarCoords(adv(:,0,:,:),ADV)
		
		if isfilepresent(tfile) then system("rm "+tfile) end if
		outfile = addfile(tfile,"c")
		outfile->adv = adv
		outfile->ADV = ADV
		outfile->dp  = dp
	else
		infile = addfile(tfile,"r")
		adv = infile->adv	(:1,:,:,:)
		ADV = infile->ADV	(:1,:,:)
		dp  = infile->dp	(:1,:,:,:)
	end if

;====================================================================================================
;====================================================================================================	
	printline()
	print("Calculating error...")
	
	e = ( adv - conform(adv,ADV,(/0,2,3/)) )*dp/g
	
	coldp = dim_sum_n( dp/g ,1)
	
	A = dim_avg_n_Wrap(ADV*coldp,0)
	
	A = (/abs(A)/)
	
	;E = sqrt( dim_avg_n( dim_sum_n( e^2. *dp/g,1)/dim_sum_n(dp/g,1) ,0) )
	
	;E1 = sqrt( dim_avg_n( dim_sum_n( e^2. *dp/g,1)/dim_sum_n(dp/g,1) ,0) )
	;E1 = sqrt( dim_avg_n( dim_avg_n( e^2. ,1) ,0) )
	E1 = dim_avg_n( dim_avg_n( abs(e) ,1) ,0)
	
	;E2 = sqrt( dim_avg_n( dim_sum_n( e^2. *dp/g,1) ,0) )
	;E2 = sqrt( dim_avg_n( dim_sum_n( (e*dp/g)^2.,1) ,0) )
	;E2 = sqrt( dim_avg_n( dim_max_n( e^2. ,1) ,0) )
	E2 = dim_avg_n( dim_max_n( abs(e) ,1) ,0)
	
	E1!0 = "lat"
	E1!1 = "lon"
	E1&lat = lat
	E1&lon = lon	
	copy_VarCoords(E1,E2)
	copy_VarCoords(E1,A)
	
;====================================================================================================
; Plot Map
;====================================================================================================\
	fig_type@wkWidth  = 2048
	fig_type@wkHeight = 2048
	wks = gsn_open_wks(fig_type,fig_file)
	;gsn_define_colormap(wks,"WhiteBlueGreenYellowRed")
	;gsn_define_colormap(wks,"ncl_default")
	plot = new(3,graphic)
		res = True
		res@gsnDraw                         = False
		res@gsnFrame                        = False
		res@vpHeightF						= 0.3
		;res@tmXTOn                          = False
		res@tmXBMinorOn						= False
		res@tmYLMinorOn						= False
		res@gsnLeftStringFontHeightF        = 0.015
		res@gsnCenterStringFontHeightF    	= 0.015
		res@gsnRightStringFontHeightF       = 0.015
		res@tmXBLabelFontHeightF            = 0.01
		res@tmYLLabelFontHeightF            = 0.01
		;res@tiXAxisFontHeightF				= 0.01
		;res@tiYAxisFontHeightF				= 0.01
		res@tmXBMajorOutwardLengthF         = 0.0
		res@tmXBMinorOutwardLengthF         = 0.0
		res@tmYLMajorOutwardLengthF         = 0.0
		res@tmYLMinorOutwardLengthF         = 0.0
		res@gsnLeftString                   = ""
		res@gsnCenterString              	= ""
		res@gsnRightString                  = ""
		
		lres = res
		lres@xyDashPattern              = 0
		lres@xyLineThicknessF           = 1.
		lres@xyLineColor                = "black"
		
		res@gsnAddCyclic					= False
		res@lbTitlePosition					= "bottom"
		res@lbLabelFontHeightF				= 0.01
		res@lbTopMarginF					= 0.08
		res@lbTitleString					= ""
		res@trYReverse				= True
		res@cnFillOn 				= True
		res@cnLinesOn				= False
		res@cnFillMode 				= "RasterFill"
		;res@gsnSpreadColors			= True
		;res@cnSpanFillPalette		= True
		res@mpLimitMode				= "LatLon"
		res@mpMinLatF				= lat1
		res@mpMaxLatF				= lat2
		;res@mpMinLonF				= lon1
		;res@mpMaxLonF				= lon2
		res@mpCenterLonF			= 180.
		res@cnLevelSelectionMode	= "ExplicitLevels"
		res@gsnRightString			= "[mm day~S~-1~N~]"
		res@lbLabelBarOn			= False
		res@cnLineLabelsOn			= False
		res@cnInfoLabelOn			= False
		
		;res@cnFillPalette			= "ncl_default"
		res@cnFillPalette			= "WhiteBlueGreenYellowRed"
		res@gsnLeftString 			= "Total Column Advection Magnitude"
		res@cnLevels				= ispan(5,500,5)/1e1
		
	plot(0) = gsn_csm_contour_map(wks,A,res)
	
		;if isatt(res,"cnLevels") then delete(res@cnLevels) end if
		res@cnFillPalette			= "WhiteBlueGreenYellowRed"
		res@gsnLeftString 			= "Average Deviation Magnitude"
		;res@cnLevels				= ispan(3,180,3)/1e1
		;res@cnLevels				= ispan(1,100,1)
		
	;E1 = (/ E1/A *100./)
	plot(1) = gsn_csm_contour_map(wks,E1,res)
	
		;if isatt(res,"cnLevels") then delete(res@cnLevels) end if
		res@lbLabelBarOn			= True
		res@gsnLeftString 			= "Maximum Deviation Magnitude"
		;res@cnLevels				= ispan(1,60,1)
		;res@cnLevels				= ispan(1,100,1)
		
	;E2 = (/ E2/A *100./)
	plot(2) = gsn_csm_contour_map(wks,E2,res)
	
	overlay(plot(0) , gsn_csm_xy(wks,(/-1e3,1e3/),(/0.,0./),lres))
	overlay(plot(1) , gsn_csm_xy(wks,(/-1e3,1e3/),(/0.,0./),lres))

;====================================================================================================
; Finalize Figure
;====================================================================================================
		pres = True
		;pres@gsnPanelBottom                    = 0.1 
		;pres@amJust                            = "TopLeft"
		;pres@gsnPanelFigureStringsFontHeightF  = 0.015
		;pres@gsnPanelFigureStrings             = (/"a","b","c","d"/) 

	gsn_panel(wks,plot,(/dimsizes(plot),1/),pres)
  
	print("")
	print(" "+fig_file+"."+fig_type)
	print("")
	if fig_type.eq."png" then system("convert -trim "+fig_file+"."+fig_type+"   "+fig_file+"."+fig_type) end if

;====================================================================================================
;====================================================================================================
end
