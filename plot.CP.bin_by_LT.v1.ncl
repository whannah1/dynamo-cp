load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/custom_functions_DYNAMO_LSF.ncl"
load "$NCARG_ROOT/custom_functions_DYNAMO_ECMWF.ncl"
load "$NCARG_ROOT/custom_functions.ncl"
begin

	fig_type = "png"
	fig_file = "~/Research/DYNAMO/CP/CP.bin_by_LT.v1"
	
	reduceRes	= True
	dailyAvg	= False
	
	debug 		= False
;====================================================================================================
;====================================================================================================		
	nvar = 1
	bins = True
	bins@verbose = False
    bins@bin_min = -30
    bins@bin_max =  30
    bins@bin_spc =   2

	xbin 	= ispan(bins@bin_min,bins@bin_max,bins@bin_spc)
    num_bin = dimsizes(xbin)
    bdim 	= (/nvar,num_bin/)
    ;binval = new(bdim,float)
    bincnt = new(bdim,float)
    bincnt = 0

	sa = "lsan"
	
	if sa.eq. "lsan" then
		lat1 =   0.
		lat2 =   7.
		lon1 =  72.
		lon2 =  79.
	end if
	if sa.eq. "lsas" then
		lat1 =  -7.
		lat2 =   0.
		lon1 =  72.
		lon2 =  79.
	end if
	
	lat1 = -15.
	lat2 =  15.
	lon1 =  60.
	lon2 =  90.
    
	wks = gsn_open_wks(fig_type,fig_file)
	plot = new(nvar,graphic)
	;plot = new(6,graphic)
		res = True
		res@gsnDraw                         = False
		res@gsnFrame                        = False
		;res@vpHeightF						= 0.3
		res@tmXTOn                          = False
		res@tmXBMinorOn						= False
		res@tmYLMinorOn						= False
		res@tmYRMinorOn						= False
		res@gsnLeftStringFontHeightF        = 0.015
		res@gsnCenterStringFontHeightF    	= 0.015
		res@gsnRightStringFontHeightF       = 0.015
		res@tmXBLabelFontHeightF            = 0.01
		res@tmYLLabelFontHeightF            = 0.01
		res@tiXAxisFontHeightF				= 0.015
		res@tiYAxisFontHeightF				= 0.015
		res@tmXBMajorOutwardLengthF         = 0.0
		res@tmXBMinorOutwardLengthF         = 0.0
		res@tmYLMajorOutwardLengthF         = 0.0
		res@tmYLMinorOutwardLengthF         = 0.0
		res@gsnLeftString                   = ""
		res@gsnCenterString              	= ""
		res@gsnRightString                  = ""
		;res@tmXBLabelAngleF					= -50.
		
		lres = res
		lres@xyDashPattern              = 0
		lres@xyLineThicknessF           = 3.
		lres@xyLineColor                = "black"
		
		res@cnFillOn 				= True
		res@cnLinesOn 				= False
		res@cnSpanFillPalette		= True
		res@trYReverse				= True
;====================================================================================================
; Load Sounding Array Data
;====================================================================================================
	ver = "2b"
	if ver.eq."2a" then ifile = "~/Data/DYNAMO/LSA/data/v2a/dynamo_basic_v2a_2011all.nc" end if
	if ver.eq."2b" then ifile = "~/Data/DYNAMO/LSA/data/v2b/dynamo_basic_v2b_2011all.nc" end if
	infile = addfile(ifile,"r")
	SAlat = infile->lat({lat1:lat2})
	SAlon = infile->lon({lon1:lon2})
	ps  = infile->ps(:,{lat1:lat2},{lon1:lon2})
	p   = infile->level
	p!0 = "lev"
	p&lev = p
	idp = calc_dP(p,ps) *100.
	iu  = infile->u(:,:,{lat1:lat2},{lon1:lon2})
	iv  = infile->v(:,:,{lat1:lat2},{lon1:lon2})
	wmr = infile->q(:,:,{lat1:lat2},{lon1:lon2})
	wmr = (/wmr/1000./)
	iq 	= wmr/(1.+wmr)
	copy_VarCoords(wmr,iq)
	dp = block_avg(idp,2)
	q  = block_avg(iq,2)
	u  = block_avg(iu,2)
	v  = block_avg(iv,2)
	;dt 	= 3*3600.
	;iSADDT = calc_ddt(q,dt)*86400.
	;iSAADV = calc_adv(q,u,v) *-1.*86400.	
	;SATPW = block_avg( dim_sum_n(        q*dp/g,1)  ,2)  ; divide by density and convert to mm
	;SADDT = block_avg( dim_sum_n(   iSADDT*dp/g,1)  ,2)
	;SAADV = block_avg( dim_sum_n(   iSAADV*dp/g,1)  ,2)
	
	dt = 6*3600.
	iSADDT = calc_ddt(q,dt)*86400.
	iSAADV = calc_adv(q,u,v) *-1.*86400.	
	SATPW = dim_sum_n(     q*dp/g,1) 
	SADDT = dim_sum_n(iSADDT*dp/g,1) 
	SAADV = dim_sum_n(iSAADV*dp/g,1) 
	
	SACPR = SADDT-SAADV
	delete([/wmr,q,u,v,p,ps,dp,iSADDT,iSAADV,idp,iq,iu,iv/])
;====================================================================================================
; Load MIMIC Data
;====================================================================================================
if False then
	ifile  = "~/Data/DYNAMO/MIMIC/cimss_morphed_tpw_DYNAMO_2011.nc"
	infile = addfile(ifile,"r")
	tmp = infile->tpw(:,{lat1:lat2},{lon1:lon2}) 
	iMITPW = new(dimsizes(tmp),float)
	iMITPW = tofloat( tmp )
	delete(tmp)
	iMITPW = tofloat( where(isnan_ieee(iMITPW),iMITPW@_FillValue,iMITPW) ) 
	bsz = 6
	MITPW = block_avg( dim_avg_n_Wrap(iMITPW,(/1,2/)) ,bsz)
	printline()
	printVarSummary(MITPW)
end if
;====================================================================================================
; Load ECMWF Data
;====================================================================================================	
	t2 = (31+30+31)*4-1
	if debug then t2 = 10*4 end if
	infile = addfile("~/Data/DYNAMO/ECMWF/data/6hour.DYNAMO.ECMWF.U.nc","r")
	u = infile->U(:t2,:,{lat1:lat2},{lon1:lon2})
	infile = addfile("~/Data/DYNAMO/ECMWF/data/6hour.DYNAMO.ECMWF.V.nc","r")
	v = infile->V(:t2,:,{lat1:lat2},{lon1:lon2})
	infile = addfile("~/Data/DYNAMO/ECMWF/data/6hour.DYNAMO.ECMWF.Q.nc","r")
	q = infile->Q(:t2,:,{lat1:lat2},{lon1:lon2})
	
	EClev = q&lev
	
	infile = addfile("~/Data/DYNAMO/ECMWF/data/6hour.DYNAMO.ECMWF.Ps.nc","r")
	ps = infile->Ps(:t2,{lat1:lat2},{lon1:lon2})	
	p = EClev*100.
	p@units = "Pa"
	p!0 = "lev"
	p&lev = p
	dp = calc_dP(p,ps)

	ifile = "~/Data/DYNAMO/ECMWF/data/6hour.DYNAMO.ECMWF.CWV.nc"
	infile = addfile(ifile,"r")
	ECTPW = infile->CWV(:t2,{lat1:lat2},{lon1:lon2}) 
	;ECTPW = dim_sum_n(q*dp/g,1)	
	
	if reduceRes then
		iECTPW = ECTPW
		ilat = q&lat
		ilon = q&lon
		iq = q
		iu = u
		iv = v
		idp = dp
		delete([/u,v,q,dp,ECTPW/])
		q  = area_hi2lores_Wrap(ilon,ilat,iq ,False,1.,SAlon,SAlat,False)
		u  = area_hi2lores_Wrap(ilon,ilat,iu ,False,1.,SAlon,SAlat,False)
		v  = area_hi2lores_Wrap(ilon,ilat,iv ,False,1.,SAlon,SAlat,False)
		dp = area_hi2lores_Wrap(ilon,ilat,idp,False,1.,SAlon,SAlat,False)
		ECTPW = area_hi2lores_Wrap(ilon,ilat,iECTPW ,False,1.,SAlon,SAlat,False)
		delete([/iu,iv,iq,idp,iECTPW/])
	end if
	
	iECDDT = calc_ddt(q,6*3600.)		 *86400.
	iECADV = calc_adv(q,u,v) *-1.*86400.
	iECCPR = iECDDT - iECADV
	
	ECDDT = dim_sum_n(iECDDT*dp/g,1)
	ECADV = dim_sum_n(iECADV*dp/g,1)
	ECCPR = ECDDT - ECADV
	
	delete([/q,u,v,iECDDT,iECADV,p,ps,dp/])
;====================================================================================================
; Create Plot 
;====================================================================================================
	
    cond1 = (ECDDT.gt.0)
    cond2 = (ECDDT.lt.0)
    tcond = (ECTPW.gt.45).and.(ECTPW.lt.50)
    cond3 = cond1.and.tcond
    cond4 = cond2.and.tcond
    
    ycond1 = conform(iECCPR,cond1,(/0,2,3/))
    ycond2 = conform(iECCPR,cond2,(/0,2,3/))
    ycond3 = conform(iECCPR,cond3,(/0,2,3/))
    ycond4 = conform(iECCPR,cond4,(/0,2,3/))
    
    nlev = dimsizes(EClev)
    binval = new((/nvar,nlev,num_bin/),float)
    binval!0 = "var"
    binval!1 = "lev"
    binval!2 = "bin"
    binval&lev = EClev
    binval&bin = xbin
    

    print("Binning data...s")
    do v = 0,nvar-1
        print("    v = "+v+"    v%nvar = "+v%nvar)
        
        if v.eq.0 then Vy = iECCPR  end if
        if v.eq.0 then Vx =  ECCPR 	end if
        
        ;if v.eq.0 then Vy = where( ycond1 ,iECCPR,iECCPR@_FillValue)  end if
        ;if v.eq.1 then Vy = where( ycond2 ,iECCPR,iECCPR@_FillValue)  end if
        ;if v.eq.2 then Vy = where( ycond3 ,iECCPR,iECCPR@_FillValue)  end if
        ;if v.eq.3 then Vy = where( ycond4 ,iECCPR,iECCPR@_FillValue)  end if
        
        ;Vx = new(dimsizes(cond1),float)
        ;if v.eq.0 then Vx = where( cond1 ,iVx,iVx@_FillValue)  end if
        ;if v.eq.1 then Vx = where( cond2 ,iVx,iVx@_FillValue)  end if
        ;if v.eq.2 then Vx = where( cond3 ,iVx,iVx@_FillValue)  end if
        ;if v.eq.3 then Vx = where( cond4 ,iVx,iVx@_FillValue)  end if
        
        
		; Daily Average
		if dailyAvg then
			tVx = Vx
			tVy = Vy
			delete([/Vx,Vy/])
			Vx = block_avg(tVx,4)
			Vy = block_avg(tVy,4)
			delete([/tVx,tVy/])
		end if
        
        do k = 0,nlev-1
			tmp = bin_YbyX(Vy(:,k,:,:),Vx,bins)
        	binval(v,k,:) = tmp
        	if k.eq.0 then bincnt(v,:) = tmp@pctcnt end if
        	delete(tmp)
        end do
        delete([/Vy,Vx/])
        
    end do
;====================================================================================================
;====================================================================================================
		clr = (/"blue","red","blue","red","blue","red"/)
		tres = res
		tres@gsnCenterString   	= "CP binned by LT"
		;tres@tiXAxisString   	= ""
		;tres@tiYAxisString   	= "[mm day~S~-1~N~]"
		tres@tmYROn				= False
		tres@cnLevelSelectionMode	= "ExplicitLevels"
		tres@cnLevels	= ispan(-8,8,1)*1e-3
		
		lres2 = lres
		lres@tiXAxisString  	= "[% of observations]"
		lres@xyLineThicknessF	= 1.
		lres2@xyLineThicknessF	= 4.
		
	do v = 0,nvar-1
		if v.eq.0 then tres@gsnCenterString = "CP binned by LT (TPW increasing)" end if
		if v.eq.1 then tres@gsnCenterString = "CP binned by LT (TPW decreasing)" end if
		if v.eq.2 then tres@gsnCenterString = "CP binned by LT (TPW inc. in transition zone)" end if
		if v.eq.3 then tres@gsnCenterString = "CP binned by LT (TPW dec. in transition zone)" end if
		plot(v) = gsn_csm_contour(wks,binval(v,:,:),tres)
		overlay(plot(v) , gsn_csm_xy2(wks,xbin,xbin*0.,bincnt(v,:),lres,lres2) )
		overlay(plot(v) , gsn_csm_xy(wks,(/0,0/),(/-1e5,1e5/),lres) )
	end do
;====================================================================================================
; Finalize Figure
;====================================================================================================
		pres = True
		pres@gsnFrame                           = False

	layout = (/dimsizes(plot),1/)
	;layout = (/(dimsizes(plot)+1)/2,2/)
	
	gsn_panel(wks,plot,layout,pres)

	legend = create "Legend" legendClass wks
		"lgAutoManage"              : False
		"vpXF"                      : 0.5
		"vpYF"                      : 0.48
		"vpWidthF"                  : 0.1
		"vpHeightF"                 : 0.05
		"lgPerimOn"                 : True
		"lgLabelsOn"                : True
		"lgLineLabelsOn"            : False
		"lgItemCount"               : 2
		"lgLabelStrings"            : (/"ECMWF","Soundings"/)
		"lgLabelFontHeightF"        : 0.01
		"lgDashIndex"             	: 0
		"lgLineThicknessF"			: 4.
		"lgLineColors"              : clr
		"lgMonoDashIndex"			: True
	end create

	;draw(legend)
	frame(wks)
  
	print("")
	print(" "+fig_file+"."+fig_type)
	print("")
	if fig_type.eq."png" then system("convert -trim "+fig_file+"."+fig_type+"   "+fig_file+"."+fig_type) end if

;====================================================================================================
;====================================================================================================
end
