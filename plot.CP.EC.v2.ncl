; Similar to v1, plots a timeseries of TPW, but uses anomalies 
; instead of the total TPW since mean values are different between
; ECMWF and DYNAMO NSA. Also, the Lagrangian Tendency of the 
; ECMWF water budget is overlayed using the right Y-Axis. 
; A smoothed version of the Lagrangian Tendency is also overlayed.
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/contrib/time_axis_labels.ncl"
load "$NCARG_ROOT/custom_functions_DYNAMO_LSF.ncl"
load "$NCARG_ROOT/custom_functions_DYNAMO_ECMWF.ncl"
load "$NCARG_ROOT/custom_functions.ncl"
begin

	fig_type = "png"
	fig_file = "~/Research/DYNAMO/CP/CP.EC.v2"

	sa = "lsan"
	
	daily = True
	
	nsmooth = 3

;====================================================================================================
;====================================================================================================
	sa = "lsan"
	
	if sa.eq. "lsan" then
		lat1 =   0.
		lat2 =   7.
		lon1 =  72.
		lon2 =  79.
	end if
	if sa.eq. "lsas" then
		lat1 =  -7.
		lat2 =   0.
		lon1 =  72.
		lon2 =  79.
	end if
	
	ypts = (/lat1,lat2,lat2,lat1/)
	xpts = (/lon1,lon1,lon2,lon2/)

	wks = gsn_open_wks(fig_type,fig_file)
	gsn_define_colormap(wks,"WhiteBlueGreenYellowRed")
	;gsn_define_colormap(wks,"ncl_default")
	plot = new(1,graphic)
		res = True
		res@gsnDraw                         = False
		res@gsnFrame                        = False
		res@vpHeightF						= 0.4
		res@tmXTOn                          = False
		res@gsnLeftStringFontHeightF        = 0.015
		res@gsnCenterStringFontHeightF    	= 0.015
		res@gsnRightStringFontHeightF       = 0.015
		res@tmXBLabelFontHeightF            = 0.01
		res@tmYLLabelFontHeightF            = 0.01
		res@tiXAxisFontHeightF				= 0.015
		res@tiYAxisFontHeightF				= 0.015
		res@tmXBMajorOutwardLengthF         = 0.0
		res@tmXBMinorOutwardLengthF         = 0.0
		res@tmYLMajorOutwardLengthF         = 0.0
		res@tmYLMinorOutwardLengthF         = 0.0
		res@gsnLeftString                   = ""
		res@gsnCenterString              	= ""
		res@gsnRightString                  = ""
		res@tmXBLabelAngleF					= -45.
		
		lres = res
		lres@xyDashPattern              = 0
		lres@xyLineThicknessF           = 1.
		lres@xyLineColor                = "black"
;====================================================================================================
; Load Sounding Array Data
;====================================================================================================
	ps  = LoadFieldsSfc("p"     ,sa)
	p   = LoadFields("p"        ,sa)
	wmr = LoadFields("wmr"      ,sa)/1000.
	q = wmr/(1.+wmr)
	dp = calc_dp(p,ps) *100.
	
	ohq = LoadLSF("hq"  ,sa)
	;ovq = LoadLSF("vq" ,sa)

	oqdim   = dimsizes(q)
	doqdt   = new(oqdim,float)
	doTPWdt = new(oqdim(0),float)
	dt = 6*3600.
	doqdt(1:oqdim(0)-2,:) = ( q(2:oqdim(0)-1,:) - q(0:oqdim(0)-3,:) ) / dt
	;doTPWdt = dim_sum_n(doqdt*dp/g,1)

	toTPW = dim_sum_n( q*dp/g,1) /1000. *1000.  ; divide by density and convert to mm
	;toLT  = doTPWdt + ohq
	toLT  = dim_sum_n(doqdt*dp/g,1) + dim_sum_n(ohq*dp/g,1)

	
	if      daily then 
		oTPW = avg4to1( toTPW )
		oLT  = avg4to1( toLT  )
	end if
	if .not.daily then 
		oTPW = toTPW
		oLT  = toLT
	end if

	not = dimsizes(oTPW)
	otime = ispan(0,not-1,1)
;====================================================================================================
; Load ECMWF Data
;====================================================================================================
	;ifile = "~/Research/DYNAMO/ECMWF/data/EC.budget.LQ.nc"
	ifile = "~/Data/DYNAMO/ECMWF/data/EC.budget.LQ.nc"
	infile = addfile(ifile,"r")

	if      daily then t2 = not*4-1 end if
	if .not.daily then t2 = not-1   end if
	;iQ       = infile->LQvi  (:t2,{lat1:lat2},{lon1:lon2})
	;iDLQDT   = infile->DLQDT (:t2,{lat1:lat2},{lon1:lon2})
	;iudQdx   = infile->udQdx (:t2,{lat1:lat2},{lon1:lon2})
	;ivdQdy   = infile->vdQdy (:t2,{lat1:lat2},{lon1:lon2})
	;iLHFLX   = infile->LHFLX (:t2,{lat1:lat2},{lon1:lon2})
	
	iQ       = infile->LQvi  (:t2,{ypts},{xpts})
	iDLQDT   = infile->DLQDT (:t2,{ypts},{xpts})
	iudQdx   = infile->udQdx (:t2,{ypts},{xpts})
	ivdQdy   = infile->vdQdy (:t2,{ypts},{xpts})
	iLHFLX   = infile->LHFLX (:t2,{ypts},{xpts})

	iTPW = iQ
	iTPW = (/iTPW/Lv/)
	iTPW@long_name = "TPW [mm]"

	iLT = ( iDLQDT + iudQdx + ivdQdy )/Lv
	copy_VarCoords(iQ,iLT)
	
	iADV = ( -1.*iudQdx + -1.*ivdQdy )/Lv
	
	iDDT = ( iDLQDT )/Lv

	tECLT  = dim_avg_n_Wrap(iLT,(/1,2/)) 
	tECTPW = dim_avg_n_Wrap(iTPW,(/1,2/))
	tECADV = dim_avg_n_Wrap(iADV,(/1,2/))
	tECDDT = dim_avg_n_Wrap(iDDT,(/1,2/))
	tECLHF = new(dimsizes(tECTPW),float)
	tECLHF = dim_avg_n(iLHFLX,(/1,2/))
	
	if daily then 
		ECTPW = block_avg( tECTPW ,4)
		ECLT  = block_avg( tECLT  ,4)
		ECDDT = block_avg( tECDDT ,4)
		ECADV = block_avg( tECADV ,4)
		ECLHF = (tECLHF(0::4)+tECLHF(2::4))/(2.*Lv)
	end if
	if .not.daily then 
		print("!!!!!")
		exit
	end if
	
	ECTPW = dim_rmvmean_Wrap(ECTPW)
	ECDDT = (/ECDDT *86400./)
	ECLT  = (/ECLT  *86400./)
	ECADV = (/ECADV *86400./)
	ECLHF = (/ECLHF *86400./)

	nt = dimsizes(ECTPW)
	time = ispan(0,nt-1,1)
;====================================================================================================
; Plot Map
;====================================================================================================
	if daily then
		time@units = "days since 2011-10-01"
	else
		time = time*6
		time@units = "hours since 2011-10-01"
	end if
	cdtime = cd_calendar(time,2)
	tmres = True
	tmres@ttmFormat 		= "%D%c"
	tmres@ttmAxis   		= "XB"
	tmres@ttmMinorStride	= 1
	tmres@ttmMajorStride	= 5
	time_axis_labels(time,res,tmres)

	do n = 0,nsmooth-1 ECLT = wgt_runave_Wrap(ECLT , (/0.25,0.5,0.25/), 0) end do
	do n = 0,nsmooth-1 ECDDT = wgt_runave_Wrap(ECDDT, (/0.25,0.5,0.25/), 0) end do
	do n = 0,nsmooth-1 ECADV = wgt_runave_Wrap(ECADV, (/0.25,0.5,0.25/), 0) end do
		
		res@xyLineThicknessF 	= 4.
		res@xyDashPattern    	= 0     
		tres1 = res
		tres1@gsnCenterString   = "ECMWF Analysis (NSA corners)"
		tres1@trYMinF         	= -14.
		tres1@trYMaxF        	=  14.
		tres1@xyLineColor   	= "black"
		tres1@tiYAxisString   	= "TPW Anomaly [mm]"
		tres2 = res
		tres2@trYMinF     		= -8.
		tres2@trYMaxF     		=  8.
		tres2@xyLineColors  	= (/"red","green","blue","purple"/)
		tres2@tiYAxisString  	= "TPW Tendency [mm/day]"
		
	plot(0) = gsn_csm_xy2(wks,time,ECTPW,(/ECLT,ECDDT,ECADV,ECLHF/),tres1,tres2)

	overlay(plot(0) , gsn_csm_xy(wks,(/-1e3,1e3/),(/0.,0./),lres))

;====================================================================================================
; Finalize Figure
;====================================================================================================
		pres = True
		pres@gsnFrame                           = False
		pres@txString                           = ""
		;pres@gsnPanelBottom                    = 0.1 
		;pres@amJust                            = "TopLeft"
		;pres@gsnPanelFigureStringsFontHeightF  = 0.015
		;pres@gsnPanelFigureStrings             = (/"a","b","c","d"/) 

	gsn_panel(wks,plot,(/dimsizes(plot),1/),pres)

	legend = create "Legend" legendClass wks
		"lgAutoManage"              : False
		"vpXF"                      : 0.4
		"vpYF"                      : 0.3
		"vpWidthF"                  : 0.2
		"vpHeightF"                 : 0.12
		"lgPerimOn"                 : True
		"lgLabelsOn"                : True
		"lgLineLabelsOn"            : False
		"lgItemCount"               : 4
		;"lgLabelStrings"            : (/"ECMWF TPW","NSA TPW","ECMWF Lagrangian Tend.","LHFLX"/)
		"lgLabelStrings"            : (/"ECMWF TPW","NSA TPW","ECMWF Lagrangian Tend.","NSA Lagrangian Tend."/)
		"lgLabelFontHeightF"        : 0.014
		"lgDashIndexes"             : (/0,1,0,1/)
		"lgLineThicknesses"         : (/4.,4.,4.,4./)
		"lgLineColors"              : (/"black","black","red","red"/)
		"lgMonoLineThickness"       : False
	end create

	;draw(legend)
	frame(wks)
  
	print("")
	print(" "+fig_file+"."+fig_type)
	print("")
	if fig_type.eq."png" then system("convert -trim "+fig_file+"."+fig_type+"   "+fig_file+"."+fig_type) end if

;====================================================================================================
;====================================================================================================
end
