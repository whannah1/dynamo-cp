; v3 is an attempt to characterize how well the steering flow 
; method of estimating column qv advection matches observations 
; in both mean and variability, with a scatter plot
; might also be good to make marker size depend on correlation????
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/contrib/time_axis_labels.ncl"
;load "$NCARG_ROOT/custom_functions_DYNAMO_ECMWF.ncl"
load "$NCARG_ROOT/custom_functions.ncl"
begin
    
    fig_type = "png"
    fig_file = "~/Research/DYNAMO/CP/ADV.profile.v3"
    
    verbose = False
    
;====================================================================================================
;====================================================================================================
    if False then
        lat1 =  -10.
        lat2 =   10.
        lon1 =   60.
        lon2 =   90.
    else
        lat1 =   0.
        lat2 =   7.
        lon1 =  72.
        lon2 =  79.
    end if
    
    lev1 =  100
    lev2 = 1000

    tlev1 =  500
    tlev2 = 1000

    mlev = (/1,1/)*800

    ;mlev1 =  (/800,1000/)
    ;mlev2 =  (/800, 500/)
    
;====================================================================================================
; Load ECMWF
;====================================================================================================
    idir = "~/Data/DYNAMO/ECMWF/data/"
    
    ufile = addfile(idir+"6hour.DYNAMO.ECMWF.U.nc","r")
    vfile = addfile(idir+"6hour.DYNAMO.ECMWF.V.nc","r")
    qfile = addfile(idir+"6hour.DYNAMO.ECMWF.Q.nc","r")
    lat = ufile->lat({lat1:lat2})
    lon = ufile->lon({lon1:lon2})

    ;---------------------------------------------------------------
    ;---------------------------------------------------------------
    printline()
    print("Loading data...")
    
    t1 = 0 
    t2 = 31+30+31
    
    t1 = t1*4
    t2 = t2*4-1

    if verbose then printMM(lat) end if
    if verbose then printMM(lon) end if

    u = ufile->U(t1:t2,{lev1:lev2},{lat1:lat2},{lon1:lon2})
    v = vfile->V(t1:t2,{lev1:lev2},{lat1:lat2},{lon1:lon2})
    q = qfile->Q(t1:t2,{lev1:lev2},{lat1:lat2},{lon1:lon2})

    q = (/ q * 1e3/)

    infile = addfile(idir+"6hour.DYNAMO.ECMWF.CWV.nc","r")
    CWV = infile->CWV(t1:t2,{lat1:lat2},{lon1:lon2})

    lev = q&lev
    num_lev = dimsizes(lev)
    ;---------------------------------------------------------------
    ; Load Ps and calculate dP
    ;---------------------------------------------------------------
    infile = addfile(idir+"6hour.DYNAMO.ECMWF.Ps.nc","r")
    ps = infile->Ps(t1:t2,{lat1:lat2},{lon1:lon2})  
    lev = q&lev
    lev!0 = "lev"
    lev&lev = lev
    p   = lev*100.
    p@units = "Pa"
    p!0 = "lev"
    p&lev = p
    dp = calc_dP(p,ps)
    dp!1 = "lev"
    dp&lev = lev
    coldp = dim_sum_n_Wrap(dp/g,1)
    ;---------------------------------------------------------------
    ;---------------------------------------------------------------
    print("Calculating vertical integrals...")
    
    tu  = u (:,{tlev1:tlev2},:,:)
    tv  = v (:,{tlev1:tlev2},:,:)
    ;tq  = q (:,{tlev1:tlev2},:,:)
    tdp = dp(:,{tlev1:tlev2},:,:)

    tcoldp = dim_sum_n_Wrap(tdp/g,1)

    dqdx = calc_ddx(q)
    dqdy = calc_ddy(q)
    
    ub = dim_sum_n(tu*tdp/g,1)    / tcoldp
    vb = dim_sum_n(tv*tdp/g,1)    / tcoldp
    ;dqdxb = dim_sum_n(dqdx*dp/g,1) / coldp
    ;dqdyb = dim_sum_n(dqdy*dp/g,1) / coldp

    CWV = CWV/coldp *1e3

        printline()
        printMAM(q)
        printline()
        printMAM(CWV)
        printline()

    dqdxb = calc_ddx(CWV)
    dqdyb = calc_ddy(CWV)
    
    delq = abs( dqdx + dqdx ) 
    copy_VarCoords(q,delq)
    avgdelq = dim_avg_n_Wrap( dim_avg_n_Wrap( delq ,(/2,3/)) ,(/0/))  
    
    stddelq = dim_stddev_n_Wrap( dim_stddev_n_Wrap( delq ,(/2,3/)) ,(/0/))  
    
    ; Scale the units
    const = 1e3
    avgdelq = (/ avgdelq*const /)
    stddelq = (/ stddelq*const /)
    ;---------------------------------------------------------------
    ;---------------------------------------------------------------
    print("Calculating advection...")
    ADV  =  - u *dqdx  - v *dqdy 
    copy_VarCoords(q,ADV)
    
    ;tADV = ADV(:,{tlev1:tlev2},:,:)
    um   = dim_sum_n( u(:,{mlev},:,:)*dp(:,{mlev},:,:)/g,1) / dim_sum_n(dp(:,{mlev},:,:)/g,1)
    vm   = dim_sum_n( v(:,{mlev},:,:)*dp(:,{mlev},:,:)/g,1) / dim_sum_n(dp(:,{mlev},:,:)/g,1)

    ADVc = dim_sum_n_Wrap(ADV *dp/g,(1)) / coldp
    ADVm = - um*dqdxb - vm*dqdyb
    ADVb = - ub*dqdxb - vb*dqdyb

    
    CORbar = escorc(ndtooned(ADVc),ndtooned(ADVb))
    CORmod = escorc(ndtooned(ADVc),ndtooned(ADVm))

    CORcol = CORbar*0. + 1.

    STDbar = stddev( ADVb )*86400.
    STDcol = stddev( ADVc )*86400.
    STDmod = stddev( ADVm )*86400.

    ;ADVb = dim_avg_n_Wrap( - ub*dqdxb - vb*dqdyb ,(/1,2/))
    ;ADVm = dim_avg_n_Wrap( - um*dqdxb - vm*dqdyb ,(/1,2/))
    ADVb := dim_avg_n_Wrap( ADVb ,(/1,2/))
    ADVm := dim_avg_n_Wrap( ADVm ,(/1,2/))

    ; time average profile
    ADVavg = dim_avg_n_Wrap( dim_avg_n_Wrap( ADV ,(/2,3/)) ,(/0/))  
    ADVbar = avg( ADVb )*86400.
    ADVcol = avg( ADVc )*86400.
    ADVmod = avg( ADVm )*86400.

    ADVavg = (/ ADVavg*86400. /)
    ;ADVbar = (/ ADVbar*86400. /)
    ;ADVcol = (/ ADVcol*86400. /)
    ;ADVmod = (/ ADVmod*86400. /)

    ADVp_b = ADVcol - ADVbar
    ADVp_m = ADVcol - ADVmod

    ;---------------------------------------------------------------
    ;---------------------------------------------------------------
    printline()
    printMAM(ADVavg)
    printMAM(ADVbar)
    printMAM(ADVcol)
    printMAM(ADVmod)
    printline()
    ;---------------------------------------------------------------
    ;---------------------------------------------------------------    
    num_time = dimsizes(q(:,0,0,0))
    num_lat  = dimsizes(q&lat)
    num_lon  = dimsizes(q&lon)
    npool = num_lat*num_lon*num_time
;====================================================================================================
; Create Plot
;====================================================================================================\
    wks = gsn_open_wks(fig_type,fig_file)
    gsn_define_colormap(wks,"default")
    plot = new(2,graphic)
        res = setres_default()
        res@gsnLeftString           = ""
        res@gsnCenterString         = ""
        res@gsnRightString          = ""
        
        lres = res
        lres@xyDashPattern          = 0
        lres@xyLineThicknessF       = 2.
        lres@xyLineColor            = "black"
        
        ;res@xyLineColors            = (/"black","red","blue"/)
        res@trYReverse              = True
        res@xyLineThicknessF        = 8.

        res@tiYAxisString           = "Pressure [hPa]"
        res@tiXAxisString           = "Magnitude of ~F34~Q~F21~q~B~v~N~ [g kg~S~-1~N~ km~S~-1~N~]"
    
        tres = res
        tres@xyLineColors = (/1,-1,-1/)
    plot(1) = gsn_csm_xy(wks,(/avgdelq,avgdelq+stddelq,avgdelq-stddelq/),lev,tres)

    xp = new(2*num_lev,float)
    yp = new(2*num_lev,float)
    xp(num_lev*0:num_lev*1-1: 1) = avgdelq-stddelq
    xp(num_lev*1:num_lev*2-1:-1) = avgdelq+stddelq
    yp(num_lev*0:num_lev*1-1: 1) = lev
    yp(num_lev*1:num_lev*2-1:-1) = lev
   
        gsres                   = True 
        gsres@tfPolyDrawOrder   = "Predraw"
        gsres@gsFillColor       = "lightgray"
    dummy = gsn_add_polygon (wks,plot(1),xp,yp,gsres)
            
    ;        res@tiYAxisString           = "Pressure [hPa]"
    ;        res@tiXAxisString           = "Advective Tendency [mm day~S~-1~N~]"
    ;        res@xyDashPattern           = 2
    ;        res@trXMinF     = min((/min(ADVavg),ADVbar,ADVcol,ADVmod/)) - 0.2
    ;        res@trXMaxF     = max((/max(ADVavg),ADVbar,ADVcol,ADVmod/)) + 0.2
    ;
    ;    plot(0) = gsn_csm_xy(wks,ADVavg,lev,res)
    ;end if


    mcolor = (/"black","blue","red"/)

        res@tiYAxisString       = "Mean Column Advection [mm day~S~-1~N~]"
        res@tiXAxisString       = "Correlation with Obs"
        res@xyMarkLineMode      = "Markers"
        res@xyMarker            = 16
        res@xyMarkerColors      = mcolor
        ;res@xyMarkerSizeF       = 0.02
        res@xyMarkerSizes       = 0.04 * (/STDcol,STDbar,STDmod/) / STDcol

    printline()
    print("std dev:")
    print( (/STDcol,STDbar,STDmod/) )
    printline()

    my = (/ADVcol,ADVbar,ADVmod/)
    mx = (/CORcol,CORbar,CORmod/)

    mm = new((/dimsizes(mx),2/),float)
    my := conform(mm,my,0)
    mx := conform(mm,mx,0)

        res@trYReverse = False
        res@trXMinF = min(mx) - stddev(mx)
        res@trXMaxF = max(mx) + stddev(mx)
        res@trYMinF = min(my) - stddev(my)
        res@trYMaxF = max(my) + stddev(my)
        
        res@trXMinF = 0.5
        res@trXMaxF = 1.1

    plot(0) = gsn_csm_xy(wks,mx,my,res)

    ;---------------------------------------------------------------
    ;---------------------------------------------------------------
    res@xyDashPattern   = 2

    res@xyLineColor     = "blue"
    ;overlay(plot(0) , gsn_csm_xy(wks,(/1,1/)*ADVp_b,(/-1e4,1e4/),res))

    res@xyLineColor     = "red"
    ;overlay(plot(0) , gsn_csm_xy(wks,(/1,1/)*ADVp_m,(/-1e4,1e4/),res))
    ;---------------------------------------------------------------
    ;---------------------------------------------------------------
    do x = 0,dimsizes(plot)-1
        if .not.ismissing(plot(x)) then 
            overlay(plot(x) , gsn_csm_xy(wks,(/-1e3,1e3/),(/0.,0./),lres)) 
            overlay(plot(x) , gsn_csm_xy(wks,(/0.,0./),(/-1e3,1e3/),lres)) 
        end if
    end do
;====================================================================================================
; Finalize Figure
;====================================================================================================
        pres = True
        pres@amJust                            = "TopLeft"
        pres@gsnPanelFigureStringsFontHeightF  = 0.015
        pres@gsnPanelFigureStrings             = (/"a","b","c","d"/) 

    ;gsn_panel(wks,plot,(/dimsizes(plot),1/),pres)
    gsn_panel(wks,plot,(/1,dimsizes(plot)/),pres)
  
    trimPNG(fig_file)
;====================================================================================================
;====================================================================================================
end
