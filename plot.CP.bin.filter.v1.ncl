; Plot the Lagrangian tendency of TPW binned by TPW
; including lo-pass and hi-pass filtered data
; v1 - ECMWF only
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/custom_functions_DYNAMO_LSF.ncl"
load "$NCARG_ROOT/custom_functions_DYNAMO_ECMWF.ncl"
load "$NCARG_ROOT/custom_functions.ncl"
begin

	fig_type = "png"
	fig_file = "~/Research/DYNAMO/CP/CP.bin.filter.v1"
	
	nday  = 10
	nWgt  = nday*4*2+1
	fca   = 1./(tofloat(nday)*4.)	
	
	reduceRes	= False
	dailyAvg	= False
	recalc		= False
	debug 		= False
;====================================================================================================
;====================================================================================================		
	nvar = 3
	bins = True
	bins@verbose = False
    bins@bin_min = 20
    bins@bin_max = 64
    bins@bin_spc =  2
    
    ;bins@bin_min = -20
    ;bins@bin_max =  20
    ;bins@bin_spc =   1

	xbin 	= ispan(bins@bin_min,bins@bin_max,bins@bin_spc)
    num_bin = dimsizes(xbin)
    bdim 	= (/nvar,num_bin/)
    binval = new(bdim,float)
    bincnt = new(bdim,float)
    bincnt = 0

	sa = "lsan"
	
	if sa.eq. "lsan" then
		lat1 =   0.
		lat2 =   7.
		lon1 =  72.
		lon2 =  79.
	end if
	if sa.eq. "lsas" then
		lat1 =  -7.
		lat2 =   0.
		lon1 =  72.
		lon2 =  79.
	end if
	
	lat1 = -10.
	lat2 =  10.
	lon1 =  50.
	lon2 = 100.
    
	wks = gsn_open_wks(fig_type,fig_file)
	plot = new(1,graphic)
		res = True
		res@gsnDraw                         = False
		res@gsnFrame                        = False
		res@vpHeightF						= 0.5
		res@tmXTOn                          = False
		res@tmXBMinorOn						= False
		res@tmYLMinorOn						= False
		res@tmYRMinorOn						= False
		res@gsnLeftStringFontHeightF        = 0.01
		res@gsnCenterStringFontHeightF    	= 0.01
		res@gsnRightStringFontHeightF       = 0.01
		res@tmXBLabelFontHeightF            = 0.01
		res@tmYLLabelFontHeightF            = 0.01
		res@tiXAxisFontHeightF				= 0.01
		res@tiYAxisFontHeightF				= 0.01
		res@tmXBMajorOutwardLengthF         = 0.0
		res@tmXBMinorOutwardLengthF         = 0.0
		res@tmYLMajorOutwardLengthF         = 0.0
		res@tmYLMinorOutwardLengthF         = 0.0
		res@gsnLeftString                   = ""
		res@gsnCenterString              	= ""
		res@gsnRightString                  = ""
		;res@tmXBLabelAngleF					= -50.
		
		lres = res
		lres@xyDashPattern              = 0
		lres@xyLineThicknessF           = 4.
		lres@xyLineColor                = "black"
;====================================================================================================
; Load ECMWF Data
;====================================================================================================	
    if reduceRes then
        ifile = "~/Data/DYNAMO/ECMWF/data/6hour.DYNAMO.ECMWF.CP.SAres.nc"
    else
        ifile = "~/Data/DYNAMO/ECMWF/data/6hour.DYNAMO.ECMWF.CP.nc"
    end if
    infile = addfile(ifile,"r")

    ECTPW = infile->ECTPW(:,{lat1:lat2},{lon1:lon2})
    ECCPR = infile->ECCPR(:,{lat1:lat2},{lon1:lon2}) 
	
	printline()
	printVarSummary(ECCPR)
	printline()
;====================================================================================================
; Create Plot 
;====================================================================================================
    do v = 0,nvar-1
        print("    v = "+v+"    v%nvar = "+v%nvar)
		
		sigma = 1.0	
		;nday  = 10
		;nWgt  = nday*2+1
		;fca   = 1./(tofloat(nday)*4.)	
		fcb   = -999.
		
		if v.eq.1 then wgt = filwgts_lanczos(nWgt, 0,fca,fcb,sigma) end if
		if v.eq.2 then wgt = filwgts_lanczos(nWgt, 1,fca,fcb,sigma) end if
		        
	    if v.eq.0 then Vy = ECCPR  end if
	    if v.gt.0 then Vy = wgt_runave_n(ECCPR, wgt, 0, 0)  end if
	    
		Vx = ECTPW
		;Vx = dim_rmvmean_n(ECTPW,0)
		;wgt = filwgts_lanczos(nWgt, 1,fca,fcb,sigma)
		;Vx  = wgt_runave_n(ECTPW, wgt, 0, 0)
		
		; Daily Average
		if dailyAvg then
			tVx = Vx
			tVy = Vy
			delete([/Vx,Vy/])
			Vx = block_avg(tVx,4)
			Vy = block_avg(tVy,4)
			delete([/tVx,tVy/])
		end if
        
		tmp = bin_YbyX(Vy,Vx,bins)
        binval(v,:) = tmp
        bincnt(v,:) = tmp@pct
        delete([/Vy,Vx,tmp/])
        
        if v.ne.0 then bincnt(v,:) = bincnt(0,:) end if
    end do
    
    print(bincnt(0,:))
    printline()
;====================================================================================================
;====================================================================================================
		clr = (/"black","red","blue"/)
		tres = res
		tres@xyLineThicknessF 	= 8.
		;tres@gsnCenterString   	= "ECMWF Lagrangian Tendency Binned by TPW" ;+" ()"
		tres@tiXAxisString   	= "Anomalous TPW [mm]"
		tres@tiYAxisString   	= "[mm day~S~-1~N~]"
		tres@xyLineColors		= clr
		;tres@xyLineColor		= "blue"
		
        tres@trXMinF    = bins@bin_min
        tres@trXMaxF    = bins@bin_max

		dres = tres
		dres@xyMonoLineColor	= True
		dres@xyLineColor		= "black"
		dres@gsnCenterString	= ""
		dres@tiYAxisString   	= "[% of Obsrevations]"
		dres@xyDashPattern 		= 2
		dres@xyLineThicknessF	= 4.
		
		tres@xyDashPatterns    	= (/0,0,2,2,3,3/)*0
		tres@trYMinF    = -2.
        tres@trYMaxF    =  7.
		
	plot(0) = gsn_csm_xy2(wks,xbin,binval,bincnt,tres,dres)
	
	overlay(plot(0) , gsn_csm_xy(wks,(/-1e3,1e3/),(/0.,0./),lres))
	;overlay(plot(0) , gsn_csm_xy(wks,(/1.,1./)*avg(ECTPW),(/-1e3,1e3/),lres))
	
;====================================================================================================
; Finalize Figure
;====================================================================================================
		pres = True
		pres@gsnFrame                           = False

	gsn_panel(wks,plot,(/dimsizes(plot),1/),pres)

	legend = create "Legend" legendClass wks
		"lgAutoManage"              : False
		"vpXF"                      : 0.2
		"vpYF"                      : 0.7
		"vpWidthF"                  : 0.1
		"vpHeightF"                 : 0.08
		"lgPerimOn"                 : True
		"lgLabelsOn"                : True
		"lgLineLabelsOn"            : False
		"lgItemCount"               : 3
		"lgLabelStrings"            : (/"LCT             ",\
                                        "LCT (low freq.) ",\
                                        "LCT (high freq.)"/)
		"lgLabelFontHeightF"        : 0.015
		"lgDashIndex"             	: 0
		"lgLineThicknessF"			: 8.
		"lgLineColors"              : clr
		"lgMonoDashIndex"			: True
	end create

	draw(legend)
	frame(wks)
  
	print("")
	print(" "+fig_file+"."+fig_type)
	print("")
	if fig_type.eq."png" then system("convert -trim "+fig_file+"."+fig_type+"   "+fig_file+"."+fig_type) end if

;====================================================================================================
;====================================================================================================
end
