; Plot the Lagrangian tendency of TPW binned by TPW
; including lo-pass and hi-pass filtered data
; v4 - subtract out surface fluxes
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/custom_functions_DYNAMO_LSF.ncl"
load "$NCARG_ROOT/custom_functions_DYNAMO_ECMWF.ncl"
load "$NCARG_ROOT/custom_functions.ncl"
begin

	fig_type = "png"
	fig_file = "~/Research/DYNAMO/CP/CP.bin.filter.v4"
	
	nday  = 10
	nWgt  = nday*4*2+1
	fca   = 1./(tofloat(nday)*4.)   
	
	reduceRes   = True
	dailyAvg    = False
	recalc      = True
	debug       = False
;====================================================================================================
;====================================================================================================       
	nvar = 3
	bins = True
	bins@verbose = False
	bins@bin_min = 20
	bins@bin_max = 60
	bins@bin_spc =  2
	
	;bins@bin_min = -20
	;bins@bin_max =  20
	;bins@bin_spc =   1

	xbin    = ispan(bins@bin_min,bins@bin_max,bins@bin_spc)
	num_bin = dimsizes(xbin)
	bdim    = (/nvar,num_bin/)
	binval = new(bdim,float)
	bincnt = new(bdim,float)
	bincnt = 0

	sa = "lsan"
	
	if sa.eq. "lsan" then
		lat1 =   0.
		lat2 =   7.
		lon1 =  72.
		lon2 =  79.
	end if
	if sa.eq. "lsas" then
		lat1 =  -7.
		lat2 =   0.
		lon1 =  72.
		lon2 =  79.
	end if
	
	;lat1 = -15.
	;lat2 =  15.
	;lon1 =  60.
	;lon2 = 150.
	
	wks = gsn_open_wks(fig_type,fig_file)
	plot = new(1,graphic)
		res = True
		res@gsnDraw                         = False
		res@gsnFrame                        = False
		res@vpHeightF                       = 0.5
		res@tmXTOn                          = False
		res@tmXBMinorOn                     = False
		res@tmYLMinorOn                     = False
		res@tmYRMinorOn                     = False
		res@gsnLeftStringFontHeightF        = 0.01
		res@gsnCenterStringFontHeightF      = 0.01
		res@gsnRightStringFontHeightF       = 0.01
		res@tmXBLabelFontHeightF            = 0.01
		res@tmYLLabelFontHeightF            = 0.01
		res@tiXAxisFontHeightF              = 0.01
		res@tiYAxisFontHeightF              = 0.01
		res@tmXBMajorOutwardLengthF         = 0.0
		res@tmXBMinorOutwardLengthF         = 0.0
		res@tmYLMajorOutwardLengthF         = 0.0
		res@tmYLMinorOutwardLengthF         = 0.0
		res@gsnLeftString                   = ""
		res@gsnCenterString                 = ""
		res@gsnRightString                  = ""
		;res@tmXBLabelAngleF                = -50.
		
		lres = res
		lres@xyDashPattern              = 0
		lres@xyLineThicknessF           = 1.
		lres@xyLineColor                = "black"
;====================================================================================================
; Load Sounding Array Data
;====================================================================================================
if True then
	print("Loading LSA...")
	ver = "2b"
	if ver.eq."2a" then ifile = "~/Data/DYNAMO/LSA/data/v2a/dynamo_basic_v2a_2011all.nc" end if
	if ver.eq."2b" then ifile = "~/Data/DYNAMO/LSA/data/v2b/dynamo_basic_v2b_2011all.nc" end if
	infile = addfile(ifile,"r")
	SAlat = infile->lat({lat1:lat2})
	SAlon = infile->lon({lon1:lon2})
	ps  = infile->ps(:,{lat1:lat2},{lon1:lon2})
	p   = infile->level
	p!0 = "lev"
	p&lev = p
	idp = calc_dP(p,ps) *100.
	iu  = infile->u(:,:,{lat1:lat2},{lon1:lon2})
	iv  = infile->v(:,:,{lat1:lat2},{lon1:lon2})
	wmr = infile->q(:,:,{lat1:lat2},{lon1:lon2})
	wmr = (/wmr/1000./)
	iq  = wmr/(1.+wmr)
	copy_VarCoords(wmr,iq)
	nblk = 2
	dp = block_avg(idp,nblk)
	q  = block_avg(iq ,nblk)
	u  = block_avg(iu ,nblk)
	v  = block_avg(iv ,nblk)
	
	dt = 6*3600.
	iSADDT = calc_ddt(q,dt)*86400.
	iSAADV = calc_adv(q,u,v) *-1.*86400.    
	SATPW = dim_sum_n(     q*dp/g,1) 
	SADDT = dim_sum_n(iSADDT*dp/g,1) 
	SAADV = dim_sum_n(iSAADV*dp/g,1) 
	
	SACPR = SADDT-SAADV
	delete([/wmr,q,u,v,p,ps,dp,iSADDT,iSAADV,idp,iq,iu,iv/])

	data = asciiread("~/Data/DYNAMO/LSA/data/v3a/eopo.nsa_3a",(/736,10/),"float")
	SALHF = conform(SACPR, block_avg( data(:,4) ,nblk) ,0)

	SACPR = (/SACPR - SALHF/)
end if
;====================================================================================================
; Load MIMIC Data
;====================================================================================================
if False then
	ifile  = "~/Data/DYNAMO/MIMIC/cimss_morphed_tpw_DYNAMO_2011.nc"
	infile = addfile(ifile,"r")
	tmp = infile->tpw(:,{lat1:lat2},{lon1:lon2}) 
	iMITPW = new(dimsizes(tmp),float)
	iMITPW = tofloat( tmp )
	delete(tmp)
	iMITPW = tofloat( where(isnan_ieee(iMITPW),iMITPW@_FillValue,iMITPW) ) 
	bsz = 6
	MITPW = block_avg( dim_avg_n_Wrap(iMITPW,(/1,2/)) ,bsz)
	printline()
	printVarSummary(MITPW)
end if
;====================================================================================================
; Load ECMWF Data
;====================================================================================================   
	print("Loading ECMWF...")
	tfile = "~/Data/DYNAMO/ECMWF/data/6hour.DYNAMO.ECMWF.CP.nc"
	if recalc then
		t2 = (31+30+31)*4-1
		if debug then t2 = 10*4 end if
		infile = addfile("~/Data/DYNAMO/ECMWF/data/6hour.DYNAMO.ECMWF.U.nc","r")
		u = infile->U(:t2,:,{lat1:lat2},{lon1:lon2})
		infile = addfile("~/Data/DYNAMO/ECMWF/data/6hour.DYNAMO.ECMWF.V.nc","r")
		v = infile->V(:t2,:,{lat1:lat2},{lon1:lon2})
		infile = addfile("~/Data/DYNAMO/ECMWF/data/6hour.DYNAMO.ECMWF.Q.nc","r")
		q = infile->Q(:t2,:,{lat1:lat2},{lon1:lon2})
	
		infile = addfile("~/Data/DYNAMO/ECMWF/data/6hour.DYNAMO.ECMWF.Ps.nc","r")
		ps = infile->Ps(:t2,{lat1:lat2},{lon1:lon2})    
		p = q&lev*100.
		p@units = "Pa"
		p!0 = "lev"
		p&lev = p
		dp = calc_dP(p,ps)

		ifile = "~/Data/DYNAMO/ECMWF/data/6hour.DYNAMO.ECMWF.CWV.nc"
		infile = addfile(ifile,"r")
		ECTPW = infile->CWV(:t2,{lat1:lat2},{lon1:lon2}) 
		

	
		if reduceRes then
			iECTPW = ECTPW
			ilat = q&lat
			ilon = q&lon
			iq = q
			iu = u
			iv = v
			idp = dp
			delete([/u,v,q,dp,ECTPW/])
			q  = area_hi2lores_Wrap(ilon,ilat,iq ,False,1.,SAlon,SAlat,False)
			u  = area_hi2lores_Wrap(ilon,ilat,iu ,False,1.,SAlon,SAlat,False)
			v  = area_hi2lores_Wrap(ilon,ilat,iv ,False,1.,SAlon,SAlat,False)
			dp = area_hi2lores_Wrap(ilon,ilat,idp,False,1.,SAlon,SAlat,False)
			ECTPW = area_hi2lores_Wrap(ilon,ilat,iECTPW ,False,1.,SAlon,SAlat,False)
			delete([/iu,iv,iq,idp,iECTPW/])
		end if
	
		iECADV = calc_adv(q,u,v) *-1.*86400.
		dt  = 6*3600.
		iECDDT = calc_ddt(q,dt)*86400.
	
		ECDDT = dim_sum_n(iECDDT*dp/g,1)
		ECADV = dim_sum_n(iECADV*dp/g,1)
		ECCPR = ECDDT - ECADV

		ECCPR = (/ECCPR - SALHF/)
	
		delete([/q,u,v,iECDDT,iECADV,p,ps,dp/])
	
		;if isfilepresent(tfile) then system("rm "+tfile) end if
		;outfile = addfile(tfile,"c")
		;outfile->ECTPW = ECTPW
		;outfile->ECDDT = ECDDT
		;outfile->ECCPR = ECCPR
		
		;outfile->ECLHF = ECLHF
	else
		;infile = addfile(tfile,"r")
		;ECTPW = infile->ECTPW
		;ECDDT = infile->ECDDT
		;ECCPR = infile->ECCPR
		
		;ECLHF = infile->ECLHF
	end if
	
	printline()
	printVarSummary(ECCPR)
	printline()
;====================================================================================================
; Create Plot 
;====================================================================================================
	do v = 0,nvar-1
		print("    v = "+v+"    v%nvar = "+v%nvar)
		
		sigma = 1.0 
		;nday  = 10
		;nWgt  = nday*2+1
		;fca   = 1./(tofloat(nday)*4.)  
		fcb   = -999.
		
		if v.eq.1 then wgt = filwgts_lanczos(nWgt, 0,fca,fcb,sigma) end if
		if v.eq.2 then wgt = filwgts_lanczos(nWgt, 1,fca,fcb,sigma) end if
				
		if v.eq.0 then Vy = ECCPR  end if
		if v.gt.0 then Vy = wgt_runave_n(ECCPR, wgt, 0, 0)  end if
		
		Vx = ECTPW
		;Vx = dim_rmvmean_n(ECTPW,0)
		;wgt = filwgts_lanczos(nWgt, 1,fca,fcb,sigma)
		;Vx  = wgt_runave_n(ECTPW, wgt, 0, 0)
		
		; Daily Average
		if dailyAvg then
			tVx = Vx
			tVy = Vy
			delete([/Vx,Vy/])
			Vx = block_avg(tVx,4)
			Vy = block_avg(tVy,4)
			delete([/tVx,tVy/])
		end if
		
		tmp = bin_YbyX(Vy,Vx,bins)
		binval(v,:) = tmp
		bincnt(v,:) = tmp@pctcnt
		delete([/Vy,Vx,tmp/])
		
		if v.ne.0 then bincnt(v,:) = bincnt(0,:) end if
	end do
	
	print(bincnt(0,:))
	printline()
;====================================================================================================
;====================================================================================================
		clr = (/"black","red","blue"/)
		tres = res
		tres@xyLineThicknessF   = 4.
		tres@gsnCenterString    = "ECMWF Lagrangian Tendency Binned by TPW" ;+" ()"
		tres@tiXAxisString      = "Anomalous TPW [mm]"
		tres@tiYAxisString      = "[mm day~S~-1~N~]"
		tres@xyLineColors       = clr
		;tres@xyLineColor       = "blue"
		
		dres = tres
		dres@xyMonoLineColor    = True
		dres@xyLineColor        = "black"
		dres@gsnCenterString    = ""
		dres@tiYAxisString      = "[% of Obsrevations]"
		dres@xyDashPattern      = 2
		dres@xyLineThicknessF   = 1.
		
		tres@xyDashPatterns     = (/0,0,2,2,3,3/)*0
		;tres@xyDashPatterns    = (/0,1,2/)
		;tres@trYMinF           = -12.
		;tres@trYMaxF           =  12.
		
	plot(0) = gsn_csm_xy2(wks,xbin,binval,bincnt,tres,dres)
	
	overlay(plot(0) , gsn_csm_xy(wks,(/-1e3,1e3/),(/0.,0./),lres))
	overlay(plot(0) , gsn_csm_xy(wks,(/1.,1./)*avg(ECTPW),(/-1e3,1e3/),lres))
	
;====================================================================================================
; Finalize Figure
;====================================================================================================
		pres = True
		pres@gsnFrame                           = False

	gsn_panel(wks,plot,(/dimsizes(plot),1/),pres)

	legend = create "Legend" legendClass wks
		"lgAutoManage"              : False
		"vpXF"                      : 0.5
		"vpYF"                      : 0.48
		"vpWidthF"                  : 0.1
		"vpHeightF"                 : 0.05
		"lgPerimOn"                 : True
		"lgLabelsOn"                : True
		"lgLineLabelsOn"            : False
		"lgItemCount"               : 2
		"lgLabelStrings"            : (/"ECMWF","Soundings"/)
		"lgLabelFontHeightF"        : 0.01
		"lgDashIndex"               : 0
		"lgLineThicknessF"          : 4.
		"lgLineColors"              : clr
		"lgMonoDashIndex"           : True
	end create

	;draw(legend)
	frame(wks)
  
	print("")
	print(" "+fig_file+"."+fig_type)
	print("")
	if fig_type.eq."png" then system("convert -trim "+fig_file+"."+fig_type+"   "+fig_file+"."+fig_type) end if

;====================================================================================================
;====================================================================================================
end
