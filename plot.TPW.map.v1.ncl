; Timeseries of 6t-hourly data at Gan
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/contrib/time_axis_labels.ncl"
load "$NCARG_ROOT/custom_functions_DYNAMO_LSF.ncl"
load "$NCARG_ROOT/custom_functions_DYNAMO_ECMWF.ncl"
load "$NCARG_ROOT/custom_functions.ncl"
begin

	fig_type = "png"
	fig_file = "~/Research/DYNAMO/CP/TPW.map.v1"

	verbose = False
	
	;nday = 31+24
	nday = 11
	
	month = "Oct"
	
	pday = nday ;-31

;====================================================================================================
;====================================================================================================		
	sa = "lsan"
	
	if sa.eq. "lsan" then
		lat1 =   0.
		lat2 =   7.
		lon1 =  72.
		lon2 =  79.
	end if
	if sa.eq. "lsas" then
		lat1 =  -7.
		lat2 =   0.
		lon1 =  72.
		lon2 =  79.
	end if
	
	lat1 = -10.
	lat2 =  15.
	lon1 =  65.
	lon2 =  85.
		
    line = "----------------------------------------------"	
	
	wks = gsn_open_wks(fig_type,fig_file)
		res = True
		res@gsnDraw                         = False
		res@gsnFrame                        = False
		res@tmXTOn                          = False
		res@tmXBMinorOn						= False
		res@tmYLMinorOn						= False
		res@gsnLeftStringFontHeightF        = 0.02
		res@gsnCenterStringFontHeightF    	= 0.02
		res@gsnRightStringFontHeightF       = 0.02
		res@tmXBLabelFontHeightF            = 0.01
		res@tmYLLabelFontHeightF            = 0.01
		res@tiXAxisFontHeightF				= 0.015
		res@tiYAxisFontHeightF				= 0.015
		res@tmXBMajorOutwardLengthF         = 0.0
		res@tmXBMinorOutwardLengthF         = 0.0
		res@tmYLMajorOutwardLengthF         = 0.0
		res@tmYLMinorOutwardLengthF         = 0.0
		res@gsnLeftString                   = ""
		res@gsnCenterString              	= ""
		res@gsnRightString                  = ""
		res@lbTitlePosition					= "bottom"
		res@lbTitleFontHeightF				= 0.015
		res@lbLabelFontHeightF				= 0.015
		
		vres = True
		vres@gsnDraw                        = False
		vres@gsnFrame                       = False
		vres@vcLineArrowColor		= "black"
		vres@vcLineArrowThicknessF	= 2.
		vres@vcRefMagnitudeF 		= 3.          		; define vector ref mag
		vres@vcRefLengthF    		= 0.04         		; define length of vec ref
		vres@vcGlyphStyle    		= "CurlyVector"		; turn on curley vectors
		vres@vcMinDistanceF  		= 0.03         		; thin out vectors
		vres@gsnLeftString       	= ""
		vres@gsnCenterString     	= ""
		vres@gsnRightString    		= ""
;====================================================================================================
; Load Sounding Array Data
;====================================================================================================
	ver = "2b"
	t = nday*8
	if ver.eq."2a" then ifile = "~/Data/DYNAMO/LSA/data/v2a/dynamo_basic_v2a_2011all.nc" end if
	if ver.eq."2b" then ifile = "~/Data/DYNAMO/LSA/data/v2b/dynamo_basic_v2b_2011all.nc" end if
	infile = addfile(ifile,"r")
	;printVarSummary(infile->time)
	print("SA time: "+ (infile->time(t)/24-273) )
	SAlat = infile->lat({lat1:lat2})
	SAlon = infile->lon({lon1:lon2})
	ps  = infile->ps(t:t+1,{lat1:lat2},{lon1:lon2})
	p   = infile->level
	p!0 = "lev"
	p&lev = p
	idp = calc_dP(p,ps) *100.
	dp = idp(0,:,:,:)
	u   = infile->u(t,:,{lat1:lat2},{lon1:lon2})
	v   = infile->v(t,:,{lat1:lat2},{lon1:lon2})
	wmr = infile->q(t,:,{lat1:lat2},{lon1:lon2}) 
	wmr = (/wmr/1000./)
	q 	= wmr/(1.+wmr)
	copy_VarCoords(wmr,q)


	;SAADV = calc_adv(q,u,v) 
	SATPW = dim_sum_n(q*dp/g,0)
	
	;SATPW = (/SATPW*1e3/)
	;SAADV = (/SAADV*86400.*1e3 *-1./)
	
	SAU = u
	SAV = v
	
	SATPW!0 = "lat"
	SATPW!1 = "lon"
	SATPW&lat = q&lat
	SATPW&lon = q&lon
	
	printline()
	if verbose then printVarSummary(SATPW) end if
	SATPW!0 = "lat"
	SATPW!1 = "lon"
	SATPW&lat = SAlat
	SATPW&lon = SAlon
	delete([/q,u,v,idp,dp,ps,p/])
;====================================================================================================
; Load MIMIC Data
;====================================================================================================
	t = nday*24
	ifile  = "~/Data/DYNAMO/MIMIC/cimss_morphed_tpw_DYNAMO_2011.nc"
	infile = addfile(ifile,"r")
	;MInt   = dimsizes(infile->time)/6
	MIlat = infile->latitude({lat1:lat2})
	MIlon = infile->longitude({lon1:lon2})
	tmp = infile->tpw(t:t+6,{lat1:lat2},{lon1:lon2}) 
	iMITPW = new(dimsizes(tmp),float)
	iMITPW = tofloat( tmp )
	delete(tmp)
	iMITPW = tofloat( where(isnan_ieee(iMITPW),iMITPW@_FillValue,iMITPW) ) 
	MITPW = dim_avg_n_Wrap(iMITPW,0)
	printline()
	MITPW!0 = "lat"
	MITPW!1 = "lon"
	MITPW&lat = MIlat
	MITPW&lon = MIlon
	printVarSummary(MITPW)
;====================================================================================================
; Load ECMWF CWV Data
;====================================================================================================
	t = nday*4
	infile = addfile("~/Data/DYNAMO/ECMWF/data/6hour.DYNAMO.ECMWF.U.nc","r")
	u = infile->U(t,:,{lat1:lat2},{lon1:lon2})
	infile = addfile("~/Data/DYNAMO/ECMWF/data/6hour.DYNAMO.ECMWF.V.nc","r")
	v = infile->V(t,:,{lat1:lat2},{lon1:lon2})
	infile = addfile("~/Data/DYNAMO/ECMWF/data/6hour.DYNAMO.ECMWF.Q.nc","r")
	q = infile->Q(t,:,{lat1:lat2},{lon1:lon2})
	
	infile = addfile("~/Data/DYNAMO/ECMWF/data/6hour.DYNAMO.ECMWF.Ps.nc","r")
	ps = infile->Ps(t:t+1,{lat1:lat2},{lon1:lon2})	
	lev = q&lev 
	p   = lev*100.
	p!0 = "lev"
	p&lev = p
	idp = calc_dP(p,ps)
	dp = idp(0,:,:,:)
	
	;ilat = q&lat
	;ilon = q&lon
	;iq = q
	;iu = u
	;iv = v
	;delete([/u,v,q/])
	;q  = area_hi2lores_Wrap(ilon,ilat,iq ,False,1.,SAlon,SAlat,False)
	;u  = area_hi2lores_Wrap(ilon,ilat,iu ,False,1.,SAlon,SAlat,False)
	;v  = area_hi2lores_Wrap(ilon,ilat,iv ,False,1.,SAlon,SAlat,False)
	;dp = area_hi2lores_Wrap(ilon,ilat,dp ,False,1.,SAlon,SAlat,False)
	;delete([/iu,iv,iq/])
	
	;ECADV = calc_adv(q,u,v)
	ECTPW = dim_sum_n(q*dp/g,0)
	
	;ECTPW = (/ECTPW*1e3/)
	;ECADV = (/ECADV*86400.*1e3 *-1./)
	
	ECU = u
	ECV = v
	
	ECTPW!0 = "lat"
	ECTPW!1 = "lon"
	ECTPW&lat = q&lat
	ECTPW&lon = q&lon
	
	printline()
	if verbose then printVarSummary(ECTPW) end if
	delete([/q,u,v,idp,dp,ps,p/])
;====================================================================================================
; Create Plot 
;====================================================================================================	
	printline()
	clr = (/"blue","green","red"/)
	
	tres = res
	tres@cnFillOn				= True
	tres@cnLinesOn				= False
	;tres@cnFillPalette			= ""
	tres@cnSpanFillPalette		= True
	tres@cnLevelSelectionMode	= "ExplicitLevels"
	tres@gsnAddCyclic			= False
	tres@mpLimitMode			= "LatLon"
	tres@mpMinLatF				= lat1
	tres@mpMaxLatF				= lat2
	tres@mpMinLonF				= lon1
	tres@mpMaxLonF				= lon2
	tres@lbTitleString			= "[mm]"
	tres@gsnRightString 		= "TPW"
	tres@cnLevels				= ispan(24,64,2)
	
	plot = new(3,graphic)	

	tres@gsnLeftString = "LSA version "+ver
	plot(0) = gsn_csm_contour_map(wks,SATPW,tres)
	tres@gsnLeftString = "ECMWF"
	plot(1) = gsn_csm_contour_map(wks,ECTPW,tres)
	
		
	tres@gsnLeftString = "MIMIC"
	plot(2) = gsn_csm_contour_map(wks,MITPW,tres)


	;overlay(plot(0) , gsn_csm_xy(wks,(/-1e3,1e3/),(/0.,0./),lres))
	
	;overlay(plot(0),gsn_csm_vector(wks,SAU,SAV,vres))
	;overlay(plot(2),gsn_csm_vector(wks,SAU,SAV,vres))
	;overlay(plot(1),gsn_csm_vector(wks,ECU,ECV,vres))
	;overlay(plot(3),gsn_csm_vector(wks,ECU,ECV,vres))
	
	num_p = dimsizes(plot)
	;--------------------------------------------------------------------------------------------
	; Add Array lines
	;--------------------------------------------------------------------------------------------
	site_lat  = (/-0.7,		0.,		-7.3,	-7.3 ,   4.1,  6.9,  0./)
	site_lon  = (/73.2,		80.,		80.,		72.5 ,  73.5, 79.8, 80./)
		lres = True
		lres@gsLineColor 		= "green"
		lres@gsLineThicknessF	= 3.
	; NSA+SSA
	array_lat = new(8,float)
	array_lon = new(8,float)
	array_lat(:3) = site_lat(:3)
	array_lon(:3) = site_lon(:3)
	array_lat(4) = site_lat(0)
	array_lon(4) = site_lon(0)
	array_lat(5:) = site_lat(4:)
	array_lon(5:) = site_lon(4:)
	ldum = new(num_p,graphic)
	do p = 0,num_p-1 ldum(p) = gsn_add_polyline(wks,plot(p),array_lon,array_lat,lres) end do
	;--------------------------------------------------------------------------------------------
	; Add Line at Equator
	;--------------------------------------------------------------------------------------------
	eq_line_lon = (/0.,360./)
	eq_line_lat = (/0.,0./)
	lres@xyDashPattern = 1
	edum = new(num_p,graphic)
	do p = 0,num_p-1 edum(p) = gsn_add_polyline(wks,plot(p),eq_line_lon,eq_line_lat,lres) end do
;====================================================================================================
; Finalize Figure
;====================================================================================================
		pres = True
		pres@gsnFrame                     		= False
		pres@txString                       	= "00Z "+month+". "+pday+", 2011"
		pres@amJust                            	= "TopLeft"
		pres@gsnPanelFigureStringsFontHeightF  	= 0.01
		pres@gsnPanelFigureStrings             	= (/"a","b","c","d"/) 

	gsn_panel(wks,plot,(/1,num_p/),pres)
	;gsn_panel(wks,plot,(/2,2/),pres)

	legend = create "Legend" legendClass wks
		"lgAutoManage"              : False
		"vpXF"                      : 0.5
		"vpYF"                      : 0.5
		"vpWidthF"                  : 0.1
		"vpHeightF"                 : 0.06
		"lgPerimOn"                 : True
		"lgLabelsOn"                : True
		"lgLineLabelsOn"            : False
		"lgItemCount"               : 3
		"lgLabelStrings"            : (/"ECMWF","MIMIC","Soundings"/)
		"lgLabelFontHeightF"        : 0.01
		"lgDashIndex"             	: 0
		"lgLineThicknessF"			: 4.
		"lgLineColors"              : clr
		"lgMonoDashIndex"			: True
	end create

	;draw(legend)
	frame(wks)
  
	print("")
	print(" "+fig_file+"."+fig_type)
	print("")
	if fig_type.eq."png" then system("convert -trim "+fig_file+"."+fig_type+"   "+fig_file+"."+fig_type) end if

;====================================================================================================
;====================================================================================================
end
