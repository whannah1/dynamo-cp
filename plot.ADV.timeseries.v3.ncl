; Timeseries of Advective tendency
; tries to adjust resolution to matchs
; same as v2, except uses precomputed terms
; similar to plot.CP.timeseries.v6.ncl
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/contrib/time_axis_labels.ncl"
load "$NCARG_ROOT/custom_functions_DYNAMO_LSF.ncl"
load "$NCARG_ROOT/custom_functions_DYNAMO_ECMWF.ncl"
load "$NCARG_ROOT/custom_functions.ncl"
begin

    nsmooth = 2

    fig_type = "png"
    fig_file = "~/Research/DYNAMO/CP/ADV.timeseries.v3";.sm"+nsmooth
    
    debug = False

    reduceRes = True

;====================================================================================================
;====================================================================================================       
    sa = "lsan"
    
    if sa.eq. "lsan" then
        lat1 =   0.
        lat2 =   7.
        lon1 =  72.
        lon2 =  79.
    end if
    if sa.eq. "lsas" then
        lat1 =  -7.
        lat2 =   0.
        lon1 =  72.
        lon2 =  79.
    end if
    
    ;lat1 =  -10.
    ;lat2 =   10.
    ;lon1 =  65.
    ;lon2 =  85.
    
    wks = gsn_open_wks(fig_type,fig_file)
    plot = new(2,graphic)
        res = setres_default()
        res@vpHeightF                       = 0.15
        res@gsnLeftStringFontHeightF        = 0.01
        res@gsnCenterStringFontHeightF      = 0.01
        res@gsnRightStringFontHeightF       = 0.01
        res@tmXBLabelFontHeightF            = 0.005
        res@tmYLLabelFontHeightF            = 0.005
        res@tiXAxisFontHeightF              = 0.01
        res@tiYAxisFontHeightF              = 0.01
        res@gsnLeftString                   = ""
        res@gsnCenterString                 = ""
        res@gsnRightString                  = ""
        res@tmXBLabelAngleF                 = -50.
        
        lres = res
        lres@xyDashPattern              = 0
        lres@xyLineThicknessF           = 1.
        lres@xyLineColor                = "black"
        
        
        
;====================================================================================================
; Load Sounding Array Data
;====================================================================================================
    ver = "2b"
    if ver.eq."2a" then ifile = "~/Data/DYNAMO/LSA/v2a/dynamo_basic_v2a_2011all.nc" end if
    if ver.eq."2b" then ifile = "~/Data/DYNAMO/LSA/v2b/dynamo_basic_v2b_2011all.nc" end if
    infile = addfile(ifile,"r")
    SAlat = infile->lat({lat1:lat2})
    SAlon = infile->lon({lon1:lon2})
    ps  = infile->ps(:,{lat1:lat2},{lon1:lon2})
    p   = infile->level
    p!0 = "lev"
    p&lev = p
    dp  = calc_dP(p,ps) *100.
    u   = infile->u(:,:,{lat1:lat2},{lon1:lon2})
    v   = infile->v(:,:,{lat1:lat2},{lon1:lon2})
    wmr = infile->q(:,:,{lat1:lat2},{lon1:lon2})
    wmr = (/wmr/1000./)
    q   = wmr/(1.+wmr)
    copy_VarCoords(wmr,q)
    iSAADV = calc_adv(q,u,v) *-1.*86400.    
    bsz = 2
    SATPW = block_avg( dim_avg_n( dim_sum_n(        q*dp/g,1) ,(/1,2/)) ,bsz)
    SAADV = block_avg( dim_avg_n( dim_sum_n(   iSAADV*dp/g,1) ,(/1,2/)) ,8)
    SAnt = dimsizes(SATPW)
    SAtime = tofloat(ispan(0,SAnt-1,1))/4. + 273.
    printline()
    printVarSummary(SAADV)
    delete([/wmr,q,u,v,p,ps,dp/])
    
    do n = 0,nsmooth-1 smSAADV = wgt_runave_Wrap(SAADV, (/0.25,0.5,0.25/), 0) end do
    
;====================================================================================================
; Load ECMWF Data
;====================================================================================================
    if reduceRes then
        ifile = "~/Data/DYNAMO/ECMWF/data/6hour.DYNAMO.ECMWF.CP.SAres.nc"
    else
        ifile = "~/Data/DYNAMO/ECMWF/data/6hour.DYNAMO.ECMWF.CP.nc"
    end if
    infile = addfile(ifile,"r")

    nblk = 4
    ECTPW = block_avg( dim_avg_n_Wrap( infile->ECTPW(:,{lat1:lat2},{lon1:lon2}) ,(/1,2/)) ,nblk)
    ;ECDDT = block_avg( dim_avg_n_Wrap( infile->ECDDT(:,{lat1:lat2},{lon1:lon2}) ,(/1,2/)) ,nblk)
    ECADV = block_avg( dim_avg_n_Wrap( infile->ECADV(:,{lat1:lat2},{lon1:lon2}) ,(/1,2/)) ,nblk)
    ;ECCPR = block_avg( dim_avg_n_Wrap( infile->ECCPR(:,{lat1:lat2},{lon1:lon2}) ,(/1,2/)) ,nblk)
    

    do n = 0,nsmooth-1 smECADV = wgt_runave_Wrap(ECADV, (/0.25,0.5,0.25/), 0) end do
;====================================================================================================
; Create Plot 
;====================================================================================================
    bsz = 4

    time = SAtime(::bsz)
    time@units = "days since 2011-01-01"
    cdtime = cd_calendar(time,2)
    tmres = True
    tmres@ttmFormat         = "%D %c, 2011"
    tmres@ttmAxis           = "XB"
    tmres@ttmMinorStride    = 4/bsz
    tmres@ttmMajorStride    = 5*4/bsz
    time_axis_labels(time,res,tmres)
    
        clr = (/"blue","red"/)
        tres = res
        tres@xyLineThicknessF   = 8.
        ;if nsmooth.eq.0 then tres@gsnCenterString       = "Advective TPW Tendency (daily mean)" end if
        ;if nsmooth.ne.0 then tres@gsnCenterString       = "Advective TPW Tendency (smoothed daily mean)" end if
        tres@gsnCenterString    = "Advective CWV Tendency (daily mean)"
        tres@trYMinF            = -12
        tres@trYMaxF            =  4; 9
        tres@trXMinF            = min(time)
        tres@trXMaxF            = max(time)
        tres@xyDashPattern      = 0
        tres@xyLineColors       = clr
        tres@tiXAxisString      = ""
        tres@tiYAxisString      = "[mm day~S~-1~N~]"
    
    plot(0) = gsn_csm_xy(wks,time,(/  ECADV,  SAADV/),tres)
    
        tres@gsnCenterString       = "Advective CWV Tendency (smoothed daily mean)"
        
    plot(1) = gsn_csm_xy(wks,time,(/smECADV,smSAADV/),tres)

    num_p = dimsizes(plot)
    dum = new((/2,num_p/),graphic)
    do p = 0,num_p-1
        ;-----------------------------------------------------------------
        ; Adding shading when Revelle was off-site
        ;-----------------------------------------------------------------
            gres = True
            gres@gsFillColor        = "lightgray"
            gres@gsFillOpacityF     = 0.2

        yy = (/-1e3,1e3/)

        xx = (/31+1,31+7/) +273.-1
        xshade = (/xx(0),xx(0),xx(1),xx(1),xx(0)/)
        yshade = (/yy(0),yy(1),yy(1),yy(0),yy(0)/)
        dum(0,p) = gsn_add_polygon(wks,plot(p),xshade,yshade,gres)

        xx = (/31+30+10,31+30+15/) +273.-1
        xshade = (/xx(0),xx(0),xx(1),xx(1),xx(0)/)
        yshade = (/yy(0),yy(1),yy(1),yy(0),yy(0)/)
        dum(1,p) = gsn_add_polygon(wks,plot(p),xshade,yshade,gres)
        ;-----------------------------------------------------------------
        ;-----------------------------------------------------------------
            lres@xyLineColor           = "black"
            lres@xyLineThicknessF      = 1.
        ;overlay(plot(0),gsn_csm_xy(wks,time,(/ECADV,SAADV/),tres))
        overlay(plot(p) , gsn_csm_xy(wks,(/-1e3,1e3/),(/0.,0./),lres))

            lres@xyLineColor           = "green"
            lres@xyLineThicknessF      = 3.

        xx = (/1,1/)*(11)+273.-1
        overlay(plot(p),gsn_csm_xy(wks,xx,yy,lres))

        ;xx = (/1,1/)*(31+24)+273.-1
        xx = (/1,1/)*(31+30+7)+273.-1
        overlay(plot(p),gsn_csm_xy(wks,xx,yy,lres))
        ;-----------------------------------------------------------------
        ;-----------------------------------------------------------------
    end do
;====================================================================================================
;====================================================================================================   
if .not.debug then
    printline()
    print("EC  "+avg(ECADV))
    print("NSA "+avg(SAADV))
    printline()
end if
;====================================================================================================
; Finalize Figure
;====================================================================================================
        pres = setres_panel_labeled()
        pres@gsnFrame                           = False

    gsn_panel(wks,plot,(/dimsizes(plot),1/),pres)

    legend = create "Legend" legendClass wks
        "lgAutoManage"              : False
        "vpXF"                      : 0.47
        "vpYF"                      : 0.68
        "vpWidthF"                  : 0.1
        "vpHeightF"                 : 0.06
        "lgPerimOn"                 : True
        "lgLabelsOn"                : True
        "lgLineLabelsOn"            : False
        "lgItemCount"               : 2
        "lgLabelStrings"            : (/"ECMWF","LSA (2b)"/)
        "lgLabelFontHeightF"        : 0.01
        "lgDashIndex"               : 0
        "lgLineThicknessF"          : 4.
        "lgLineColors"              : clr
        "lgMonoDashIndex"           : True
    end create

    draw(legend)
    frame(wks)
  
    print("")
    print(" "+fig_file+"."+fig_type)
    print("")
    if fig_type.eq."png" then system("convert -trim "+fig_file+"."+fig_type+"   "+fig_file+"."+fig_type) end if

;====================================================================================================
;====================================================================================================
end
