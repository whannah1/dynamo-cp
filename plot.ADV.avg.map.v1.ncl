; Timeseries of 6t-hourly data at Gan
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/contrib/time_axis_labels.ncl"
load "$NCARG_ROOT/custom_functions_DYNAMO_LSF.ncl"
load "$NCARG_ROOT/custom_functions_DYNAMO_ECMWF.ncl"
load "$NCARG_ROOT/custom_functions.ncl"
begin

    fig_type = "png"
    fig_file = "~/Research/DYNAMO/CP/ADV.avg.map.v1"
    
;====================================================================================================
;====================================================================================================       
    if True then 
        lat1 = -20.
        lat2 =  20.
        lon1 =  40.
        lon2 = 120.
    else
        lat1 = -15.
        lat2 =  15.
        lon1 =  65.
        lon2 =  85.
    end if
    
    wks = gsn_open_wks(fig_type,fig_file)
        res = setres_contour()
        res@tmXBMinorOn                     = False
        res@tmYLMinorOn                     = False
        res@gsnLeftStringFontHeightF        = 0.015
        res@gsnCenterStringFontHeightF      = 0.015
        res@gsnRightStringFontHeightF       = 0.015
        res@gsnLeftString                   = ""
        res@gsnCenterString                 = ""
        res@gsnRightString                  = ""
;====================================================================================================
; Load MIMIC Data
;====================================================================================================
if False then
    ifile  = "~/Data/DYNAMO/MIMIC/cimss_morphed_tpw_DYNAMO_2011.nc"
    infile = addfile(ifile,"r")
    ;MInt   = dimsizes(infile->time)/6
    tmp = infile->tpw(:,{lat1:lat2},{lon1:lon2}) 
    ;tmp = infile->tpw(:,{ypts},{xpts}) 
    iMITPW = new(dimsizes(tmp),float)
    iMITPW = tofloat( tmp )
    delete(tmp)
    iMITPW = tofloat( where(isnan_ieee(iMITPW),iMITPW@_FillValue,iMITPW) ) 
    bsz = 6
    MITPW = block_avg( dim_avg_n_Wrap(iMITPW,(/1,2/)) ,bsz)
    printline()
    printVarSummary(MITPW)
end if
;====================================================================================================
; Load ECMWF Data
;====================================================================================================
    ifile = "~/Data/DYNAMO/ECMWF/data/6hour.DYNAMO.ECMWF.ADV.nc"
    infile = addfile(ifile,"r")

    ECTPW = infile->ECTPW(:,{lat1:lat2},{lon1:lon2})
    ECADV = infile->ECADV(:,{lat1:lat2},{lon1:lon2})

    ECADVmean = infile->ECADVmean(:,{lat1:lat2},{lon1:lon2})
    ECADVeddy = infile->ECADVeddy(:,{lat1:lat2},{lon1:lon2})

    aECTPW = dim_avg_n_Wrap(ECTPW,0)
    aECADV = dim_avg_n_Wrap(ECADV,0)
    aECADVmean = dim_avg_n_Wrap(ECADVmean,0)
    aECADVeddy = dim_avg_n_Wrap(ECADVeddy,0)
;====================================================================================================
; Create Plot 
;====================================================================================================   
    clr = (/"blue","green","red"/)
    
        tres = res
        ;tres@cnFillPalette         = ""
        tres@gsnAddCyclic           = False
        tres@mpLimitMode            = "LatLon"
        tres@mpMinLatF              = lat1
        tres@mpMaxLatF              = lat2
        tres@mpMinLonF              = lon1
        tres@mpMaxLonF              = lon2
        tres@gsnLeftString          = "ECMWF"   
    
    plot  = new(3,graphic)
    num_p = dimsizes(plot)

    tres@cnLevelSelectionMode   = "ExplicitLevels"
    tres@cnLevels               = ispan(-10,10,1)
    
    tres@gsnRightString         = "ADV"
    plot(0) = gsn_csm_contour_map(wks,aECADV,tres)


    ;delete(tres@cnLevels)
    ;tres@cnLevels               = ispan(2,22,2)
    tres@gsnRightString         = "ADV (mean)"
    plot(1) = gsn_csm_contour_map(wks,aECADVmean,tres)

    tres@gsnRightString         = "ADV (eddy)"
    plot(2) = gsn_csm_contour_map(wks,aECADVeddy,tres)
    
    ;--------------------------------------------------------------------------------------------
    ; Overlay mean TPW contours
    ;--------------------------------------------------------------------------------------------
        delete(tres)
        tres = res
        tres@cnFillOn   = False
        tres@cnLinesOn  = True
        tres@cnLevelSelectionMode   = "ExplicitLevels"
    do p = 0,num_p-1
        
        ; thin solid lines greater than threshold
        if isatt(tres,"cnLevels") then delete(tres@cnLevels) end if
        tres@cnLevels               = ispan(45,65,5)
        tres@cnLineDashPattern      = 0
        tres@cnLineThicknessF       = 3.
        overlay( plot(p) , gsn_csm_contour(wks,aECTPW,tres) )
        
        ; thin dashed lines less than threshold
        delete(tres@cnLevels)
        tres@cnLevels               = ispan(0,45,5)
        tres@cnLineDashPattern      = 2
        tres@cnLineThicknessF       = 3.
        overlay( plot(p) , gsn_csm_contour(wks,aECTPW,tres) )
        
        ; thick solid lines when equal to threshold
        delete(tres@cnLevels)
        tres@cnLineDashPattern      = 0
        tres@cnLineThicknessF       = 6.
        tres@cnLevels               = (/1/) * 45.
        overlay( plot(p) , gsn_csm_contour(wks,aECTPW,tres) )

    end do
    ;--------------------------------------------------------------------------------------------
    ; Add Array lines
    ;--------------------------------------------------------------------------------------------
    site_lat  = (/-0.7,     0.,     -7.3,   -7.3 ,   4.1,  6.9,  0./)
    site_lon  = (/73.2,     80.,        80.,        72.5 ,  73.5, 79.8, 80./)
    lres = True
    lres@gsLineColor        = "green"
    lres@gsLineThicknessF   = 4.
    ; NSA+SSA
    array_lat = new(8,float)
    array_lon = new(8,float)
    array_lat(:3) = site_lat(:3)
    array_lon(:3) = site_lon(:3)
    array_lat(4)  = site_lat(0)
    array_lon(4)  = site_lon(0)
    array_lat(5:) = site_lat(4:)
    array_lon(5:) = site_lon(4:)
    ldum = new(num_p,graphic)
    do p = 0,num_p-1 ldum(p) = gsn_add_polyline(wks,plot(p),array_lon,array_lat,lres) end do
    ;--------------------------------------------------------------------------------------------
    ; Add Line at Equator
    ;--------------------------------------------------------------------------------------------
    ;eq_line_lon = (/0.,360./)
    ;eq_line_lat = (/0.,0./)
    ;lres@xyDashPattern = 1
    ;edum = new(num_p,graphic)
    ;do p = 0,num_p-1 edum(p) = gsn_add_polyline(wks,plot(p),eq_line_lon,eq_line_lat,lres) end do
;====================================================================================================
; Finalize Figure
;====================================================================================================
    gsn_panel(wks,plot,(/num_p,1/),setres_panel_labeled())
  
    print("")
    print(" "+fig_file+"."+fig_type)
    print("")

    trimPNG(fig_file)

;====================================================================================================
;====================================================================================================
end
