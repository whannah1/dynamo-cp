; similar to plot.ADV.error.v2.ncl
; this script simply checks for consistency with Liebniz's integral rule
; Specifically it checks whether the integral of del(qv) is identical to
; del* the integral of qv
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/contrib/time_axis_labels.ncl"
;load "$NCARG_ROOT/custom_functions_DYNAMO_ECMWF.ncl"
load "$NCARG_ROOT/custom_functions.ncl"
begin
    
    fig_type = "png"
    fig_file = "~/Research/DYNAMO/CP/ADV.error.v3"
    
    verbose = False

    lat1 =  -40.
    lat2 =   40.
    lon1 =    0.
    lon2 =  360.
    
    recalc = True
    
    absFlag = False
    
    lev1 =  100
    lev2 = 1000

;====================================================================================================
; Load ECMWF
;====================================================================================================
    idir = "~/Data/DYNAMO/ECMWF/data/"

    tfile = idir+"ADV.error.nc"
    
    ufile = addfile(idir+"6hour.DYNAMO.ECMWF.U.nc","r")
    vfile = addfile(idir+"6hour.DYNAMO.ECMWF.V.nc","r")
    qfile = addfile(idir+"6hour.DYNAMO.ECMWF.Q.nc","r")
    lat = ufile->lat({lat1:lat2})
    lon = ufile->lon({lon1:lon2})
    
    if recalc then
        ;---------------------------------------------------------------
        ;---------------------------------------------------------------
        print("Loading EC data...")
        
        t1 = 25*4
        t2 = t1+1*4
    
        if verbose then printMM(lat) end if
        if verbose then printMM(lon) end if

        u = ufile->U(t1:t2,{lev1:lev2},{lat1:lat2},{lon1:lon2})
        v = vfile->V(t1:t2,{lev1:lev2},{lat1:lat2},{lon1:lon2})
        q = qfile->Q(t1:t2,{lev1:lev2},{lat1:lat2},{lon1:lon2})
    
        infile = addfile(idir+"6hour.DYNAMO.ECMWF.Ps.nc","r")
        ps = infile->Ps(t1:t2,{lat1:lat2},{lon1:lon2})  
        lev = q&lev
        p   = lev*100.
        p@units = "Pa"
        p!0 = "lev"
        p&lev = p
        dp = calc_dP(p,ps)
        
        coldp = dim_sum_n_Wrap(dp/g,1)
        
        dqdx = calc_ddx(q)
        dqdy = calc_ddy(q)
        
        dqdxb = dim_sum_n_Wrap(dqdx*dp/g,1) /coldp 
        dqdyb = dim_sum_n_Wrap(dqdy*dp/g,1) /coldp 
        ;qb = dim_sum_n_Wrap(q*dp/g,1) /coldp
        ub = dim_sum_n_Wrap(u*dp/g,1) /coldp
        vb = dim_sum_n_Wrap(v*dp/g,1) /coldp
        
        dqdxp = dqdx - conform(dqdx,dqdxb,(/0,2,3/))
        dqdyp = dqdy - conform(dqdy,dqdyb,(/0,2,3/))
        ;qp = q - conform(q,qb,(/0,2,3/))
        up = u - conform(u,ub,(/0,2,3/))
        vp = v - conform(v,vb,(/0,2,3/))
        
        ;copy_VarCoords(q,qp)
        ;copy_VarCoords(q(:,0,:,:),qb)
        ;dqbdx = calc_ddx(qb)
        ;dqbdy = calc_ddy(qb)
        ;dqpdx = calc_ddx(qp)
        ;dqpdy = calc_ddy(qp)
        ;---------------------------------------------------------------
        ;---------------------------------------------------------------
        print("Calculating advection...")
    
        ADV0 = dim_sum_n( (u*dqdx + v*dqdy) *dp/g,1)
        
        ;ADV1 = ( ub*dqdxb + vb*dqdyb ) *coldp
        ;ADV2 = dim_sum_n( (up*dqdxp + vp*dqdyp) *dp/g,1)

        ADV1 = dim_sum_n(dqdx*dp/g,1) + dim_sum_n(dqdy*dp/g,1)
        
        CWV = dim_sum_n_Wrap(q*dp/g,1)
        copy_VarCoords(q(:,0,:,:),CWV)
        ADV2 = calc_ddx(CWV) + calc_ddy(CWV)
        
        copy_VarCoords(q(:,0,:,:),ADV0)
        copy_VarCoords(ADV0,ADV1)
        copy_VarCoords(ADV0,ADV2)
        
        delete([/u,v,q,ub,vb,up,vp,dqdx,dqdy/])
        delete([/dqdxb,dqdyb,dqdxp,dqdyp/])
        ;delete([/qb,qp,dqbdx,dqbdy,dqpdx,dqpdy/])
        ;---------------------------------------------------------------
        ;---------------------------------------------------------------    
        ADV0 = (/ADV0*86400.*-1./)
        ADV1 = (/ADV1*86400.*-1./)
        ADV2 = (/ADV2*86400.*-1./)
                
        ;if isfilepresent(tfile) then system("rm "+tfile) end if
        ;outfile = addfile(tfile,"c")
        ;outfile->adv = adv
        ;outfile->ADV = ADV
        ;outfile->dp  = dp
    else
        ;infile = addfile(tfile,"r")
        ;adv = infile->adv  (:1,:,:,:)
        ;ADV = infile->ADV  (:1,:,:)
        ;dp  = infile->dp   (:1,:,:,:)
    end if

;====================================================================================================
;====================================================================================================   
    printline()
    
    A0 = dim_avg_n_Wrap(ADV0,0)
    A1 = dim_avg_n_Wrap(ADV1,0)
    A2 = dim_avg_n_Wrap(ADV2,0)
    
    A3 = ( A0 - (A1+A2) )
    copy_VarCoords(A0,A3)
    
    if absFlag then
        A0 = abs(A0)
        A1 = abs(A1)
        A2 = abs(A2)
    end if
;====================================================================================================
; Plot Map
;====================================================================================================\
    ;fig_type@wkWidth  = 2048
    ;fig_type@wkHeight = 2048
    wks = gsn_open_wks(fig_type,fig_file)
    plot = new(3,graphic)
        res = True
        res@gsnDraw                         = False
        res@gsnFrame                        = False
        res@vpHeightF                       = 0.3
        res@tmXBOn                          = False
        res@tmYLOn                          = False
        res@tmXBMinorOn                     = False
        res@tmYLMinorOn                     = False
        res@gsnLeftStringFontHeightF        = 0.015
        res@gsnCenterStringFontHeightF      = 0.015
        res@gsnRightStringFontHeightF       = 0.015
        res@tmXBLabelFontHeightF            = 0.01
        res@tmYLLabelFontHeightF            = 0.01
        ;res@tiXAxisFontHeightF             = 0.01
        ;res@tiYAxisFontHeightF             = 0.01
        res@tmXBMajorOutwardLengthF         = 0.0
        res@tmXBMinorOutwardLengthF         = 0.0
        res@tmYLMajorOutwardLengthF         = 0.0
        res@tmYLMinorOutwardLengthF         = 0.0
        res@gsnLeftString                   = ""
        res@gsnCenterString                 = ""
        res@gsnRightString                  = ""
        
        lres = res
        lres@xyDashPattern              = 0
        lres@xyLineThicknessF           = 1.
        lres@xyLineColor                = "black"
        
        res@gsnAddCyclic                    = False
        res@lbTitlePosition                 = "bottom"
        res@lbLabelFontHeightF              = 0.01
        res@lbTopMarginF                    = 0.08
        res@lbTitleString                   = ""
        res@trYReverse              = True
        res@cnFillOn                = True
        res@cnLinesOn               = False
        res@cnFillMode              = "RasterFill"
        res@mpLimitMode             = "LatLon"
        res@mpMinLatF               = lat1
        res@mpMaxLatF               = lat2
        res@mpCenterLonF            = 180.
        res@cnLevelSelectionMode    = "ExplicitLevels"
        res@gsnRightString          = "[mm day~S~-1~N~]"
        res@cnLineLabelsOn          = False
        res@cnInfoLabelOn           = False
        
        if absFlag.eq.True  then res@cnFillPalette  = "WhiteBlueGreenYellowRed" end if
        
        if absFlag.eq.False then res@cnLevels   = ispan(-30,30,1)       end if
        if absFlag.eq.True  then res@cnLevels   = ispan(1,20,1)+1       end if
        
        
        res@gsnLeftString           = "Total Column Advection  ~F18~O~F22~v~F18~Vt~F21~q~F18~P~F21~ "
    plot(0) = gsn_csm_contour_map(wks,A0,res)
    
        if isatt(res,"cnLevels") then delete(res@cnLevels) end if
        res@cnLevels        = ispan(-40,40,2) 
        res@gsnRightString  = "[mm]"

        res@gsnLeftString           = "";~F18~O~F22~v~F18~PVOt~F21~q~F18~P~F21~"
    plot(1) = gsn_csm_contour_map(wks,A1,res)
    
        res@lbLabelBarOn            = True
        res@gsnLeftString           = "";"~F18~O~F22~v~F21~'~F18~Vt~F21~q'~F18~P~F21~"
    ;plot(2) = gsn_csm_contour_map(wks,A2,res)
    A3 = A2
    A3 = (/A2-A1/)
    plot(2) = gsn_csm_contour_map(wks,A3,res)
    
    
        if isatt(res,"cnLevels") then delete(res@cnLevels) end if
        ;res@cnFillPalette          = "WhiteBlueGreenYellowRed"
        res@cnFillPalette           = "ncl_default"
        res@cnLevels                = ispan(-100,100,5)/1e1
        res@lbLabelBarOn            = True
        res@gsnLeftString           = "Residual = ADV0 - ( ADV1 + ADV2 )"
    ;plot(3) = gsn_csm_contour_map(wks,A3,res)
    
    do x = 0,dimsizes(plot)-1
        if .not.ismissing(plot(x)) then overlay(plot(x) , gsn_csm_xy(wks,(/-1e3,1e3/),(/0.,0./),lres)) end if
    end do
;====================================================================================================
; Finalize Figure
;====================================================================================================
        pres = True
        pres@amJust                            = "TopLeft"
        pres@gsnPanelFigureStringsFontHeightF  = 0.015
        pres@gsnPanelFigureStrings             = (/"a","b","c","d"/) 

    gsn_panel(wks,plot,(/dimsizes(plot),1/),pres)
  
    print("")
    print(" "+fig_file+"."+fig_type)
    print("")
    if fig_type.eq."png" then system("convert -trim "+fig_file+"."+fig_type+"   "+fig_file+"."+fig_type) end if

;====================================================================================================
;====================================================================================================
end
