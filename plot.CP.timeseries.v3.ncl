; Similar to v1, plots a timeseries of TPW, but uses anomalies 
; instead of the total TPW since mean values are different between
; ECMWF and DYNAMO NSA. Also, the Lagrangian Tendency of the 
; ECMWF water budget is overlayed using the right Y-Axis. 
; A smoothed version of the Lagrangian Tendency is also overlayed.
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/contrib/time_axis_labels.ncl"
load "$NCARG_ROOT/custom_functions_DYNAMO_LSF.ncl"
load "$NCARG_ROOT/custom_functions_DYNAMO_ECMWF.ncl"
begin

	fig_type = "x11"
	fig_file = "~/Research/DYNAMO/CP/CP.timeseries.v3"

	sa = "lsan"
	
	daily = True
	
	nsmooth = 3

;====================================================================================================
;====================================================================================================
	;lat1 =  -10.
	;lat2 =   10.
	;lon1 =  70.
	;lon2 =  83.
	if sa.eq."lsan" then
		lat1 =  -1.
		lat2 =   8.
		lon1 =  72.
		lon2 =  81.
	end if
	if sa.eq."lsas" then
		lat1 = -8.
		lat2 = -1.
		lon1 =  72.
		lon2 =  81.
	end if


	wks = gsn_open_wks(fig_type,fig_file)
	gsn_define_colormap(wks,"WhiteBlueGreenYellowRed")
	;gsn_define_colormap(wks,"ncl_default")
	plot = new(2,graphic)
		res = True
		res@gsnDraw                         = False
		res@gsnFrame                        = False
		res@vpHeightF						= 0.4
		res@tmXTOn                          = False
		res@gsnLeftStringFontHeightF        = 0.015
		res@gsnCenterStringFontHeightF    	= 0.015
		res@gsnRightStringFontHeightF       = 0.015
		res@tmXBLabelFontHeightF            = 0.015
		res@tmYLLabelFontHeightF            = 0.015
		res@tiXAxisFontHeightF				= 0.015
		res@tiYAxisFontHeightF				= 0.015
		res@tmXBMajorOutwardLengthF         = 0.0
		res@tmXBMinorOutwardLengthF         = 0.0
		res@tmYLMajorOutwardLengthF         = 0.0
		res@tmYLMinorOutwardLengthF         = 0.0
		res@gsnLeftString                   = ""
		res@gsnCenterString              	= ""
		res@gsnRightString                  = ""
		
		lres = res
		lres@xyDashPattern              = 0
		lres@xyLineThicknessF           = 1.
		lres@xyLineColor                = "black"
;====================================================================================================
; Load Sounding Array Data
;====================================================================================================
	ps  = LoadFieldsSfc("p"     ,sa)
	p   = LoadFields("p"        ,sa)
	wmr = LoadFields("wmr"      ,sa)/1000.
	q = wmr/(1.+wmr)
	dp = calc_dp(p,ps) *100.
	
	ohq = LoadLSF("hq"  ,sa)
	;ovq = LoadLSF("vq" ,sa)

	oqdim   = dimsizes(q)
	doqdt   = new(oqdim,float)
	doTPWdt = new(oqdim(0),float)
	dt = 6*3600.
	doqdt(1:oqdim(0)-2,:) = ( q(2:oqdim(0)-1,:) - q(0:oqdim(0)-3,:) ) / dt
	;doTPWdt = dim_sum_n(doqdt*dp/g,1)

	toTPW = dim_sum_n( q*dp/g,1) /1000. *1000.  ; divide by density and convert to mm
	;toLT  = doTPWdt + ohq
	toLT  = dim_sum_n(doqdt*dp/g,1) + dim_sum_n(ohq*dp/g,1)

	
	if      daily then 
		oTPW = avg4to1( toTPW )
		oLT  = avg4to1( toLT  )
	end if
	if .not.daily then 
		oTPW = toTPW
		oLT  = toLT
	end if

	not = dimsizes(oTPW)
	otime = ispan(0,not-1,1)
;====================================================================================================
; Load ECMWF Data
;====================================================================================================
	;ifile = "~/Research/DYNAMO/ECMWF/data/EC.budget.LQ.nc"
	ifile = "~/Data/DYNAMO/ECMWF/data/EC.budget.LQ.nc"
	infile = addfile(ifile,"r")

	if      daily then t2 = not*4-1 end if
	if .not.daily then t2 = not-1   end if
	Q       = infile->LQvi  (:t2,{lat1:lat2},{lon1:lon2})
	DLQDT   = infile->DLQDT (:t2,{lat1:lat2},{lon1:lon2})
	udQdx   = infile->udQdx (:t2,{lat1:lat2},{lon1:lon2})
	vdQdy   = infile->vdQdy (:t2,{lat1:lat2},{lon1:lon2})
	;VdelQ   = infile->VdelQ (:t2,{lat1:lat2},{lon1:lon2})
	LHFLX   = infile->LHFLX (:t2,{lat1:lat2},{lon1:lon2})

	TPW = Q
	TPW = (/Q/Lv/)
	TPW@long_name = "TPW [mm]"
	
	;udQdx = (/udQdx*-1./)
	;vdQdy = (/vdQdy*-1./)

	LT1 = ( DLQDT + udQdx + vdQdy )/Lv
	LT2 = LT1;( DLQDT + udQdx + vdQdy - LHFLX )/Lv
	copy_VarCoords(Q,LT1)
	copy_VarCoords(Q,LT2)

	taLT1 = dim_avg_n_Wrap(LT1,(/1,2/)) 
	taLT2 = dim_avg_n_Wrap(LT2,(/1,2/)) 
	taTPW = dim_avg_n_Wrap(TPW,(/1,2/))
	taLHF = dim_avg_n_Wrap(LHFLX,(/1,2/))
	if      daily then 
		aTPW = avg4to1( taTPW )
		aLT1 = avg4to1( taLT1 )
		aLT2 = avg4to1( taLT2 ) 
		aLHF = (taLHF(0::4)+taLHF(2::4))/(2.*Lv)
		aLT2 = (/ aLT2 - aLHF /)
	end if
	if .not.daily then 
		aTPW = taTPW
		aLT1 = taLT1
		aLT2 = taLT2
	end if

	nt = dimsizes(aTPW)
	time = ispan(0,nt-1,1)
;====================================================================================================
; Plot Map
;====================================================================================================
	if daily then
		time@units = "days since 2011-10-01"
	else
		time = time*6
		time@units = "hours since 2011-10-01"
	end if
	cdtime = cd_calendar(time,2)
	tmres = True
	tmres@ttmFormat = "%D%c"
	tmres@ttmAxis   = "XB"
	time_axis_labels(time,res,tmres)

		tres1 = res
		tres1@xyLineThicknessF          = 4.
		tres1@trYMinF                   = -12.
		tres1@trYMaxF                   =  12.
		tres1@tiYAxisString             = "TPW Anomaly [mm]"

		tres2 = res
		tres2@xyDashPattern             = 0     
		tres2@trYMinF                   = -8.
		tres2@trYMaxF                   =  8.
		tres2@tiYAxisString             = "Lagrangian TPW Tendency [mm/day]"; (x10~S~3~N~)"

	oTPW = dim_rmvmean_Wrap(oTPW)
	aTPW = dim_rmvmean_Wrap(aTPW)

	aLT1 = (/aLT1*86400./)
	aLT2 = (/aLT2*86400./)
	aLHF = (/aLHF*86400./)
	oLT  = (/oLT *86400./)

	do n = 0,nsmooth-1 aLT1 = wgt_runave_Wrap(aLT1, (/0.25,0.5,0.25/), 0) end do
	do n = 0,nsmooth-1 aLT2 = wgt_runave_Wrap(aLT2, (/0.25,0.5,0.25/), 0) end do
	do n = 0,nsmooth-1 oLT  = wgt_runave_Wrap(oLT , (/0.25,0.5,0.25/), 0) end do
	do n = 0,nsmooth-1 aLHF = wgt_runave_Wrap(aLHF, (/0.25,0.5,0.25/), 0) end do
		

		tres1@gsnCenterString   = "ECMWF Analysis"
		tres1@xyLineColor       = "black"
		tres2@xyLineColor       = "red"
		tres1@xyDashPattern     = 0
		tres2@xyDashPattern     = 0
		tres2@xyLineThicknessF  = 4.
	plot(0) = gsn_csm_xy2(wks,time,aTPW,aLT1,tres1,tres2)


		tres1@gsnCenterString   = "DYNAMO Sounding Array"
		tres1@xyLineColor   	= "black"
		tres2@xyLineColor   	= "red"
		tres1@xyDashPattern    	= 0
		tres2@xyDashPattern    	= 0
	plot(1) = gsn_csm_xy2(wks,time,oTPW,oLT,tres1,tres2)

	overlay(plot(0) , gsn_csm_xy(wks,(/-1e3,1e3/),(/0.,0./),lres))
	overlay(plot(1) , gsn_csm_xy(wks,(/-1e3,1e3/),(/0.,0./),lres))

;====================================================================================================
; Finalize Figure
;====================================================================================================
		pres = True
		pres@gsnFrame                           = False
		pres@txString                           = ""
		;pres@gsnPanelBottom                    = 0.1 
		;pres@amJust                            = "TopLeft"
		;pres@gsnPanelFigureStringsFontHeightF  = 0.015
		;pres@gsnPanelFigureStrings             = (/"a","b","c","d"/) 

	gsn_panel(wks,plot,(/dimsizes(plot),1/),pres)

	legend = create "Legend" legendClass wks
		"lgAutoManage"              : False
		"vpXF"                      : 0.4
		"vpYF"                      : 0.3
		"vpWidthF"                  : 0.2
		"vpHeightF"                 : 0.12
		"lgPerimOn"                 : True
		"lgLabelsOn"                : True
		"lgLineLabelsOn"            : False
		"lgItemCount"               : 4
		;"lgLabelStrings"            : (/"ECMWF TPW","NSA TPW","ECMWF Lagrangian Tend.","LHFLX"/)
		"lgLabelStrings"            : (/"ECMWF TPW","NSA TPW","ECMWF Lagrangian Tend.","NSA Lagrangian Tend."/)
		"lgLabelFontHeightF"        : 0.014
		"lgDashIndexes"             : (/0,1,0,1/)
		"lgLineThicknesses"         : (/4.,4.,4.,4./)
		"lgLineColors"              : (/"black","black","red","red"/)
		"lgMonoLineThickness"       : False
	end create

	;draw(legend)
	frame(wks)
  
	print("")
	print(" "+fig_file+"."+fig_type)
	print("")
	if fig_type.eq."png" then system("convert -trim "+fig_file+"."+fig_type+"   "+fig_file+"."+fig_type) end if

;====================================================================================================
;====================================================================================================
end
