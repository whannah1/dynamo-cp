; based off v1, but bins conditionally
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/custom_functions.ncl"
begin

    src = "SPOL"

    fig_type = "png"
    fig_file = "~/Research/DYNAMO/CP/radar.bin.v6"
    
    debug       = False
;====================================================================================================
;====================================================================================================           
    wks = gsn_open_wks(fig_type,fig_file)
        res = True
        res@gsnDraw                         = False
        res@gsnFrame                        = False
        res@vpHeightF                       = 0.45
        res@tmXTOn                          = False
        res@tmXBMinorOn                     = False
        res@tmYLMinorOn                     = False
        res@tmYRMinorOn                     = False
        res@gsnLeftStringFontHeightF        = 0.025
        res@gsnCenterStringFontHeightF      = 0.025
        res@gsnRightStringFontHeightF       = 0.025
        res@tmXBLabelFontHeightF            = 0.02
        res@tmYLLabelFontHeightF            = 0.02
        res@tiXAxisFontHeightF              = 0.02
        res@tiYAxisFontHeightF              = 0.02
        res@tmXBMajorOutwardLengthF         = 0.0
        res@tmXBMinorOutwardLengthF         = 0.0
        res@tmYLMajorOutwardLengthF         = 0.0
        res@tmYLMinorOutwardLengthF         = 0.0
        res@gsnLeftString                   = ""
        res@gsnCenterString                 = ""
        res@gsnRightString                  = ""
        ;res@tmXBLabelAngleF                    = -50.
        
        lres = res
        lres@xyDashPattern              = 0
        lres@xyLineThicknessF           = 3.
        lres@xyLineColor                = "black"
        
        cres = res
        cres@cnFillOn               = True
        cres@cnLinesOn              = False
        cres@cnSpanFillPalette      = True
        cres@trYReverse             = True
;====================================================================================================
; Load Radar Data
;====================================================================================================
    if src.eq."SPOL" then ifile = "~/Data/DYNAMO/RADAR/SPOL/spol_radar_gridded.v2.alt.OND_2011_DYNAMO.nc" end if
    infile = addfile(ifile,"r")
    ;rtime = infile->time
    ;rtime = (/ rtime - rtime(0) /)
    ;rtime@units = "hours since 2011-10-01 00:00:00"
    
    rlat = infile->lat
    rlon = infile->lon
    lat1 = min(rlat) 
    lat2 = max(rlat) 
    lon1 = min(rlon) 
    lon2 = max(rlon) 
    
    
    rain_total = infile->rain_total
    rain_other = infile->rain_other
    rain_strat = infile->rain_strat
    rain_deep  = infile->rain_deep
    rain_cong  = infile->rain_cong
    rain_shal  = infile->rain_shal

    frac_cong  = infile->frac_cong
    frac_deep  = infile->frac_deep
    frac_shal  = infile->frac_shal
    frac_strat = infile->frac_strat
    frac_other = infile->frac_other

    avg_frac_deep  = conform(rain_total, dim_avg_n_Wrap(frac_deep ,(/1,2/)) ,0)
    avg_frac_strat = conform(rain_total, dim_avg_n_Wrap(frac_strat,(/1,2/)) ,0)

;====================================================================================================
; Load ECMWF Data
;====================================================================================================   
    ifile = "~/Data/DYNAMO/ECMWF/data/6hour.DYNAMO.ECMWF.CP.nc"
    infile = addfile(ifile,"r")

    nblk = 4
    ECTPW = infile->ECTPW(:,{lat1:lat2},{lon1:lon2})
    ECDDT = infile->ECDDT(:,{lat1:lat2},{lon1:lon2})
    ECADV = infile->ECADV(:,{lat1:lat2},{lon1:lon2})
    ECCPR = infile->ECCPR(:,{lat1:lat2},{lon1:lon2})
;====================================================================================================
; Bin data
;====================================================================================================
    bins = True
    bins@verbose = False
    bins@bin_min = -21 ;-3
    bins@bin_max =  21 ;+3
    bins@bin_spc =  3

;bins@bin_min = -10
;bins@bin_max =  14
;bins@bin_spc =  2
    
    ngrp = 1
    nvar = 2
    
    afrac_deep  := conform(frac_strat, dim_avg_n_Wrap(frac_deep ,(/1,2/)) ,0)
    afrac_strat := conform(frac_strat, dim_avg_n_Wrap(frac_strat,(/1,2/)) ,0)

    print("Binning data...")
    do n = 0,ngrp-1
    do v = 0,nvar-1
        print("    v = "+v+"    v%nvar = "+v%nvar)
        
        Vx = ECCPR
        if n.eq.0 then
            if v.eq.0 then Vy = rain_strat  end if
            if v.eq.1 then Vy = rain_strat  end if
            if v.eq.2 then Vy = rain_deep  end if
            if v.eq.3 then Vy = rain_strat end if
            if v.eq.4 then Vy = rain_other end if
        end if

        if n.eq.1 then
            if v.eq.0 then Vy = frac_strat  end if
            if v.eq.1 then Vy = frac_cong  end if
            if v.eq.2 then Vy = frac_deep  end if
            if v.eq.3 then Vy = frac_strat end if
            if v.eq.4 then Vy = frac_other end if
        end if

        ;if n.eq.0 then Vy = (/ Vy/stddev(Vy) /) end if
        ;Vy = (/ Vy/stddev(Vy) /)

        if v.eq.1 then Vy = where(avg_frac_deep.le.0.05,Vy,0.) end if
        if v.eq.1 then Vy = where(ECTPW.ge.60.,Vy,Vy@_FillValue) end if

        Vy = (/ Vy/stddev(Vy) /)

        ;Vy1 = rain_strat
        ;Vy2 = rain_deep
        ;Vy1 := dim_avg_n_Wrap(Vy1,(/1,2/))
        ;Vy2 := dim_avg_n_Wrap(Vy2,(/1,2/))
        ;Vy1 = (/ Vy1/stddev(Vy1) /)
        ;Vy2 = (/ Vy2/stddev(Vy2) /)
        ;Vy := ( Vy1 - Vy2 )/2.

        ;Vy = where(afrac_deep.lt.stddev(afrac_deep)*1.,Vy,Vy@_FillValue)
        ;Vy = where(afrac_strat.gt.stddev(afrac_strat)*1.,Vy,Vy@_FillValue)

        ;Vx := dim_avg_n_Wrap(Vx,(/1,2/))
        ;Vy := dim_avg_n_Wrap(Vy,(/1,2/))

        tmp = bin_YbyX(Vy,Vx,bins)

        if n.eq.0 .and. v.eq.0 then
            xbin   = tmp&bin
            nbin = dimsizes(xbin)
            bdim   = (/ngrp,nvar,nbin/)
            binval = new(bdim,float)
            bincnt = new(bdim,float)
            bincnt = 0
        end if

        binval(n,v,:) = tmp
        bincnt(n,v,:) = tmp@pct
                
        delete([/Vy,Vx/])
    end do
    end do
    
    name = new((/5/),string)

    name(0) = "shallow"
    name(1) = "congestus "
    name(2) = "deep"
    name(3) = "stratiform"
    name(4) = "other"

    name := name(:nvar-1)
;====================================================================================================
; Create Plot
;====================================================================================================
    plot = new(ngrp,graphic)

        clr = (/"red","green","blue","purple","gray"/)
        
        lres@trXMinF    = min(xbin)
        lres@trXMaxF    = max(xbin)
        lres1 = lres
        lres2 = lres
        lres1@tiXAxisString     = "Lagrangian Tendency [mm day~S~-1~N~]"
        lres2@tiYAxisString     = "[% of observations]"
        lres1@xyLineThicknessF  = 8.
        lres2@xyLineThicknessF  = 3.
        lres2@xyDashPattern     = 1

        lres1@xyLineColors = clr
        
    do n = 0,ngrp-1

        if n.eq.0 then 
            lres1@tiYAxisString     = ""
            lres1@gsnLeftString     = ""
            lres1@tiYAxisString     = "";"[mm day~S~-1~N~]"
        end if
        if n.eq.1 then 
            lres1@tiYAxisString     = ""
            lres1@gsnLeftString     = "Standardized Area Fraction"
            lres1@tiYAxisString     = ""
        end if

        plot(n) = gsn_csm_xy2(wks,xbin,binval(n,:,:),bincnt(n,0,:),lres1,lres2)

        overlay(plot(n) , gsn_csm_xy(wks,(/0,0/),(/-1e4,1e4/),lres))

    end do
;====================================================================================================
; Finalize Figure
;====================================================================================================
        pres = True
        pres@gsnFrame                           = False
        pres@amJust                             = "TopLeft"
        pres@gsnPanelFigureStringsFontHeightF   = 0.015
        pres@gsnPanelFigureStrings              = (/"a","b","c","d"/)
        pres@gsnPanelXWhiteSpacePercent = 5

    layout = (/1,dimsizes(plot)/)
    
    gsn_panel(wks,plot,layout,pres)

    legend = create "Legend" legendClass wks
        "lgAutoManage"              : False
        ;"vpXF"                      : 0.22
        ;"vpYF"                      : 0.93
        ;"vpWidthF"                  : 0.1
        ;"vpHeightF"                 : 0.1
        "vpXF"                      : 0.78
        "vpYF"                      : 0.44
        "vpWidthF"                  : 0.06
        "vpHeightF"                 : 0.05
        "lgPerimOn"                 : True
        "lgLabelsOn"                : True
        "lgLineLabelsOn"            : False
        "lgItemCount"               : dimsizes(name(::-1))
        "lgLabelStrings"            : name(::-1)
        "lgLabelFontHeightF"        : 0.01
        "lgDashIndex"               : 0
        "lgLineThicknessF"          : 8.
        "lgLineColors"              : clr(:nvar-1:-1)
        "lgMonoDashIndex"           : True
    end create

    ;draw(legend)
    frame(wks)
  
    trimPNG(fig_file)

;====================================================================================================
;====================================================================================================
end
