; Timeseries of 6t-hourly data at Gan
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/contrib/time_axis_labels.ncl"
;load "$NCARG_ROOT/custom_functions_DYNAMO_LSF.ncl"
;load "$NCARG_ROOT/custom_functions_DYNAMO_ECMWF.ncl"
load "$NCARG_ROOT/custom_functions.ncl"
begin

	fig_type = "png"
	fig_file = "~/Research/DYNAMO/CP/TPW.timeseries.v4"

	daily = True
	
	nsmooth = 0

;====================================================================================================
;====================================================================================================		
	bins = True
	bins@verbose = False
    bins@bin_min = 30
    bins@bin_max = 74
    bins@bin_spc =  2
    xbin = ispan(bins@bin_min,bins@bin_max,bins@bin_spc)
	
	sa = "lsan"
	
	if sa.eq. "lsan" then
		lat1 =   0.
		lat2 =   7.
		lon1 =  72.
		lon2 =  79.
	end if
	if sa.eq. "lsas" then
		lat1 =  -7.
		lat2 =   0.
		lon1 =  72.
		lon2 =  79.
	end if
	
	lat1 = 0.5
	lon1 = 73.5
	
	ypts = (/lat1,lat1,lat1,lat1/)
	xpts = (/lon1,lon1,lon1,lon1/)
    
    line = "----------------------------------------------"	
	
	wks = gsn_open_wks(fig_type,fig_file)
	plot = new(2,graphic)
		res = True
		res@gsnDraw                         = False
		res@gsnFrame                        = False
		res@tmXTOn                          = False
		res@gsnLeftStringFontHeightF        = 0.015
		res@gsnCenterStringFontHeightF    	= 0.015
		res@gsnRightStringFontHeightF       = 0.015
		res@tmXBLabelFontHeightF            = 0.008
		res@tmYLLabelFontHeightF            = 0.008
		res@tiXAxisFontHeightF				= 0.01
		res@tiYAxisFontHeightF				= 0.01
		res@tmXBMajorOutwardLengthF         = 0.0
		res@tmXBMinorOutwardLengthF         = 0.0
		res@tmYLMajorOutwardLengthF         = 0.0
		res@tmYLMinorOutwardLengthF         = 0.0
		res@gsnLeftString                   = ""
		res@gsnCenterString              	= ""
		res@gsnRightString                  = ""
		res@tmXBLabelAngleF					= -50.
		
		lres = res
		lres@xyDashPattern              = 0
		lres@xyLineThicknessF           = 1.
		lres@xyLineColor                = "black"

;====================================================================================================
; Load NEW Gan Radiometer Data
;====================================================================================================
	ifiles = systemfunc("ls -1 ~/Data/DYNAMO/GAN/new_data/*cdf")
	infiles = addfiles(ifiles,"r")
	;ListSetType (infiles, "join") 
	num_f = dimsizes(ifiles)	
	RATPW = infiles[:]->phys_pwv

	RATPW = (/RATPW*1e1/)
	RATPW@units = "mm"

	printVarSummary(RATPW)
	printMAM(RATPW)
	exit
;====================================================================================================
; Load OLD Gan Radiometer Data
;====================================================================================================
	ifile = "~/Research/DYNAMO/GAN/miamiradiometer_gan.cdf"
	infile = addfile(ifile,"r")
	RAtime = infile->time
	RAnt = dimsizes(RAtime)
	RAwvp = new(RAnt,float)
	RAlwp = new(RAnt,float)
	RAwvp = infile->wvp
	RAlwp = infile->lwp
	
	RAwvp = (/RAwvp*1e1/)
	RAlwp = (/RAlwp*1e1/)
	
	RAwvp = where(isnan_ieee(RAwvp),RAwvp@_FillValue,RAwvp)
	RAlwp = where(isnan_ieee(RAlwp),RAlwp@_FillValue,RAlwp)
	
	RAnt = dimsizes(SAtime)
	iRATPW = new(RAnt,float)
	iRATPW!0 = "time"
	iRATPW&time = SAtime
	do t = 0,RAnt-1
		condition = abs(SAtime(t)-RAtime).le.(1./8.)
		tmp = where(condition,RAwvp,RAwvp@_FillValue)
		if .not.all(ismissing(tmp)) then iRATPW(t)  = avg(tmp) end if
		delete(tmp)
	end do
	
	RATPW  = iRATPW
	aRATPW = iRATPW
;====================================================================================================
; Load Sounding Array Data
;====================================================================================================
	ver = "2b"
	if ver.eq."2a" then ifile = "~/Data/DYNAMO/LSA/v2a/dynamo_basic_v2a_2011all.nc" end if
	if ver.eq."2b" then ifile = "~/Data/DYNAMO/LSA/v2b/dynamo_basic_v2b_2011all.nc" end if
	infile = addfile(ifile,"r")
	ps  = infile->ps(:,{ypts},{xpts})
	p   = infile->level
	p!0 = "lev"
	p&lev = p
	dp  = calc_dP(p,ps) *100.
	;u   = infile->u(:,:,{lat1:lat2},{lon1:lon2})
	;v   = infile->v(:,:,{lat1:lat2},{lon1:lon2})
	wmr = infile->q(:,:,{ypts},{xpts})
	wmr = (/wmr/1000./)
	q 	= wmr/(1.+wmr)
	copy_VarCoords(wmr,q)
	dt 	= 3*3600.
	
	;adv = calc_adv(q,u,v) *-1.
	;ddt = calc_ddt(q,dt)
	bsz = 1
	;iSATPW = block_avg( dim_avg_n( dim_sum_n(        q*dp/g,1) ,(/1,2/)) ,bsz)  ; divide by density and convert to mm

	SATPW = block_avg(  dim_sum_n(  q*dp/g,1) ,bsz) 
	aSATPW = dim_avg_n(SATPW,(/1,2/))

	;DDT = block_avg( dim_avg_n( dim_sum_n(      ddt*dp/g,1) ,(/1,2/)) ,8)
	;ADV = block_avg( dim_avg_n( dim_sum_n(      adv*dp/g,1) ,(/1,2/)) ,8)
	;LT  = block_avg( dim_avg_n( dim_sum_n((ddt-adv)*dp/g,1) ,(/1,2/)) ,8)
	SAnt = dimsizes(aSATPW)
	SAtime = tofloat(ispan(0,SAnt-1,1))/8. + 273.
	printline()



;====================================================================================================
; Load MIMIC Data
;====================================================================================================
	ifile  = "~/Data/DYNAMO/MIMIC/cimss_morphed_tpw_DYNAMO_2011.nc"
	infile = addfile(ifile,"r")
	;MInt   = dimsizes(infile->time)/6
	;tmp = infile->tpw(:,{lat1:lat2},{lon1:lon2}) 
	tmp = infile->tpw(:,{ypts},{xpts}) 
	iMITPW = new(dimsizes(tmp),float)
	iMITPW = tofloat( tmp )
	delete(tmp)
	iMITPW = tofloat( where(isnan_ieee(iMITPW),iMITPW@_FillValue,iMITPW) ) 
	bsz = 3
	MITPW = block_avg( dim_avg_n_Wrap(iMITPW,(/1,2/)) ,bsz)

	aMITPW =  MITPW
	MITPW := iMITPW
;====================================================================================================
; Create Plot 
;====================================================================================================
	tres = res
	time = SAtime
	time@units = "days since 2011-01-01"
	cdtime = cd_calendar(time,2)
	tmres = True
	tmres@ttmFormat 		= "%D %c, 2011"
	tmres@ttmAxis   		= "XB"
	tmres@ttmMinorStride	= 8
	tmres@ttmMajorStride	= 5*8
	time_axis_labels(time,tres,tmres)
	
	
		clr = (/"orange","red","green"/)
		tres@xyLineThicknessF 	= 4.
		tres@gsnCenterString   	= "TPW at Gan (0.69S, 73.16sE)"
		tres@trYMinF         	= 30.
		tres@trYMaxF        	= 74.
		tres@trXMinF			= min(time)
		tres@trXMaxF			= max(time)
		;tres@xyLineColor    	= "black"
		tres@xyDashPattern    	= 0
		tres@xyLineColors		= clr
		;tres@xyDashPatterns  	= (/0,0,0,0,0,0,0/)
		tres@tiXAxisString   	= ""
		tres@tiYAxisString   	= "[mm]"
		
		tres@vpHeightF			= 0.2
		tres@vpWidthF           = 0.8
		tres@vpXF               = 0.05
		tres@vpYF               = 0.6
		
	
	;plot(0) = gsn_csm_xy(wks,xbin,(/ECTPW,MITPW,RATPW,SATPW/),tres)
	plot(0) = gsn_csm_xy(wks,time,(/aRATPW,aSATPW,aMITPW/),tres)

	;overlay(plot(0) , gsn_csm_xy(wks,(/-1e3,1e3/),(/0.,0./),lres))
	
	overlay(plot(0) , gsn_csm_xy(wks,(/1.,1./)*15*8+273,(/-1e3,1e3/),lres))
	
	nh = 8
	xx = (/15,28/) +273.
	;xx = time((/15,28/)*nh)
	yy = (/-1e3,1e3/)
	xshade = (/xx(0),xx(0),xx(1),xx(1),xx(0)/)
	yshade = (/yy(0),yy(1),yy(1),yy(0),yy(0)/)
		gres = True
		gres@gsFillColor     	= "gray"
		gres@gsFillOpacityF		= 0.5 
	;dum = gsn_add_polygon(wks,plot(0),xshade,yshade,gres)

;====================================================================================================
; Add distribution on side of plot
;====================================================================================================
	dist = new((/3,dimsizes(xbin)/),float)
	dist(0,:) = bin_pct(RATPW,bins)
	dist(1,:) = bin_pct(SATPW,bins)
	dist(2,:) = bin_pct(MITPW,bins)

		tres := res
		tres@vpHeightF			= 0.2
		tres@vpWidthF           = 0.1
		tres@vpXF               = 0.85
		tres@vpYF               = 0.6
		
		tres@xyDashPattern    	= 0
		tres@xyLineColors		= clr
		tres@xyLineThicknessF 	= 4.
		tres@gsnCenterString   	= ""
		;tres@tiXAxisFontHeightF	= 0.01
		tres@tiXAxisString      = "[%]"
		tres@tmYLLabelsOn       = False
		tres@tmYROn             = False
		tres@trYMinF         	= 30.
		tres@trYMaxF        	= 74.

	plot(1) = gsn_csm_xy(wks,dist,xbin,tres)

;====================================================================================================
; Finalize Figure
;====================================================================================================
		pres = True
		pres@gsnFrame = False
		pres@gsnPanelXWhiteSpacePercent = 0


	;gsn_panel(wks,plot,(/1,dimsizes(plot)/),pres)
	;gsn_panel(wks,plot(0),(/1,1/),pres)

	legend = create "Legend" legendClass wks
		"lgAutoManage"              : False
		"vpXF"                      : 0.42
		"vpYF"                      : 0.47
		"vpWidthF"                  : 0.1
		"vpHeightF"                 : 0.05
		"lgPerimOn"                 : True
		"lgLabelsOn"                : True
		"lgLineLabelsOn"            : False
		"lgItemCount"               : 3
		"lgLabelStrings"            : (/"Radiometer","Soundings","MIMIC"/)
		"lgLabelFontHeightF"        : 0.01
		"lgDashIndex"             	: 0
		"lgLineThicknessF"			: 4.
		"lgLineColors"              : clr
		"lgMonoDashIndex"			: True
	end create

    draw(plot(0))
    draw(plot(1))
	draw(legend)
	frame(wks)
  
	print("")
	print(" "+fig_file+"."+fig_type)
	print("")
	if fig_type.eq."png" then system("convert -trim "+fig_file+"."+fig_type+"   "+fig_file+"."+fig_type) end if

;====================================================================================================
;====================================================================================================
end
