; Plot the Lagrangian tendency of TPW binned by TPW
; subtracts out WHOI OAflux LHF data as well as ECMWF LHF data to compare
; v4 is similar to v2, but uses precalculated TPW and CPR data
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/custom_functions_DYNAMO_LSF.ncl"
load "$NCARG_ROOT/custom_functions_DYNAMO_ECMWF.ncl"
load "$NCARG_ROOT/custom_functions.ncl"
begin

    fig_type = "png"
    fig_file = "~/Research/DYNAMO/CP/CP.bin.w-OAflux.v4"
    
    nday  = 10
    nsmp  = 1
    nWgt  = nday*nsmp*2+1
    fca   = 1./(tofloat(nday)*tofloat(nsmp))   
    
    reduceRes   = True
    dailyAvg    = False
    debug       = False
;====================================================================================================
;====================================================================================================       
    nvar = 3
    bins = True
    bins@verbose = False
    bins@bin_min = 20
    bins@bin_max = 64
    bins@bin_spc =  2
    
    ;bins@bin_min = -20
    ;bins@bin_max =  20
    ;bins@bin_spc =   1

    xbin    = ispan(bins@bin_min,bins@bin_max,bins@bin_spc)
    num_bin = dimsizes(xbin)
    bdim    = (/nvar,num_bin/)
    binval = new(bdim,float)
    bincnt = new(bdim,float)
    bincnt = 0

    sa = "lsan"
    
    if sa.eq. "lsan" then
        lat1 =   0.
        lat2 =   7.
        lon1 =  72.
        lon2 =  79.
    end if
    if sa.eq. "lsas" then
        lat1 =  -7.
        lat2 =   0.
        lon1 =  72.
        lon2 =  79.
    end if
    
    lat1 = -10.
    lat2 =  10.
    lon1 =  50.
    lon2 = 100.
    
    wks = gsn_open_wks(fig_type,fig_file)
    plot = new(1,graphic)
        res = setres_default()
        res@vpHeightF                       = 0.5
        res@tmXTOn                          = False
        res@tmXBMinorOn                     = False
        res@tmYLMinorOn                     = False
        res@tmYRMinorOn                     = False
        res@gsnLeftStringFontHeightF        = 0.01
        res@gsnCenterStringFontHeightF      = 0.01
        res@gsnRightStringFontHeightF       = 0.01
        res@gsnLeftString                   = ""
        res@gsnCenterString                 = ""
        res@gsnRightString                  = ""
        res@xyLineThicknessF                = 8.
        
        lres = res
        lres@xyDashPattern              = 0
        lres@xyLineThicknessF           = 1.
        lres@xyLineColor                = "black"
;====================================================================================================
; Load Sounding Array Data
;====================================================================================================
    printline()
    print("Loading LSA...")
    ver = "2b"
    if ver.eq."2a" then ifile = "~/Data/DYNAMO/LSA/v2a/dynamo_basic_v2a_2011all.nc" end if
    if ver.eq."2b" then ifile = "~/Data/DYNAMO/LSA/v2b/dynamo_basic_v2b_2011all.nc" end if
    infile = addfile(ifile,"r")
    SAlat = infile->lat({lat1:lat2})
    SAlon = infile->lon({lon1:lon2})
;====================================================================================================
; Load WHOI OAflux Evaporation data
;====================================================================================================   
    printline()
    print("Loading OALHF...")
    setfileoption("nc","MissingToFillValue",False) 
    infile = addfile("~/Data/DYNAMO/OAflux/evapr_oaflux_2011.nc","r")
    OAlat = infile->lat({lat1:lat2})
    OAlon = infile->lon({lon1:lon2})
    t1 = 365 - (31+30+31)
    t2 = 365 - 1

    iOALHF = tofloat( infile->evapr(t1:t2,{lat1:lat2},{lon1:lon2}) )
    OALHF = new(dimsizes(iOALHF),float)
    OALHF = where(iOALHF.ne.32766,tofloat(iOALHF*infile->evapr@scale_factor),OALHF@_FillValue)
    OALHF = (/ OALHF*10./365. /)
    OALHF@long_name     = "Evaporation"
    OALHF@units     = "mm/day"
    
    OALHF = dim_rmvmean_n_Wrap(OALHF,0)
    
    setfileoption("nc","MissingToFillValue",True) 
;====================================================================================================
; Load ECMWF LHF
;====================================================================================================   
if True then
    printline()
    print("Loading ECLHF...")
    infile = addfile("~/Data/DYNAMO/ECMWF/data/12hour.00-12.DYNAMO.ECMWF.LHF.nc","r")
    EClat = infile->lat({lat1:lat2})
    EClon = infile->lon({lon1:lon2})
    t1 = 365 - (31+30+31)
    t2 = 365 - 1

    ECLHF = block_avg( infile->LHF(:,{lat1:lat2},{lon1:lon2}) ,2)
    ECLHF = (/ECLHF/Lv*86400*-1./)
    ECLHF@long_name     = "Evaporation"
    ECLHF@units         = "mm/day"
    
    ECLHF = dim_rmvmean_n_Wrap(ECLHF,0)
    
    
        if False then
        print(infile)
        printline()
        printMAM(OALHF)
        printMAM(ECLHF)
        L1 = dim_avg_n(OALHF,(/1,2/))
        L2 = dim_avg_n(ECLHF,(/1,2/))
        print((L2/L1))
        exit
        end if
    
    if reduceRes then
        iECLHF = ECLHF
        ilat = ECLHF&lat
        ilon = ECLHF&lon
        delete([/ECLHF/])
        ECLHF = area_hi2lores_Wrap(ilon,ilat,iECLHF ,False,1.,SAlon,SAlat,False)
        delete([/iECLHF,EClat,EClon/])
        EClat = ECLHF&lat
        EClon = ECLHF&lon
    end if
    
    tmp = ECLHF
    delete(ECLHF)
    ECLHF = area_hi2lores_Wrap(EClon,EClat,tmp ,False,1.,OAlon,OAlat,False) 
    delete(tmp)
    
end if
;====================================================================================================
; Load ECMWF Data
;====================================================================================================
    if reduceRes then
        ifile = "~/Data/DYNAMO/ECMWF/data/6hour.DYNAMO.ECMWF.CP.SAres.nc"
    else
        ifile = "~/Data/DYNAMO/ECMWF/data/6hour.DYNAMO.ECMWF.CP.nc"
    end if
    infile = addfile(ifile,"r")

    ECTPW = infile->ECTPW(:,{lat1:lat2},{lon1:lon2})
    ECCPR = infile->ECCPR(:,{lat1:lat2},{lon1:lon2}) 
    
    printline()
    printMAM(ECCPR)
    
    printline()
    printVarSummary(ECCPR)

    printline()
    tmp = ECCPR
    delete(ECCPR)
    ECCPR = block_avg( area_hi2lores_Wrap(EClon,EClat,tmp ,False,1.,OAlon,OAlat,False) ,4)
    delete(tmp)
    
    tmp = ECTPW
    delete(ECTPW)
    ECTPW = block_avg( area_hi2lores_Wrap(EClon,EClat,tmp ,False,1.,OAlon,OAlat,False) ,4)
    delete(tmp)
    
    printline()
    printVarSummary(ECCPR)
    printline()
    printVarSummary(ECLHF)
    printline()
    printVarSummary(OALHF)
    printline()
;====================================================================================================
; Create Plot 
;====================================================================================================

    Rdim = (/nvar/)
    Rcof = new(Rdim,float)  ; REgression Coefficient
    Rdof = new(Rdim,float)  ; Degrees of Freedom
    Ryin = new(Rdim,float)  ; Y-intercept
    Rstd = new(Rdim,float)  ; Standard Error
    Rtst = new(Rdim,float)  ; t-statistic

    do v = 0,nvar-1
        print("    v = "+v+"    v%nvar = "+v%nvar)
        
        if v.eq.0 then Vy = ECCPR  end if
        if v.eq.1 then Vy = ECCPR - OALHF  end if
        if v.eq.2 then Vy = ECCPR - ECLHF end if
        
        if v.eq.0 then Vx = ECTPW end if
        if v.eq.1 then Vx = ECTPW end if
        if v.eq.2 then Vx = ECTPW end if
        
        ; Daily Average
        if dailyAvg then
            tVx = Vx
            tVy = Vy
            delete([/Vx,Vy/])
            Vx = block_avg(tVx,4)
            Vy = block_avg(tVy,4)
            delete([/tVx,tVy/])
        end if
        ;---------------------------------------
        ; Binning
        ;---------------------------------------
        tmp = bin_YbyX(Vy,Vx,bins)
        binval(v,:) = tmp
        bincnt(v,:) = tmp@pct
        delete(tmp)
        ;---------------------------------------
        ; Regression
        ;---------------------------------------
        rVx = ndtooned(Vx)
        rVy = ndtooned(Vy)
        tmp = regline(rVx,rVy)
        Rcof(v) = tmp
        Rdof(v) = tmp@nptxy/2-2
        Ryin(v) = tmp@yintercept
        Rstd(v) = tmp@rstd
        Rtst(v) = tmp@tval
        
        yerr = Vy - ( Vx*Rcof(v) + Ryin(v) )
        xerr = Vx - avg(Vx)
        Rstd(v) = sqrt( sum(yerr^2.)/(sum(xerr^2.)*Rdof(v)) )

        delete([/tmp,rVx,rVy,yerr,xerr/])
        ;---------------------------------------
        ;---------------------------------------
        delete([/Vy,Vx/])
        
        if v.ne.0 then bincnt(v,:) = bincnt(0,:) end if
    end do
    
    print(bincnt(0,:))
    printline()
;====================================================================================================
;====================================================================================================
        ;clr = (/"black","red","blue","black","red","blue"/)
        ;dsh = (/0,0,0,1,1,1/)
        clr = (/"blue","cyan","tan"/)
        dsh = (/0,0,0,0/)
        tres = res
        tres@xyLineThicknessF   = 8.
        ;tres@gsnCenterString    = "ECMWF Lagrangian Tendency Binned by TPW" ;+" ()"
        tres@tiXAxisString      = "CWV [mm]"
        tres@tiYAxisString      = "[mm day~S~-1~N~]"

        
        dres = tres
        dres@xyLineColor        = "black"
        dres@gsnCenterString    = ""
        dres@tiYAxisString      = "[% of Obsrevations]"
        dres@xyDashPattern      = 2
        dres@xyLineThicknessF   = 4.
        
        tres@xyLineColors       = clr
        tres@xyDashPatterns     = dsh
        
        tres@trXMinF            = bins@bin_min
        tres@trXMaxF            = bins@bin_max
        ;tres@trYMinF            = 0.0
        tres@trYMaxF            = 5.0
        
    plot(0) = gsn_csm_xy2(wks,xbin,binval,bincnt,tres,dres)
    
    
    ;-----------------------------------------------------------------
    ; Add regression lines
    ;-----------------------------------------------------------------
        rres = res
        rres@xyLineColors       = clr
        rres@xyLineThicknessF   = 4.
        rres@xyDashPatterns     = dsh
    reg = new((/nvar,num_bin/),float)
    reg = conform(reg,Rcof,0) * conform(reg,xbin,1) + conform(reg,Ryin,0)
    ;overlay(plot(0) , gsn_csm_xy(wks,xbin,reg,rres))
    ;-----------------------------------------------------------------
    ; add horz and vert lines
    ;-----------------------------------------------------------------
    overlay(plot(0) , gsn_csm_xy(wks,(/-1e3,1e3/),(/0.,0./),lres))

    ;overlay(plot(0) , gsn_csm_xy(wks,(/1.,1./)*avg(ECTPW),(/-1e3,1e3/),lres))
    ;overlay(plot(0) , gsn_csm_xy(wks,(/1.,1./)*avg(SATPW),(/-1e3,1e3/),lres))
    
;====================================================================================================
; Finalize Figure
;====================================================================================================
        pres = True
        pres@gsnFrame                           = False

    gsn_panel(wks,plot,(/dimsizes(plot),1/),pres)

    legend = create "Legend" legendClass wks
        "lgAutoManage"              : False
        "vpXF"                      : 0.2
        "vpYF"                      : 0.8
        "vpWidthF"                  : 0.1
        "vpHeightF"                 : 0.08
        "lgPerimOn"                 : True
        "lgLabelsOn"                : True
        "lgLineLabelsOn"            : False
        "lgItemCount"               : 3
        "lgLabelStrings"            : (/"LCT-LHF (EC)","LCT-LHF (WHOI)","LCT"/)
        "lgLabelFontHeightF"        : 0.015
        "lgDashIndex"               : 0
        "lgLineThicknessF"          : 8.
        "lgLineColors"              : clr(::-1)
        "lgMonoDashIndex"           : True
    end create

    draw(legend)
    frame(wks)
  
    print("")
    print(" "+fig_file+"."+fig_type)
    print("")
    if fig_type.eq."png" then system("convert -trim "+fig_file+"."+fig_type+"   "+fig_file+"."+fig_type) end if

;====================================================================================================
;====================================================================================================
end
