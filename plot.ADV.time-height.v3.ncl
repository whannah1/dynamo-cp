; similar to v1, but plots the wind and gradient variables separately
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/contrib/time_axis_labels.ncl"
load "$NCARG_ROOT/custom_functions_DYNAMO_ECMWF.ncl"
load "$NCARG_ROOT/custom_functions.ncl"
begin
    
    fig_type = "png"
    fig_file = "~/Research/DYNAMO/CP/ADV.time-height.v3"

    sa = "lsan"
    
    daily = True
    
    nsmooth = 0

;====================================================================================================
;====================================================================================================
    if sa.eq."lsan" then
        lat1 =  -1.
        lat2 =   8.
        lon1 =  72.
        lon2 =  81.
    end if
    if sa.eq."lsas" then
        lat1 = -8.
        lat2 = -1.
        lon1 =  72.
        lon2 =  81.
    end if

    lev1 = 500
    lev2 = 1000
;====================================================================================================
; Load ECMWF
;====================================================================================================
    infile = addfile("~/Data/DYNAMO/ECMWF/data/6hour.DYNAMO.ECMWF.U.nc","r")
    u = infile->U(:,{lev1:lev2},{lat1:lat2},{lon1:lon2})
    infile = addfile("~/Data/DYNAMO/ECMWF/data/6hour.DYNAMO.ECMWF.V.nc","r")
    v = infile->V(:,{lev1:lev2},{lat1:lat2},{lon1:lon2})
    infile = addfile("~/Data/DYNAMO/ECMWF/data/6hour.DYNAMO.ECMWF.Q.nc","r")
    q = infile->Q(:,{lev1:lev2},{lat1:lat2},{lon1:lon2})
    
    EClev = q&lev
    
    dt  = 6*3600.
    adv = calc_adv(q,u,v) *-1.
    ;ddt = calc_ddt(q,dt)
    ;cpr = (ddt-adv)
    
    ;ECDDT = block_avg( dim_avg_n( ddt ,(/2,3/)) ,4)
    ECADV = block_avg( dim_avg_n( adv ,(/2,3/)) ,4)
    ;ECCPR = block_avg( dim_avg_n( cpr ,(/2,3/)) ,4)
    
    ECADV = (/ECADV *86400.*1e3/)
    ;ECDDT = (/ECDDT *86400.*1e3/)
    ;ECCPR = (/ECCPR *86400.*1e3/)

    mlev = 800
    mu = conform(u,u(:,{mlev},:,:),(/0,2,3/))
    mv = conform(v,v(:,{mlev},:,:),(/0,2,3/))
    madv = calc_adv(q,mu,mv) *-1.
    mECADV = block_avg( dim_avg_n( madv ,(/2,3/)) ,4)
    mECADV = (/mECADV *86400.*1e3/)
    

    dqdx = calc_ddx(q)
    dqdy = calc_ddy(q)

    dqdx = (/dqdx*1e3/)
    dqdy = (/dqdy*1e3/)

    udqdx = u*dqdx * -1 *86400. 
    vdqdy = v*dqdy * -1 *86400. 

    adqdx := block_avg( dim_avg_n( dqdx ,(/2,3/)) ,4)
    adqdy := block_avg( dim_avg_n( dqdy ,(/2,3/)) ,4)
    au := block_avg( dim_avg_n( u ,(/2,3/)) ,4)
    av := block_avg( dim_avg_n( v ,(/2,3/)) ,4)
    audqdx := block_avg( dim_avg_n( udqdx ,(/2,3/)) ,4)
    avdqdy := block_avg( dim_avg_n( vdqdy ,(/2,3/)) ,4)

    do n = 0,nsmooth-1 ECCPR = wgt_runave_n_Wrap(ECCPR, (/0.25,0.5,0.25/), 0,0) end do
    do n = 0,nsmooth-1 ECDDT = wgt_runave_n_Wrap(ECDDT, (/0.25,0.5,0.25/), 0,0) end do
    do n = 0,nsmooth-1 ECADV = wgt_runave_n_Wrap(ECADV, (/0.25,0.5,0.25/), 0,0) end do
    
    ECADV!0 = "time"
    ECADV!1 = "lev"
    ECADV&lev = EClev

    copy_VarCoords(ECADV,adqdx)
    copy_VarCoords(ECADV,adqdy)
    copy_VarCoords(ECADV,au)
    copy_VarCoords(ECADV,av)
    copy_VarCoords(ECADV,audqdx)
    copy_VarCoords(ECADV,avdqdy)

    copy_VarCoords(ECADV,mECADV)
    
    delete([/u,v,q,dt,adv/])
    ;delete([/ddt,cpr/])
;====================================================================================================
; Plot Map
;====================================================================================================\
    wks = gsn_open_wks(fig_type,fig_file)
    ;gsn_define_colormap(wks,"WhiteBlueGreenYellowRed")
    gsn_define_colormap(wks,"ncl_default")
    plot = new(2,graphic)
        res = True
        res@gsnDraw                         = False
        res@gsnFrame                        = False
        res@vpHeightF                       = 0.3
        res@tmXTOn                          = False
        res@gsnLeftStringFontHeightF        = 0.015
        res@gsnCenterStringFontHeightF      = 0.015
        res@gsnRightStringFontHeightF       = 0.015
        res@tmXBLabelFontHeightF            = 0.01
        res@tmYLLabelFontHeightF            = 0.01
        res@tiXAxisFontHeightF              = 0.015
        res@tiYAxisFontHeightF              = 0.015
        res@tmXBMajorOutwardLengthF         = 0.0
        res@tmXBMinorOutwardLengthF         = 0.0
        res@tmYLMajorOutwardLengthF         = 0.0
        res@tmYLMinorOutwardLengthF         = 0.0
        res@gsnLeftString                   = ""
        res@gsnCenterString                 = ""
        res@gsnRightString                  = ""
        res@tmXBLabelAngleF                 = -45.
        res@lbTitlePosition                 = "bottom"
        res@lbTitleFontHeightF              = 0.015
        res@lbLabelFontHeightF              = 0.015
        ;res@lbTitleString                   = "[g kg~S~-1~N~ day~S~-1~N~]"
        res@gsnYAxisIrregular2Linear        = True
        
        lres = res
        lres@xyDashPattern              = 0
        lres@xyLineThicknessF           = 1.
        lres@xyLineColor                = "black"
        
        time = ispan(0,dimsizes(ECADV(:,0))-1,1)
        time@units = "days since 2011-10-01"
        cdtime = cd_calendar(time,2)
        tmres = True
        tmres@ttmFormat         = "%D%c"
        tmres@ttmAxis           = "XB"
        tmres@ttmMinorStride    = 1
        tmres@ttmMajorStride    = 5
        time_axis_labels(time,res,tmres)


        tres = res
        tres@trYReverse             = True
        tres@cnFillOn               = True
        tres@cnLinesOn              = False
        tres@gsnSpreadColors        = True
        tres@tiYAxisString          = "[hPa]"
        tres@cnLevelSelectionMode   = "ExplicitLevels"
        ;tres@cnLevels               = ispan(-20,20,2)/1e1
        tres@gsnRightString         = "";Advective Tendency"

    res@lbTitleString  = "[g kg~S~-1~N~ day~S~-1~N~]"
    tres@cnLevels     := ispan(-20,20,2)/1e1
    tres@gsnLeftString = "Advective Tendency (full)"
    plot(0) = gsn_csm_contour(wks,ECADV(lev|:,time|:),tres)
    tres@gsnLeftString = "Advective Tendency (steering flow)"
    plot(1) = gsn_csm_contour(wks,mECADV(lev|:,time|:),tres)

    ;overlay(plot(0) , gsn_csm_xy(wks,(/-1e3,1e3/),(/0.,0./),lres))

;====================================================================================================
; Finalize Figure
;====================================================================================================
    
    gsn_panel(wks,plot,(/2,1/),setres_panel_labeled())

    trimPNG(fig_file)

;====================================================================================================
;====================================================================================================
end
