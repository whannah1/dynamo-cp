; plots TRMM data within the joint distribution of CWV and CLT
; from MIMIC data based on an 800hPa steering flow
; MIMIC data processed by mk.joint.bin.MIMIC.TRMM.v1.ncl
; This plotting script was initally based on plot.joint.bin.TRMM.v3.ncl
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/custom_functions.ncl"
begin

    ver = 2

    fig_type = "png"
    fig_file = "~/Research/DYNAMO/CP/joint.bin.MIMIC.TRMM.v1."+ver
    
    reduceRes   = False
    dailyAvg    = False
    avgFlag     = False
    debug       = False

    nvar = 2

    lat1 = -10
    lat2 =  10
    lon1 =  50
    lon2 = 100

;====================================================================================================
;====================================================================================================
        res = setres_default()
        res@vpHeightF                       = 0.5
        res@gsnLeftStringFontHeightF        = 0.025
        res@gsnCenterStringFontHeightF      = 0.015
        res@gsnRightStringFontHeightF       = 0.015
        res@gsnLeftString                   = ""
        res@gsnCenterString                 = ""
        res@gsnRightString                  = ""
        
        lres = res
        lres@xyDashPattern              = 0
        lres@xyLineThicknessF           = 1.
        lres@xyLineColor                = "black"
;====================================================================================================
;====================================================================================================
    ifile = "~/Data/DYNAMO/CP/joint.bin.MIMIC.TRMM.binmap.nc"
    infile = addfile(ifile,"r")

    binmap = infile->binmap
    binpct = infile->bincnt

printline()
printMAM(binpct)
    
    Yavg = infile->Yavg
    Xavg = infile->Xavg
    
    binsx = infile->binsx
    binsy = infile->binsy
    
    lat1 = infile->lat1
    lat2 = infile->lat2
    lon1 = infile->lon1
    lon2 = infile->lon2
;====================================================================================================
; Load MIMIC Data
;====================================================================================================   
    ifile = "~/Data/DYNAMO/MIMIC/6hour.DYNAMO.MIMIC.CP.nc"
    infile = addfile(ifile,"r")
    MIlat = infile->lat({lat1:lat2})
    MIlon = infile->lon({lon1:lon2})
    MITPW = infile->MICPR(:,{lat1:lat2},{lon1:lon2})
    MICPR = infile->MICPR(:,{lat1:lat2},{lon1:lon2})

    MItime = ispan(0,92*24-6,6)
    MItime@units = "hours after 2011-10-01 00:00:00"

    num_t   = dimsizes(MItime)
    num_lat = dimsizes(MIlat)
    num_lon = dimsizes(MIlon)

    MICPR&time = MItime
    MITPW&time = MItime
;====================================================================================================
; Load TRMM orbital data
;====================================================================================================
    ifile = "~/Data/DYNAMO/TRMM/orbital/TRMM.DYNAMO.PRdata.2011.50E_100E_10S_10N.v2.nc"
    infile = addfile(ifile,"r")

    t1 = 0
    if debug then 
        t2 = toint( 1000 )
    else
        t2 = dimsizes(infile->time)-1
    end if

    TRlat  = infile->lat        (t1:t2)
    TRlon  = infile->lon        (t1:t2)
    TRtime = infile->time       (t1:t2)
    TRrain = infile->rainSfc    (t1:t2)
    
    ;numRain  = infile->numRain    (t1:t2)
    ;numConv  = infile->numConv    (t1:t2)
    numStrat = infile->numStrat   (t1:t2)

    cluster1 = new(dimsizes(TRtime),float)
    cluster4 = new(dimsizes(TRtime),float)

    cluster1  = infile->cluster1    (t1:t2)
    cluster4  = infile->cluster4    (t1:t2)

    dt = ( MItime(1) - MItime(0) )/2.
    do t = 0,dimsizes(MItime)-1
        condition = TRtime.gt.(MItime(t)-dt) .and. TRtime.le.(MItime(t)+dt)
        TRtime = where(condition,MItime(t),TRtime)
        delete(condition)
    end do

    TRrain@lat  = TRlat
    TRrain@lon  = TRlon
    TRrain@time = TRtime

    copy_VarAtts(TRrain,cluster1)
    copy_VarAtts(TRrain,cluster4)   
;====================================================================================================
; Bin data
;====================================================================================================
    nbiny  = toint( ( binsy@bin_max - binsy@bin_min + binsy@bin_spc )/binsy@bin_spc )
    nbinx  = toint( ( binsx@bin_max - binsx@bin_min + binsx@bin_spc )/binsx@bin_spc )
    biny   = fspan(binsy@bin_min,binsy@bin_max,nbiny)
    binx   = fspan(binsx@bin_min,binsx@bin_max,nbinx)
    bdim   = (/nvar,nbiny,nbinx/)
    binval = new(bdim,float)
    bincnt = new(bdim,float)
    
    name = new(nvar,string)

    do v = 0,nvar-1
    
        print("    v = "+v+"    v%nvar = "+v%nvar)

        Vz = new(dimsizes(TRtime),float)        ; This is to supress "_FillValue" warnings

        ; 4-panel of main cloud clusters
        if ver.eq.1 then
            if v.eq.0 then Vz = where(cluster1.eq.1,1.,0.)  end if
            if v.eq.1 then Vz = where(cluster1.eq.2,1.,0.)  end if
            if v.eq.0 then name(v) = "cluster class: shallow and congestus" end if
            if v.eq.1 then name(v) = "cluster class: congestus plus some deep" end if
        end if

        if ver.eq.2 then
            if v.eq.0 then Vz = where(cluster1.eq.0,1.,0.)  end if
            if v.eq.1 then Vz = where(cluster1.eq.4,1.,0.)  end if
            if v.eq.0 then name(v) = "cluster class: non-raining" end if
            if v.eq.1 then name(v) = "cluster class: stratiform" end if
        end if

        ; comparison of 0.25 and 1 degree cluster domains
        if ver.eq.3 then
            if v.eq.0 then Vz = cluster1 end if
            if v.eq.1 then Vz = cluster4 end if
            Vz = where(Vz.eq.0,1.,0.)
            if v.eq.0 then name(v) =  "1.0-deg cluster class: non-raining" end if
            if v.eq.1 then name(v) = "0.25-deg cluster class: deep" end if
        end if

        do by = 0,nbiny-1
        do bx = 0,nbinx-1
            binid   = toint( by*1e3 + bx )
            condition = binmap.eq.binid
            bincnt(v,by,bx) = num(condition)
            binval(v,by,bx) = avg( where(condition,Vz,Vz@_FillValue) ) 
        end do
        end do
        delete([/Vz/])
    end do
    
    bincnt := bincnt(0,:,:)
    printMAM(bincnt)
    bincnt = 100.* tofloat(bincnt) / tofloat(product(dimsizes(cluster1)))
    printMAM(bincnt)

    printline()

    if any(ver.eq.(/1,2,3/)) then
        binval = binval*100.
    end if
    
    binval!0 = "var"
    binval!1 = "ybin"
    binval!2 = "xbin"
    binval&ybin = biny
    binval&xbin = binx
    copy_VarCoords(binval(0,:,:),bincnt)
;====================================================================================================
;====================================================================================================
    wks = gsn_open_wks(fig_type,fig_file)
    ;plot = new(nvar*2,graphic)
    plot = new(nvar,graphic)
        cres = res
        cres@cnFillOn           = True
        cres@cnLinesOn          = False
        cres@cnInfoLabelOn      = False
        cres@cnFillPalette      = "CBR_wet"
        cres@cnFillPalette      = "GMT_drywet"
        cres@cnFillPalette      = "MPL_cool"
        ;cres@cnFillPalette     = ""
        cres@lbOrientation      = "vertical"
        ;cres@cnFillMode         = "RasterFill"
        cres@cnFillMode         = "CellFill"
        cres@tiYAxisString      = "Lagrangian Tendency [mm day~S~-1~N~]"
        cres@tiXAxisString      = "TPW [mm]"

        tres = cres
        tres@cnFillOn           = False
        tres@cnLinesOn          = True
        tres@cnLineThicknessF   = 3.
        tres@gsnContourZeroLineThicknessF   = 6.
        tres@gsnContourNegLineDashPattern   = 1
        
        ;cres@cnLevelSelectionMode  = "ExplicitLevels"
        ;cres@cnLevels               = ispan(5,95,5)/1e2
        ;cres@lbLabelStride          = 2

        if any(ver.eq.(/1,2,3/)) then
            cres@lbTitleString = "%"
        end if

        
    do v = 0,nvar-1
        p1 = v
        p2 = v+nvar
        
        cres@gsnLeftString = name(v)

        plot(v) = gsn_csm_contour(wks,binval(v,:,:),cres)
        
        overlay(plot(v) , gsn_csm_contour(wks,bincnt,tres))
        
        overlay(plot(v) , gsn_csm_xy(wks,(/-1e3,1e3/),(/0.,0./),lres))
        overlay(plot(v) , gsn_csm_xy(wks,(/1.,1./)*Xavg,(/-1e3,1e3/),lres)) 
        
        ;if v.eq.0 then 
        ;   overlay(plot(v) , gsn_csm_xy(wks,(/1.,1./)*avg(MICPR),(/-1e3,1e3/),lres)) 
        ;end if
    end do

    
;====================================================================================================
; Finalize Figure
;====================================================================================================
        pres = setres_panel_labeled()
        pres@gsnPanelYWhiteSpacePercent = 5

    if nvar.eq.4 then
        gsn_panel(wks,plot,(/2,2/),pres)
    end if
    
    if any(nvar.eq.(/5,6/)) then
        gsn_panel(wks,plot,(/3,2/),pres)
    end if
    
    if all(nvar.ne.(/4,5,6/)) then
       gsn_panel(wks,plot,(/1,dimsizes(plot)/),pres)
       ;gsn_panel(wks,plot,(/dimsizes(plot),1/),pres)
    end if

    trimPNG(fig_file)
;====================================================================================================
;====================================================================================================
end
