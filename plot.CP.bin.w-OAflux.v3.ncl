; plots the Lagrangian tendency binned by TPW
; v3 - add the linear regression estimate to see if trend is significant
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/custom_functions_DYNAMO_LSF.ncl"
load "$NCARG_ROOT/custom_functions_DYNAMO_ECMWF.ncl"
load "$NCARG_ROOT/custom_functions.ncl"
begin

	fig_type = "png"
	fig_file = "~/Research/DYNAMO/CP/CP.bin.w-OAflux.v3"
	
	reduceRes	= False
	
	dailyAvg	= False
	
	debug 		= False
	
	recalc		= False
;====================================================================================================
;====================================================================================================		
	lat1 =   0.
	lat2 =   7.
	lon1 =  72.
	lon2 =  79.
	
	lat1 = -10.
	lat2 =  10.
	lon1 =  50.
	lon2 = 100.
    
    
	fig_type@wkWidth  = 2048
	fig_type@wkHeight = 2048
	
	wks = gsn_open_wks(fig_type,fig_file)
	plot = new(1,graphic)
		res = True
		res@gsnDraw                         = False
		res@gsnFrame                        = False
		res@vpHeightF						= 0.5
		res@tmXTOn                          = False
		res@tmXBMinorOn						= False
		res@tmYLMinorOn						= False
		res@tmYRMinorOn						= False
		res@gsnLeftStringFontHeightF        = 0.01
		res@gsnCenterStringFontHeightF    	= 0.01
		res@gsnRightStringFontHeightF       = 0.01
		res@tmXBLabelFontHeightF            = 0.01
		res@tmYLLabelFontHeightF            = 0.01
		res@tiXAxisFontHeightF				= 0.01
		res@tiYAxisFontHeightF				= 0.01
		res@tmXBMajorOutwardLengthF         = 0.0
		res@tmXBMinorOutwardLengthF         = 0.0
		res@tmYLMajorOutwardLengthF         = 0.0
		res@tmYLMinorOutwardLengthF         = 0.0
		res@gsnLeftString                   = ""
		res@gsnCenterString              	= ""
		res@gsnRightString                  = ""
		;res@tmXBLabelAngleF					= -50.
		
		lres = res
		lres@xyDashPattern              = 0
		lres@xyLineThicknessF           = 1.
		lres@xyLineColor                = "black"
		
bsz = 2
k1 = 0
dt = 3*3600.*bsz
;====================================================================================================
; Load WHOI OAflux Evaporation data
;====================================================================================================	
	printline()
	print("Loading OALHF...")
	setfileoption("nc","MissingToFillValue",False) 
	infile = addfile("~/Data/DYNAMO/OAflux/evapr_oaflux_2011.nc","r")
	OAlat = infile->lat({lat1:lat2})
	OAlon = infile->lon({lon1:lon2})
	t1 = 365 - (31+30+31)
	t2 = 365 - 1

	iOALHF = tofloat( infile->evapr(t1:t2,{lat1:lat2},{lon1:lon2}) )
	OALHF = new(dimsizes(iOALHF),float)
	OALHF = where(iOALHF.ne.32766,tofloat(iOALHF*infile->evapr@scale_factor),OALHF@_FillValue)
	OALHF = (/ OALHF*10./365. /)
	OALHF@long_name 	= "Evaporation"
	OALHF@units		= "mm/day"
	
	printline()
	printMAM(OALHF)
	
	OALHF = dim_rmvmean_n_Wrap(OALHF,0)
	
	printline()
	printMAM(OALHF)
	
	printline()
	printVarSummary(OALHF)
	printMAM(OALHF)
	
	setfileoption("nc","MissingToFillValue",True) 
;====================================================================================================
; Load ECMWF Data
;====================================================================================================	
if True then
	printline()
	print("Loading ECMWF...")
	tfile = "~/Data/DYNAMO/ECMWF/data/6hour.DYNAMO.ECMWF.CP.nc"
	if recalc then
		t2 = (31+30+31)*4-1
		if debug then t2 = 10*4 end if
		infile = addfile("~/Data/DYNAMO/ECMWF/data/6hour.DYNAMO.ECMWF.U.nc","r")
		u = infile->U(:t2,:,{lat1:lat2},{lon1:lon2})
		infile = addfile("~/Data/DYNAMO/ECMWF/data/6hour.DYNAMO.ECMWF.V.nc","r")
		v = infile->V(:t2,:,{lat1:lat2},{lon1:lon2})
		infile = addfile("~/Data/DYNAMO/ECMWF/data/6hour.DYNAMO.ECMWF.Q.nc","r")
		q = infile->Q(:t2,:,{lat1:lat2},{lon1:lon2})
	
		infile = addfile("~/Data/DYNAMO/ECMWF/data/6hour.DYNAMO.ECMWF.Ps.nc","r")
		ps = infile->Ps(:t2,{lat1:lat2},{lon1:lon2})	
		p = q&lev*100.
		p@units = "Pa"
		p!0 = "lev"
		p&lev = p
		dp = calc_dP(p,ps)

		ifile = "~/Data/DYNAMO/ECMWF/data/6hour.DYNAMO.ECMWF.CWV.nc"
		infile = addfile(ifile,"r")
		ECTPW = infile->CWV(:t2,{lat1:lat2},{lon1:lon2}) 
	
		if reduceRes then
			iECTPW = ECTPW
			ilat = q&lat
			ilon = q&lon
			iq = q
			iu = u
			iv = v
			idp = dp
			delete([/u,v,q,dp,ECTPW/])
			q  = area_hi2lores_Wrap(ilon,ilat,iq ,False,1.,SAlon,SAlat,False)
			u  = area_hi2lores_Wrap(ilon,ilat,iu ,False,1.,SAlon,SAlat,False)
			v  = area_hi2lores_Wrap(ilon,ilat,iv ,False,1.,SAlon,SAlat,False)
			dp = area_hi2lores_Wrap(ilon,ilat,idp,False,1.,SAlon,SAlat,False)
			ECTPW = area_hi2lores_Wrap(ilon,ilat,iECTPW ,False,1.,SAlon,SAlat,False)
			delete([/iu,iv,iq,idp,iECTPW/])
		end if
	
		iECADV = calc_adv(q,u,v) *-1.*86400.
		dt 	= 6*3600.
		iECDDT = calc_ddt(q,dt)*86400.
	
		ECDDT = dim_sum_n(iECDDT*dp/g,1)
		ECADV = dim_sum_n(iECADV*dp/g,1)
		ECCPR = ECDDT - ECADV
	
		delete([/q,u,v,iECDDT,iECADV,p,ps,dp/])
	
		if isfilepresent(tfile) then system("rm "+tfile) end if
		outfile = addfile(tfile,"c")
		outfile->ECTPW = ECTPW
		outfile->ECDDT = ECDDT
		outfile->ECCPR = ECCPR
	else
		infile = addfile(tfile,"r")
		ECTPW = infile->ECTPW
		ECDDT = infile->ECDDT
		ECCPR = infile->ECCPR
	end if
	
	copy_VarCoords(ECTPW,ECDDT)
	copy_VarCoords(ECTPW,ECCPR)
	
	EClat = ECCPR&lat
	EClon = ECCPR&lon
	
	tmp = ECCPR
	delete(ECCPR)
	ECCPR = block_avg( area_hi2lores_Wrap(EClon,EClat,tmp ,False,1.,OAlon,OAlat,False) ,4)
	delete(tmp)
	
	tmp = ECTPW
	delete(ECTPW)
	ECTPW = block_avg( area_hi2lores_Wrap(EClon,EClat,tmp ,False,1.,OAlon,OAlat,False) ,4)
	delete(tmp)
	
	printline()
	printMAM(ECCPR)
	printline()
	printVarSummary(ECCPR)
	printline()
	
end if
;====================================================================================================
; Create Plot 
;====================================================================================================

    nvar = 2
    
	bins = True
	bins@verbose = False
    bins@bin_min = 24
    bins@bin_max = 64
    bins@bin_spc =  2

	xbin 	= ispan(bins@bin_min,bins@bin_max,bins@bin_spc)
    num_bin = dimsizes(xbin)
    bdim 	= (/nvar,num_bin/)
    binval  = new(bdim,float)
    binpct  = new(bdim,float)
    
    Rdim = (/nvar/)
	Rcof = new(Rdim,float)	; REgression Coefficient
	Rdof = new(Rdim,float)	; Degrees of Freedom
	Ryin = new(Rdim,float)	; Y-intercept
	Rstd = new(Rdim,float)	; Standard Error
	Rtst = new(Rdim,float)	; t-statistic
    
    do v = 0,nvar-1
        print("    v = "+v+"    v%nvar = "+v%nvar)
		
		if v.eq.0 then Vy = ECCPR  end if
		if v.eq.1 then Vy = ECCPR - OALHF  end if
		
		if v.eq.0 then Vx = ECTPW end if
		if v.eq.1 then Vx = ECTPW end if
		if v.eq.2 then Vx = ECTPW end if
		
        ;---------------------------------------
        ; Binning
        ;---------------------------------------
		tmp = bin_YbyX(Vy,Vx,bins)
        binval(v,:) = tmp
        binpct(v,:) = tmp@pct
        delete([/tmp/])
        ;---------------------------------------
        ; Regression
        ;---------------------------------------
		rVx = ndtooned(Vx)
		rVy = ndtooned(Vy)
		tmp = regline(rVx,rVy)
		Rcof(v) = tmp
		Rdof(v) = tmp@nptxy/2-2
		;Rdof(v) = product(dimsizes(rVx))-1
		Ryin(v) = tmp@yintercept
		Rstd(v) = tmp@rstd
		Rtst(v) = tmp@tval
		
		yerr = Vy - ( Vx*Rcof(v) + Ryin(v) )
		xerr = Vx - avg(Vx)
		Rstd(v) = sqrt( sum(yerr^2.)/(sum(xerr^2.)*Rdof(v)) )
    
		;tdof = 1./( Rstd(v)^2./(sum(yerr^2.)/sum(xerr^2.)) )
		;print("		"+tdof+"		"+Rdof(v))
    
    	;Rtst(v) = Rcof(v) / Rstd(v)
    
		delete([/tmp,rVx,rVy,yerr,xerr/])
        ;---------------------------------------
        ;---------------------------------------
        ;if v.ne.0 then binpct(v,:) = binpct(0,:) end if
        delete([/Vy,Vx/])
    end do
    
    printline()
    tval = 1.960	
    print(Rdof+"		"+Rstd+"		"+Rtst+"		("+tval+")")
    printline()

;====================================================================================================
;====================================================================================================
		;clr = (/"blue","red","orange","red","blue","red"/)
		;dsh = (/0,0,2,2,3,3/)
		;clr = (/"blue","red","orange","red","blue","green"/)
		clr = (/"blue","cyan"/)
		dsh = (/0,0,0/)
		tres = res
		tres@xyLineThicknessF 	= 4.
		tres@gsnCenterString   	= "Lagrangian Tendency Binned by CWV"
		tres@tiXAxisString   	= "[mm]"
		tres@tiYAxisString   	= "[mm day~S~-1~N~]"
		
		
		dres = tres
		;dres@xyLineColor		= "black"
		dres@gsnCenterString	= ""
		dres@tiYAxisString   	= "[% of Obsrevations]"
		dres@xyDashPattern 		= 1
		dres@xyLineThicknessF	= 1.
		dres@xyLineColors		= clr
		
		tres@xyDashPatterns    	= dsh
		tres@xyLineColors		= clr
		;tres@xyDashPatterns 	= (/0,1,2/)
		;tres@trYMinF         	= -2.
		;tres@trYMaxF        	=  8.
		tres@trXMinF         	= bins@bin_min
		tres@trXMaxF        	= bins@bin_max
		
	plot(0) = gsn_csm_xy2(wks,xbin,binval,binpct,tres,dres)
	
		rres = res
		rres@xyLineColors		= clr
		rres@xyLineThicknessF	= 1.
		rres@xyDashPatterns    	= dsh
	reg = new((/nvar,num_bin/),float)
	reg = conform(reg,Rcof,0) * conform(reg,xbin,1) + conform(reg,Ryin,0)
	overlay(plot(0) , gsn_csm_xy(wks,xbin,reg,rres))
	
	overlay(plot(0) , gsn_csm_xy(wks,(/-1e3,1e3/),(/0.,0./),lres))
	
;====================================================================================================
; Finalize Figure
;====================================================================================================
		pres = True
		pres@gsnFrame                           = False

	gsn_panel(wks,plot,(/dimsizes(plot),1/),pres)

	legend = create "Legend" legendClass wks
		"lgAutoManage"              : False
		"vpXF"                      : 0.5
		"vpYF"                      : 0.48
		"vpWidthF"                  : 0.1
		"vpHeightF"                 : 0.05
		"lgPerimOn"                 : True
		"lgLabelsOn"                : True
		"lgLineLabelsOn"            : False
		"lgItemCount"               : 2
		"lgLabelStrings"            : (/"ECMWF","Soundings"/)
		"lgLabelFontHeightF"        : 0.01
		"lgDashIndex"             	: 0
		"lgLineThicknessF"			: 4.
		"lgLineColors"              : clr
		"lgMonoDashIndex"			: True
	end create

	;draw(legend)
	frame(wks)
  
	print("")
	print(" "+fig_file+"."+fig_type)
	print("")
	if fig_type.eq."png" then system("convert -trim "+fig_file+"."+fig_type+"   "+fig_file+"."+fig_type) end if

;====================================================================================================
;====================================================================================================
end
